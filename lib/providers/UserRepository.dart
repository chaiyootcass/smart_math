import 'dart:convert';

import 'package:flutter/widgets.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:hive/hive.dart';
import 'package:http/http.dart' as http;
import 'package:smart_math/utility/list_game.dart';
import 'package:smart_math/utility/setting.dart';

enum Status { Uninitialized, Authenticated, Authenticating, Unauthenticated }

class UserRepository with ChangeNotifier {
  FirebaseAuth _auth;
  FirebaseUser _user;
  Status _status = Status.Uninitialized;
  String userId;
  int countStickers = 0;

  UserRepository.instance() : _auth = FirebaseAuth.instance {
    _auth.onAuthStateChanged.listen(_onAuthStateChanged);
  }

  Status get status => _status;
  FirebaseUser get user => _user;

  Future<bool> signIn(String email, String password) async {
    try {
      _status = Status.Authenticating;
      notifyListeners();
      await _auth.signInWithEmailAndPassword(email: email, password: password);
      print('userID $userId');
      await http.post(
        '$serverUrl/user/userdata',
        body: '{"uuid":"${_user.uid}"}',
        headers: {
          "content-type": "application/json",
          "accept": "application/json",
        },
      ).then((result) async {
        final Map<String, dynamic> data =
            json.decode(result.body) as Map<String, dynamic>;
        await Hive.openBox('appData').then((_) {
          data.forEach((k, v) {
            Hive.box('appData').put(k, v);
          });
          data.forEach((k, v) {
            print(k + ' ' + Hive.box('appData').get(k).toString());
          });
        });
      }).catchError((err) {
        print(err);
      });

      return true;
    } catch (e) {
      _status = Status.Unauthenticated;
      notifyListeners();
      return false;
    }
  }

  Future signOut() async {
    await Hive.deleteFromDisk();
    await Hive.close();
    _auth.signOut();
    _status = Status.Unauthenticated;
    userId = null;
    notifyListeners();
    return Future.delayed(Duration.zero);
  }

  Future<bool> signUp(String email, String password) async {
    try {
      _status = Status.Authenticating;
      notifyListeners();
      await _auth.createUserWithEmailAndPassword(
          email: email, password: password);
      userId = _user.uid;
      print('userID $userId');
      await http.post(
        '$serverUrl/user/newusername',
        body: '{"uuid":"${_user.uid}","username":"${email.split("@")[0]}"}',
        headers: {
          "content-type": "application/json",
          "accept": "application/json",
        },
      ).then((result) {
        getUpdate();
        // final data = json.decode(result.body);
        // print('mongodbID : '+data['id']);
      }).catchError((err) {
        print(err);
      });
      await http.post(
        '$serverUrl/user/userdata',
        body: '{"uuid":"${_user.uid}"}',
        headers: {
          "content-type": "application/json",
          "accept": "application/json",
        },
      ).then((result) async {
        final Map<String, dynamic> data =
            json.decode(result.body) as Map<String, dynamic>;
        await Hive.openBox('appData').then((_) {
          data.forEach((k, v) {
            Hive.box('appData').put(k, v);
          });
          data.forEach((k, v) {
            print(k + ' ' + Hive.box('appData').get(k).toString());
          });
        });
      }).catchError((err) {
        print(err);
      });
      return true;
    } catch (e) {
      _status = Status.Unauthenticated;
      notifyListeners();
      return false;
    }
  }

  Future<void> _onAuthStateChanged(FirebaseUser firebaseUser) async {
    if (firebaseUser == null) {
      _status = Status.Unauthenticated;
    } else {
      _user = firebaseUser;
      userId = _user.uid;
      await getUpdate();
      _status = Status.Authenticated;
    }
    notifyListeners();
  }

  void addStickers() {
    countStickers++;
  }

  getUpdate() async {
    await Hive.openBox('appData');
    String lastUpdate = Hive.box('appData').get('lastUpdate') ?? null;
    // print('lastupdate $lastUpdate');
    if (Hive.box('appData').get('setting') == null) {
      Hive.box('appData').put('allTime', 0.0);
      Hive.box('appData').put('allCorrect', 0);
      Hive.box('appData').put('allFail', 0);
      Hive.box('appData').put('setting', [
        {
          'Name': 'Counting Task',
          'Icon': 'assets/menu/count.jpg',
          'route': '/counting_task',
          'open': true,
        },
        {
          'Name': 'Number identification Task',
          'Icon': 'assets/menu/numiden.jpg',
          'route': '/numberident_task',
          'open': true,
        },
        {
          'Name': 'Comparison Task',
          'Icon': 'assets/menu/compareobj.jpg',
          'route': '/comparison_task',
          'open': true,
        },
        {
          'Name': 'Comparison Number Task',
          'Icon': 'assets/menu/comparenumber.jpg',
          'route': '/comparisonnumber_task',
          'open': true,
        },
        // {
        //   'Name': 'Correspondance Task',
        //   'Icon': 'assets/menu/numcorres.jpg',
        //   'route': '/correspondence_task',
        //   'open': true,
        // },
        {
          'Name': 'Ordinal Task',
          'Icon': 'assets/menu/ordinality.jpg',
          'route': '/ordinal_task',
          'open': true,
        },
        {
          'Name': 'Numberline Task',
          'Icon': 'assets/menu/numberline.jpg',
          'route': '/numberline_task',
          'open': true,
        },
        {
          'Name': 'Addition Task',
          'Icon': 'assets/menu/addition.jpg',
          'route': '/addition_task',
          'open': true,
        },
        // {
        //   'Name': 'Test Task',
        //   'Icon': 'assets/menu/test.jpg',
        //   'route': '/test_task',
        //   'open': true,
        // },
      ]);
    }
    listgames.forEach((game) async {
      await Hive.openBox(game['game']);
      await Hive.openBox(game['game'].toString() + '_data');
    });
    await Hive.openBox('test_task');
    if (lastUpdate == null ||
        DateTime.now().difference(DateTime.parse(lastUpdate)).inDays > 0) {
      print('Update Game Level');
      final http.Response response = await http.get("$serverUrl/user/getdata");
      if (response.statusCode == 200) {
        final data = json.decode(response.body) as Map<String, dynamic>;
        Hive.box('appData').put('time', data['limit']['time'] as int);
       // Hive.box('appData').put('game', data['limit']['game'] as int);
        (data['levelgame'] as List<dynamic>).forEach((game) async {
          // await Hive.openBox(game['game'].toString());
          // await Hive.openBox(game['game'].toString() + '_data');
          Hive.box(game['game'].toString()).put('level', game['level']);
          Hive.box(game['game'].toString()).put('numLevel', game['numLevel']);
        });
        Hive.box('appData').put('lastUpdate', DateTime.now().toIso8601String());
      } else {
        throw Exception('Failed to load post');
      }
    }
  }
}
