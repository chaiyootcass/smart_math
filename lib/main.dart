

import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:hive/hive.dart';

import 'package:hive_flutter/hive_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:smart_math/screens/addition_task/additon_task.dart';
import 'package:smart_math/screens/comparison_task.dart';
import 'package:smart_math/screens/comparisonnumber_task.dart';
import 'package:smart_math/screens/correspondace_task.dart';
import 'package:smart_math/screens/counting_task.dart';
import 'package:smart_math/screens/main_task.dart';
import 'package:smart_math/screens/numberline_task.dart';
import 'package:smart_math/screens/ordinal_task.dart';
import 'package:smart_math/screens/size_config.dart';
import 'package:smart_math/screens/test_task/addition_a_task.dart';
import 'package:smart_math/screens/test_task/addition_b_task.dart';
import 'package:smart_math/screens/test_task/comparison_task.dart';
import 'package:smart_math/screens/test_task/comparisonnumber_task.dart';
import 'package:smart_math/screens/test_task/correspondence_task.dart';
import 'package:smart_math/screens/test_task/couting_bc_task.dart';
import 'package:smart_math/screens/test_task/couting_d_task.dart';
import 'package:smart_math/screens/test_task/game_menu.dart';
import 'package:smart_math/screens/test_task/number_ident_task.dart';
import 'package:smart_math/screens/test_task/numberline_task.dart';
import 'package:smart_math/screens/test_task/ordinal_a_task.dart';
import 'package:smart_math/screens/test_task/ordinal_b_task.dart';
import 'package:smart_math/screens/test_task/subtraction_a_task.dart';
import 'package:smart_math/screens/test_task/subtraction_b_task.dart';
import 'providers/UserRepository.dart';
import 'screens/chart_page.dart';
import 'screens/game_menu.dart';
import 'screens/login_page.dart';
import 'screens/number_ident_task.dart';
import 'screens/stickers_room.dart';
import 'package:smart_math/screens/size_config.dart';



void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  //   var dir = await getApplicationDocumentsDirectory();
  //   Hive.initFlutter(dir.path);
  // runApp(DevicePreview(
  //   enabled: true, //เปลี่ยนเป็น false ตอน production
  //   builder: (context) => MyApp(),
  // ));
  SystemChrome.setPreferredOrientations([])
      .then((_) async {
    var dir = await getApplicationDocumentsDirectory();
    Hive.initFlutter(dir.path);
    runApp(new MyApp());
   
  });
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
    // ignore: missing_return
    builder: (context, constraints)
    {
      return OrientationBuilder(
        builder: (context, orientation) {
          SizeConfig().init(constraints, orientation);

          return MultiProvider(
      providers: [
        ChangeNotifierProvider<UserRepository>(
          create: (context) => UserRepository.instance(),
        ),
      ],
      child:MaterialApp(
          // locale: DevicePreview.of(context).locale, // ไว้ตอนใช้กับ DevicePreview
          // builder: DevicePreview.appBuilder,
            theme: ThemeData(
              primarySwatch: Colors.blueGrey,
            ),
            debugShowCheckedModeBanner: false,
            title: 'SmartMath Demo',
            home: HomePage(),
            routes: {
              '/stickers_room': (ctx) => StickersRoom(),
              '/numberident_task': (ctx) => NumberIden(),
              '/chart': (ctx) => ChartPage(),
              '/counting_task': (ctx) => CountingTask(),
              '/comparison_task': (ctx) => ComparisonTask(),
              '/addition_task': (ctx) => AdditionTask(),
              '/ordinal_task': (ctx) => OrdinalTask(),
              '/correspondence_task': (ctx) => CorrespondanceTask(),
              '/numberline_task': (ctx) => NumberlineTask(),
              '/comparisonnumber_task': (ctx) => ComparisonNumber(),
              '/test_task': (ctx) => TestGameMenu(),
              '/game_menu': (ctx) => GameMenu(),
              '/main_task':(ctx) => MainTask(),
              '/test_comparison_task': (ctx) => TestComparisonTask(),
              '/test_comparisonnumber_task': (ctx) => TestComparisonNumber(),
              '/test_addition_a_task': (ctx) => TestAdditionATask(),
              '/test_addition_b_task': (ctx) => TestAdditionBTask(),
              '/test_subtraction_a_task': (ctx) => TestSubtractionATask(),
              '/test_subtraction_b_task': (ctx) => TestSubtractionBTask(),
              '/test_numberline_task': (ctx) => TestNumberlineTask(),
              '/test_numberident_task': (ctx) => TestNumberidentTask(),
              '/test_correspondence_task': (ctx) => TestCorrespondenceTask(),
              '/test_ordinal_a_task': (ctx) => TestOrdinalATask(),
              '/test_ordinal_b_task': (ctx) => TestOrdinalBTask(),
              '/test_counting_bc_task': (ctx) => TestCountingBCTask(),
              '/test_counting_d_task': (ctx) => TestCountingDTask(),
            },
          ));
        },
      );
    },
    );
  }
}

class HomePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return  Consumer<UserRepository>(
        builder: (context, UserRepository user, Widget child) {
          switch (user.status) {
            case Status.Uninitialized:
              return Splash();
            case Status.Unauthenticated:
            case Status.Authenticating:
              return LoginPage();
            case Status.Authenticated:
              return MainTask();
            default:
              return child;
          }
        },
      )
    ;
  }
}

class Splash extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Center(
        child: Text("Splash Screen"),
      ),
    );
  }
}
