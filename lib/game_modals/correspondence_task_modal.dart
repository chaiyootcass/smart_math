import 'dart:math' show Random;
import 'dart:convert';
import 'package:hive/hive.dart';

class CorrespondenceTaskExam {
  
  int level;
  int numExam;
  int min;
  int max;
  int suffuse;
  List<Map<String, dynamic>> exams = [];

  int indexExam; //index exams
  int correct;
  int fail;
  List<int> ans ; //เก็บคำตอบ
  List<int> sol; //เก็บเฉลย
  int countTime;
  CorrespondenceTaskExam(){
    //  var data = Hive.box('correspondence_task').get('unfinished_game');
    // if (data != null) {
    //   print('data ' + data['exams']);
    //   fromJson(data);
    //   return;
    // }
    level = Hive.box('appData').get('correspondence_task_level');
    print('userlevel :$level');
    final Map<dynamic,dynamic> setting = (Hive.box('correspondence_task').get('level') as List<dynamic>)[level-1];
    print(setting);
    numExam = setting['numExam'];
    min = setting['min'];
    max =  setting['max'];
    suffuse =  setting['suffuse'];
    indexExam = 0; 
    correct = 0;
    fail = 0;
     ans = [];
     sol = []; 
    countTime=0;
    genRang();
    exams.forEach((data){
      print(data);
    });
  }
    void fromJson(Map<dynamic, dynamic> data) {
    indexExam = data['indexExam'];
    level = data['level'];
    numExam = data['numExam'];
    min = data['min'];
    max = data['max'];
    suffuse = data['suffuse'];
    correct = data['correct'];
    fail = data['fail'];
    sol = data['sol'];
    ans = data['ans'];
    countTime = data['countTime'];

    List<dynamic> temdata = json.decode(data['exams']);
    temdata.forEach((element) {
      exams.add({"exam": element['exam'], "ans": element['ans'],"suffuse":element['suffuse']});
    });
  }

 Map<String, dynamic> toJson() => {
        'indexExam': indexExam,
        'level': level,
        'numExam': numExam,
        'suffuse': suffuse,
        'min': min,
        'max': max,
        'correct': correct,
        'fail': fail,
        'ans': ans,
        'sol': sol,
        'countTime': countTime,
        'exams': json.encode(exams)
      };

  void genRang(){
     Random rd = new Random();
     int lastAns = 0;
     for(int i=0;i<numExam;i++){
       int ans = rd.nextInt((max - min) + 1) + min;
       while(ans == lastAns){
         ans = rd.nextInt((max - min) + 1) + min;
       }
       lastAns= ans;
       int tem =  rd.nextInt((max - min) + 1) + min;
       while(tem==ans){
           tem = rd.nextInt((max - min) + 1) + min;
       }
       rd.nextBool()?exams.add({'exam':[ans,tem],'ans':ans,'suffuse':suffuse}):
       exams.add({'exam':[tem,ans],'ans':ans,'suffuse':suffuse})
       ;
     }
  }

}