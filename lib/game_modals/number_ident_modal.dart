import 'dart:math' show Random;
import 'dart:convert';
import 'package:hive/hive.dart';

class NumberIdentExam {
  int level;
  int numExam;
  int numChoice;
  int min;
  int max;
  List<Map<String, dynamic>> exams = [];

  int indexExam;
  int correct;
  int fail;
  int countTime;
  List<int> ans = []; //เก็บคำตอบ
  List<int> sol = []; //เก็บเฉลย

  NumberIdentExam() {
    // var data = Hive.box('numberident_task').get('unfinished_game');
    // if (data != null) {
    //   print('data ' + data['exams']);
    //   fromJson(data);
    //   return;
    // }
    level = Hive.box('appData').get('numberident_task_level');
    print('userlevel :$level');
    final Map<dynamic, dynamic> setting =
        (Hive.box('numberident_task').get('level') as List<dynamic>)[level - 1];
    numExam = setting['numExam'];
    numChoice = setting['choice'];
    min = setting['min'];
    max = setting['max'];
    indexExam = 0;
    correct = 0;
    fail = 0;
    countTime = 0;
    ans = [];
    sol = [];
    genRang();
    exams.forEach((data) {
      print(data);
    });
  }
  void fromJson(Map<dynamic, dynamic> data) {
    indexExam = data['indexExam'];
    level = data['level'];
    numExam = data['numExam'];
    numChoice = data['numChoice'];
    min = data['min'];
    max = data['max'];
    correct = data['correct'];
    fail = data['fail'];
    sol = data['sol'];
    ans = data['ans'];
    countTime = data['countTime'];
    List<dynamic> temdata = json.decode(data['exams']);
    temdata.forEach((element) {
      exams.add({"exam": element['exam'], "ans": element['ans']});
    });
  }

  Map<String, dynamic> toJson() => {
        'indexExam': indexExam,
        'level': level,
        'numExam': numExam,
        'numChoice': numChoice,
        'min': min,
        'max': max,
        'correct': correct,
        'fail': fail,
        'ans': ans,
        'sol': sol,
        'countTime': countTime,
        'exams': json.encode(exams)
      };

  int get getNumChoice => numChoice;

  void genRang() {
    Random rd = new Random();
    int oldValue1 = 0;
    int oldValue2 = 0;
    List<int> otherChoice = [];
    for (int i = 0; i < numExam; i++) {
      var x = rd.nextInt(max - min + 1) + min;
      while (x == oldValue1 || x == oldValue2) {
        x = rd.nextInt(max - min + 1) + min;
      }
      oldValue2 = oldValue1;
      oldValue1 = x;
      for (int l = 0; l < numChoice - 1; l++) {
        var y = rd.nextInt(max - min + 1) + min;
        while (y == x || otherChoice.contains(y)) {
          y = rd.nextInt(max - min + 1) + min;
        }
        otherChoice.add(y);
      }

      int ansPosition = rd.nextInt(numChoice);
      List<int> exam = [];
      for (int p = 0, p2 = 0; p < numChoice; p++) {
        if (p == ansPosition) {
          exam.add(x);
        } else {
          exam.add(otherChoice[p2]);
          p2++;
        }
      }
      // print("ans : $x choce : $otherChoice");
      // print("p : $ansPosition exam : $exam");
      exams.add({"exam": exam, "ans": x});
      otherChoice.clear();
    }
  }
}
