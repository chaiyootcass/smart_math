import 'dart:math' show Random;
import 'dart:convert';
import 'package:hive/hive.dart';

class AdditionTaskExam {
  int level;
  int numExam;
  int plusUnder;
  int maxSum;
  List<Map<String, dynamic>> exams = [];

  int indexExam;
  int correct;
  int fail;
  int countTime;
  List<int> ans = []; //เก็บคำตอบ
  List<int> sol = []; //เก็บเฉลย

  AdditionTaskExam() {
    // var data = Hive.box('addition_task').get('unfinished_game');
    // if(data!=null){
    //     print('data ' +data['exams']);
    //     fromJson(data);
    //     return;
    // }
    level = Hive.box('appData').get('addition_task_level');
    print('userlevel :$level');
    final Map<dynamic, dynamic> setting =
        (Hive.box('addition_task').get('level') as List<dynamic>)[level-1];
    print(setting);
    numExam = setting['numExam'];
    plusUnder = setting['plusUnder'];
    maxSum = setting['maxSum'];
    indexExam = 0;
    correct = 0;
    fail = 0;
    countTime = 0;
    ans = [];
    sol = [];
    genRang();
    exams.forEach((data) {
      print(data);
    });
  }

  void fromJson(Map<dynamic, dynamic> data) {
    level = data['level'];
    numExam = data['numExam'];
    correct = data['correct'];
    plusUnder = data['plusUnder'];
    maxSum = data['maxSum'];
    fail = data['fail'];
    sol = data['sol'];
    ans = data['ans'];
    countTime = data['countTime'];
    List<dynamic> temdata = json.decode(data['exams']);
    temdata.forEach((element) {
      exams.add({
        "exam": element['exam'],
        'choice': element['choice'],
        "ans": element['ans']
      });
    });
  }

  Map<String, dynamic> toJson() => {
        'indexExam': indexExam,
        'plusUnder': plusUnder,
        'maxSum': maxSum,
        'level': level,
        'numExam': numExam,
        'correct': correct,
        'fail': fail,
        'ans': ans,
        'sol': sol,
        'countTime': countTime,
        'exams': json.encode(exams)
      };

  void genRang() {
    Random rd = Random();
    int lastAns = 0;
    for (int i = 0; i < numExam; i++) {
      List<int> choice = [];
      List<int> exam = [];
      int ans = rd.nextInt(maxSum - 2 + 1) + 2;
      while (ans == lastAns) {
        ans = rd.nextInt(maxSum - 2 + 1) + 2;
      }
      lastAns = ans;
      int ex1 = rd.nextInt(ans - 1) + 1;
      int ex2 = ans - ex1;
      while (ex2 >= plusUnder) {
        ex1 = rd.nextInt(ans - 1) + 1;
        ex2 = ans - ex1;
      }
      exam.add(ex1);
      exam.add(ex2);
      exam.shuffle();
      choice.add(ans);
      int numChoice =
          level <= 2 ? 3 : level == 3 || level == 4 ? 2 : level >= 5 ? 4 : 4;
      for (int i = 0; i < numChoice - 1; i++) {
        int tem = rd.nextInt(maxSum) + 1;
        while (choice.contains(tem)) tem = rd.nextInt(maxSum) + 1;
        choice.add(tem);
      }
      choice.shuffle();
      exams.add({'exam': exam, 'choice': choice, 'ans': ans});
    }
  }
}
