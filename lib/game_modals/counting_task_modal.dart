import 'dart:math' show Random;
import 'dart:convert' show json;
import 'package:hive/hive.dart';

class CountingTaskExam {
  int level;
  int numExam;
  int min;
  int max;
  int sound;
  List<Map<String, dynamic>> exams = [];

  int indexExam;
  int correct;
  int fail;
  int countTime;
  List<int> ans; //เก็บคำตอบ
  List<int> sol; //เก็บเฉลย
  List<int> itemVisible;
  List<int> itemRetore;

  CountingTaskExam() {
    // var data = Hive.box('counting_task').get('unfinished_game');
    // if (data != null) {
    //   print('data ' + data['exams']);
    //   fromJson(data);
    //   return;
    // }
    level = Hive.box('appData').get('counting_task_level');
    print('userlevel :$level');
    final Map<dynamic, dynamic> setting =
        (Hive.box('counting_task').get('level') as List<dynamic>)[level - 1];
    print(setting);
    numExam = setting['numExam'];
    min = setting['min'];
    max = setting['max'];
    sound = setting['sound'];
    indexExam = 0;
    correct = 0;
    fail = 0;
    countTime = 0;
    ans = [];
    sol = [];
    itemVisible = List<int>.generate(20, (_) => 1);
    itemRetore = [];
    genRang();
    exams.forEach((data) {
      print(data);
    });
  }
void fromJson(Map<dynamic, dynamic> data) {
    indexExam = data['indexExam'];
    level = data['level'];
    numExam = data['numExam'];
    min = data['min'];
    max = data['max'];
    correct = data['correct'];
    fail = data['fail'];
    sol = data['sol'];
    ans = data['ans'];
    sound = data['sound'];
    countTime = data['countTime'];
    itemVisible = data['itemVisible'];
    itemRetore = data['itemRetore'];

    List<dynamic> temdata = json.decode(data['exams']);
    temdata.forEach((element) {
      exams.add({"sound": element['sound'], "ans": element['ans']});
    });
  }
  Map<String, dynamic> toJson() => {
        'indexExam': indexExam,
        'level': level,
        'numExam': numExam,
        'min': min,
        'max': max,
        'correct': correct,
        'fail': fail,
        'ans': ans,
        'sol': sol,
        'sound':sound,
        'countTime': countTime,
        'itemRetore':itemRetore,
        'itemVisible':itemVisible,
        'exams': json.encode(exams)
      };

  void genRang() {
    Random rd = Random();
    int lastAns = 0;
    for (int i = 0; i < numExam; i++) {
      int ans = rd.nextInt(max - min + 1) + min;
      while (ans == lastAns) {
        ans = rd.nextInt(max - min + 1) + min;
      }
      lastAns = ans;
      exams.add({"sound": sound, "ans": ans});
    }
  }
}
