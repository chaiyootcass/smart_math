import 'dart:convert';
import 'dart:math' show Random;
import 'package:hive/hive.dart';

class NumberlineTaskExam {
  int level;
  int numExam;
  int min;
  int max;
  int levelShowNumber;
  int lineLv;
  int countTime;
  List<Map<String, dynamic>> exams = [];
  int indexExam;
  int correct;
  int fail;
  List<int> ans; //เก็บคำตอบ
  List<int> sol; //เก็บเฉลย
  NumberlineTaskExam() {
    // var data = Hive.box('numberline_task').get('unfinished_game');
    // if (data != null) {
    //   print('data ' + data['exams']);
    //   fromJson(data);
    //   return;
    // }
    level = Hive.box('appData').get('numberline_task_level');
    print('userlevel :$level');
    final Map<dynamic, dynamic> setting =
        (Hive.box('numberline_task').get('level') as List<dynamic>)[level - 1];
    print(setting);
    numExam = setting['numExam'];
    min = setting['min'];
    max = setting['max'];
    lineLv = setting['lineLv'];
    countTime = 0;
    indexExam = 0;
    correct = 0;
    fail = 0;
    ans = [];
    sol = [];
    genRang();
    exams.forEach((data) {
      print(data);
    });
  }

  void fromJson(Map<dynamic, dynamic> data) {
    indexExam = data['indexExam'];
    level = data['level'];
    numExam = data['numExam'];
    min = data['min'];
    max = data['max'];
    lineLv = data['lineLv'];
    correct = data['correct'];
    fail = data['fail'];
    sol = data['sol'];
    ans = data['ans'];
    countTime = data['countTime'];

    List<dynamic> temdata = json.decode(data['exams']);
    temdata.forEach((element) {
      exams.add({"choice": element['choice'], "ans": element['ans']});
    });
  }

  Map<String, dynamic> toJson() => {
        'indexExam': indexExam,
        'level': level,
        'numExam': numExam,
        'min': min,
        'max': max,
        'lineLv': lineLv,
        'correct': correct,
        'fail': fail,
        'ans': ans,
        'sol': sol,
        'countTime': countTime,
        'exams': json.encode(exams)
      };

  void genRang() {
    Random rd = Random();
    int lastAns = 0;
    for (int i = 0; i < numExam; i++) {
      List<int> choice = List<int>.generate(11, (i) => i  );
      int ans = choice[rd.nextInt(choice.length - min)]+min;
      while (ans == lastAns) {
        ans = choice[rd.nextInt(choice.length - min)]+min;
      }
      lastAns = ans;
      exams.add({
        'choice': choice,
        'ans': ans,
      });
    }
  }
}
