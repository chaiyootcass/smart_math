class LogGame {
  String userid;
  int level;
  int correct;
  int fail;
  int time;
  String note;
  // List<Map<String, dynamic>> choice = [];
  List<int> solve = [];
  List<int> answer = [];
  LogGame(
      {this.userid,
        this.level,
      this.correct,
      this.fail,
      this.time,
      // this.choice,
      this.note,
       this.solve,
      this.answer});
  toJson() {
    return {
      'userid':userid,
      'level': level,
      'correct': correct,
      'fail': fail,
      'time': time,
      'note':note,
      // 'choice': choice,
      'solve': solve,
      'answer': answer,
    };
  }
}
