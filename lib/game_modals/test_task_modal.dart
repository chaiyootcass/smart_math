import 'package:hive/hive.dart';
import 'package:smart_math/game_modals/test_task_exams.dart';

class TestTaskExams {
  TestTaskExams() {
    int indexTestTask = Hive.box('test_task').get('index');
    if (indexTestTask == null || indexTestTask >= maxIndex-1) {
       if (indexTestTask == maxIndex-1) {
             print('update test task');
            Hive.box('test_task').delete('index');
            List<dynamic> data = Hive.box('test_task').get('data') as List<dynamic>;
            int correct = 0;
            int counttime = 0;
            data.forEach((element) {
              correct += element['correct'];
              counttime += element['counttime'];
            });
            // http.post(
            //   '$serverUrl/test_task/add',
            //   body: json.encode({
            //     'userid': Hive.box('appData').get('_id'),
            //     'log': data,
            //     'correct': correct,
            //     'counttime': counttime
            //   }),
            //   headers: {
            //     "content-type": "application/json",
            //     "accept": "application/json",
            //   },
            // ).then((_) {
            //   print('Upload test_task');
            //    Hive.box('test_task').put('data',[]);
            // });
          }
      //ไม่เคยทำหรือทำจนครบ
      print('maxIndex');
      Hive.box('test_task').put('testgame', listexams[0]['game']);
      Hive.box('test_task').put('index', 0);
      Hive.box('test_task').put('indexTask',0);
      Hive.box('test_task').put('data',[]);
      return;
    }
    if (indexTestTask >= 0) {
      for (int i = 0; i < listexams.length; i++) {
        //ถ้า index เท่ากับข้อสุดท้าย
        if (indexTestTask == listexams[i]['end']) {
          //ไปเกมส์ถัดไปแต่ไม่เกินเกมท้าย
           print('nextgame');
          Hive.box('test_task').put('testgame',
              listexams[i + 1 <= listexams.length ? i + 1 : i]['game']);
              Hive.box('test_task').put('indexTask',0);
          break;
        }
        if (indexTestTask >= listexams[i]['start'] &&
            indexTestTask < listexams[i]['end']) {
          Hive.box('test_task').put('indexTask',indexTestTask-listexams[i]['start'] );
          Hive.box('test_task').put('testgame', listexams[i]['game']);
          break;
        }
      }
    }
  }
}
