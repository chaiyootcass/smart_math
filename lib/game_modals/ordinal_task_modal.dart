import 'dart:convert';
import 'dart:math' show Random;
import 'package:hive/hive.dart';

class OrdinalTaskExam {
  int level;
  int numExam;
  int min;
  int max;
  List<Map<String, dynamic>> exams = [];
  int indexExam;
  int correct;
  int fail;
  List<int> ans; //เก็บคำตอบ
  List<int> sol; //เก็บเฉลย
  int countTime;

  OrdinalTaskExam() {
    level = Hive.box('appData').get('ordinal_task_level');
    print('userlevel :$level');
    final Map<dynamic, dynamic> setting =
       (Hive.box('ordinal_task').get('level') as List<dynamic>)[level-1];
    print(setting);
    numExam = setting['numExam'];
    min = setting['min'];
    max = setting['max'];
    indexExam = 0;
    correct = 0;
    fail = 0;
    ans = [];
    sol = [];
    countTime = 0;
    genRang();
    exams.forEach((d) {
      print(d);
    });
  }

  void fromJson(Map<dynamic, dynamic> data) {
    indexExam = data['indexExam'];
    level = data['level'];
    numExam = data['numExam'];
    min = data['min'];
    max = data['max'];
    correct = data['correct'];
    fail = data['fail'];
    sol = data['sol'];
    ans = data['ans'];
    countTime = data['countTime'];
    List<dynamic> temdata = json.decode(data['exams']);
    temdata.forEach((element) {
      exams.add({
        "exam": element['exam'],
        'choice': element['choice'],
        "ans": element['ans']
      });
    });
  }

  Map<String, dynamic> toJson() => {
        'indexExam': indexExam,
        'level': level,
        'numExam': numExam,
        'min': min,
        'max': max,
        'correct': correct,
        'fail': fail,
        'ans': ans,
        'sol': sol,
        'countTime': countTime,
        'exams': json.encode(exams)
      };

  void genRang() {
    Random rd = Random();
    int numChoice = max < 11 ? 5 : 8;
    int lastIndex = 0;
    for (int i = 0; i < numExam; i++) {
      List<int> ex = [];
      List<int> choice = [];
      int ansindex = rd.nextInt(5);
      while (ansindex == lastIndex) {
        ansindex = rd.nextInt(5);
      }
      lastIndex = ansindex;
      int ans;
      ex = List<int>.generate(5, (i) => max-i);
      ex.sort();
      // choice = List<int>.generate(5, (i) => i + 1);
      ans = ex[ansindex];
      // if (level <= 2) {
      //   ex = List<int>.generate(5, (i) => max-i);
      //   // choice = List<int>.generate(5, (i) => i + 1);
      //   ans = ex[ansindex];
      // } else {
      //   //int tem = rd.nextInt((max - min) + 1) + min;
      //   ex = List<int>.generate(5, (i) => max-i);
      //   ans = ex[ansindex];
      //   // choice.add(ans);
      //   // for (int i = 0; i < numChoice; i++) {
      //   //   int tem = rd.nextInt(max) + 1;
      //   //   while (tem == ans) {
      //   //     tem = rd.nextInt(max) + 1;
      //   //   }
      //   //   choice.add(tem);
      //   // }
      // }
      choice = List<int>.generate(numChoice, (i) => max-i);
      choice.shuffle();
      ex[ansindex] = -1;
      exams.add({'exam': ex, 'choice': choice, 'ans': ans});
    }
  }
}
