import 'dart:math' show Random;
import 'dart:convert';
import 'package:hive/hive.dart';

class ComparsionNumberTask {
  int level;
  int numExam;
  int min;
  int max;
  List<Map<String, dynamic>> exams = [];

  int indexExam;
  int correct;
  int fail;
  int countTime;
  List<int> ans = []; //เก็บคำตอบ
  List<int> sol = []; //เก็บเฉลย

  ComparsionNumberTask() {
    //ไว้เรียกแบบฝึกที่ยังค้างไว้อยู่
    // var data = Hive.box('comparisonnumber_task').get('unfinished_game');
    // if (data != null) {
    //   print('data ' + data['exams']);
    //   fromJson(data);
    //   return;
    // }
    level = Hive.box('appData').get('comparisonnumber_task_level');
    print('userlevel :$level');
    final Map<dynamic, dynamic> setting =
        (Hive.box('comparisonnumber_task').get('level') as List<dynamic>)[level - 1];
    numExam = setting['numExam'];
    min = setting['min'];
    max = setting['max'];
    indexExam = 0;
    correct = 0;
    fail = 0;
    countTime = 0;
    ans = [];
    sol = [];
    genRang();
    // exams.forEach((data) {
    //   print(data);
    // });
  }
  void fromJson(Map<dynamic, dynamic> data) {
    indexExam = data['indexExam'];
    level = data['level'];
    numExam = data['numExam'];
    min = data['min'];
    max = data['max'];
    correct = data['correct'];
    fail = data['fail'];
    sol = data['sol'];
    ans = data['ans'];
    countTime = data['countTime'];
    List<dynamic> temdata = json.decode(data['exams']);
    temdata.forEach((element) {
      exams.add({"exam": element['exam'], "ans": element['ans']});
    });
  }

  Map<String, dynamic> toJson() => {
        'indexExam': indexExam,
        'level': level,
        'numExam': numExam,
        'min': min,
        'max': max,
        'correct': correct,
        'fail': fail,
        'ans': ans,
        'sol': sol,
        'countTime': countTime,
        'exams': json.encode(exams)
      };
  void genRang() {
  Random rd = new Random();
    int oldValue1 = 0;
    int oldValue2 = 0;
    List<int> otherChoice = [];
    for (int i = 0; i < numExam; i++) {
      var x = rd.nextInt(max - min + 1) + min;
      while (x == oldValue1 || x == oldValue2) {
        x = rd.nextInt(max - min + 1) + min;
      }
      oldValue2 = oldValue1;
      oldValue1 = x;
        var y = rd.nextInt(max - min + 1) + min;
        while (y == x || otherChoice.contains(y)) {
          y = rd.nextInt(max - min + 1) + min;
        }
        otherChoice.add(y);
      int ansPosition = rd.nextInt(2);
      List<int> exam = [];
      for (int p = 0, p2 = 0; p < 2; p++) {
        if (p == ansPosition) {
          exam.add(x);
        } else {
          exam.add(otherChoice[p2]);
          p2++;
        }
      }
      // print("ans : $x choce : $otherChoice");
      // print("p : $ansPosition exam : $exam");
      exams.add({"exam": exam, "ans": exam[0]>exam[1]?exam[0]:exam[1]});
      otherChoice.clear();
    }
  }
}
