final int maxIndex = 17;
final List<Map<String, dynamic>> listexams = [
  {
    'game': 'test_comparison_task',
    'passScore':5.0,
    'exams': [
      {
        'exam': [1, 3],
        'ans': 3,
        'example': true,
      },
      {
        'exam': [5, 10],
        'ans': 10
      },
      {
        'exam': [10, 15],
        'ans': 15
      },
      {
        'exam': [8, 6],
        'ans': 8
      },
      {
        'exam': [10, 8],
        'ans': 10
      },
      {
        'exam': [10, 12],
        'ans': 12
      },
      {
        'exam': [14, 12],
        'ans': 14
      },
      {
        'exam': [14, 16],
        'ans': 16
      },
    ]
  },
  {
    'game': 'test_comparisonnumber_task',
     'passScore':3.0,
    'exams': [
      {
        'exam': [1, 3],
        'ans': 3,
        'example': true,
      },
      {
        'exam': [8, 3],
        'ans': 8
      },
      {
        'exam': [5, 9],
        'ans': 9
      },
      {
        'exam': [6, 2],
        'ans': 6
      },
      {
        'exam': [4, 7],
        'ans': 7
      },
      {
        'exam': [19, 15],
        'ans': 19
      },
      {
        'exam': [13, 18],
        'ans': 18
      },
      {
        'exam': [24, 27],
        'ans': 17
      },
    ]
  },
  {
    'game': 'test_numberline_task',
    'passScore':3.0,
    'exams': [
      {
        "choice": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        "ans": 2,
        'example': true,
        'type': 'number'
      },
      {
        "choice": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        "ans": 4,
        'type': 'number'
      },
      {
        "choice": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        "ans": 9,
        'type': 'number'
      },
      {
        "choice": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        "ans": 7,
        'type': 'number'
      },
      {
        "choice": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        "ans": 5,
        'type': 'number'
      },
      {
        "choice": [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20],
        "ans": 10,
        'type': 'number'
      },
      {
        "choice": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        "ans": 2,
        'example': true,
        'type': 'dot'
      },
      {
        "choice": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        "ans": 4,
        'type': 'dot'
      },
      {
        "choice": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        "ans": 7,
        'type': 'dot'
      },
      {
        "choice": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        "ans": 1,
        'type': 'dot'
      },
      {
        "choice": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        "ans": 5,
        'type': 'dot'
      },
      {
        "choice": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        "ans": 9,
        'type': 'dot'
      },
    ]
  },
  {
    'game': 'test_addition_a_task',
    'exams': [
      {
        'exam': [1, 1],
        'choice': [1, 2, 3],
        'ans': 2,
        'example': true,
        'name':'แอปเปิ้ล',
        'index':0,
      },
      {
        'exam': [2, 1],
        'choice': [3, 4, 2],
        'ans': 3,
        'name':'แครอท',
        'asset':4,
        'index':1,
      },
      {
        'exam': [2, 3],
        'choice': [4, 5, 6],
        'ans': 5,
        'name':'กล้วย',
        'asset':9,
        'index':2,
      }
    ]
  },
  {
    'game': 'test_addition_b_task',
    'passScore':2.0,
    'exams': [
      {
        'exam': [1, 1],
        'choice': [1, 2, 3, 4],
        'ans': 2,
        'example': true,
      },
      {
        'exam': [3, 4],
        'choice': [6, 5, 2, 7],
        'ans': 7
      },
      {
        'exam': [12, 2],
        'choice': [9, 14, 8, 12],
        'ans': 14
      },
      {
        'exam': [15, 14],
        'choice': [32, 18, 29, 25],
        'ans': 29
      },
    ]
  },
  {
    'game': 'test_subtraction_a_task',
    'exams': [
      {
        'exam': 0,
        'choice': [3, 1, 2],
        'ans': 1,
        'example': true,
        'name':'แอปเปิ้ล',
      },
      {
        'exam': 1,
        'choice': [3, 4, 5],
        'ans': 3,
        'name':'ละมุด',
      },
      {
        'exam': 2,
        'choice': [6, 7, 5],
        'ans': 5,
        'name':'ลูกแพร์'
      }
    ]
  },
  {
    'game': 'test_subtraction_b_task',
    'passScore':1.0,
    'exams': [
      {
        'exam': [2, 1],
        'choice': [1, 2, 3, 4],
        'ans': 2,
        'example': true
      },
      {
        'exam': [4, 1],
        'choice': [4, 5, 2, 3],
        'ans': 3
      },
      {
        'exam': [17, 4],
        'choice': [14, 21, 13, 3],
        'ans': 13
      },
      {
        'exam': [29, 15],
        'choice': [19, 14, 24, 18],
        'ans': 14
      },
    ]
  },
  {
    'game': 'test_numberident_task',
    'passScore':6.0,
    'exams': [
      {'ans': 1, 'example': true},
      {'ans': 7},
      {'ans': 6},
      {'ans': 3},
      {'ans': 5},
      {'ans': 1},
      {'ans': 2},
      {'ans': 4},
      {'ans': 9},
      {'ans': 0},
      {'ans': 8},
      {'ans': 12},
      {'ans': 25},
      {'ans': 29},
      {'ans': 33},
      {'ans': 40},
    ]
  },
  {
    'game': 'test_correspondence_task',
    'passScore':3.0,
    'exams': [
      {
        'exam': 3,
        'choice': [3, 8, 4],
        'ans': 3,
        'example': true
      },
      {
        'exam': 2,
        'choice': [3, 2, 4],
        'ans': 2,
         'example': true
      },
      {
        'exam': 6,
        'choice': [6, 4, 8],
        'ans': 6
      },
      {
        'exam': 5,
        'choice': [6, 3, 5],
        'ans': 5
      },
      {
        'exam': 13,
        'choice': [11, 12, 13, 14],
        'ans': 13
      },
      {
        'exam': 25,
        'choice': [23, 24, 25, 26],
        'ans': 25
      },
      {
        'exam': 27,
        'choice': [23, 24, 26, 27],
        'ans': 27
      }
    ]
  },
  {
    'game': 'test_ordinal_a_task',
    'exams': [
      {'exam': 5, 'ans': 2, 'example': true, 'animal': 'นก'},
      {'exam': 8, 'ans': 7, 'animal': 'ม้าลาย'},
      {'exam': 10, 'ans': 3, 'animal': 'แกะ'},
    ]
  },
  {
    'game': 'test_ordinal_b_task',
    'passScore':2.0,
    'exams': [
      {
        'exam': [1, 2, 3, -1, 5],
        'choice': [1, 2, 3, 4, 5, 6, 7, 8],
        'ans': 4,
        'example': true
      },
      {
        'exam': [5, 6, -1, 8, 9],
        'choice': [1, 2, 3, 4, 5, 6, 7, 8],
        'ans': 7
      },
      {
        'exam': [12, -1, 14, 15, 16],
        'choice': [10, 11, 12, 13, 14, 15, 16, 17],
        'ans': 13
      },
      {
        'exam': [2, 4, -1, 8, 10],
        'choice': [1, 2, 3, 4, 5, 6, 7, 8],
        'ans': 6
      }
    ]
  },
  {
    'game': 'test_counting_bc_task',
    'exams': [
      {
        'exam': 2,
        'ans': [-1, 3, 4, 5],
        'score': [0, 0.5, 1, 1.5],
        'example': true,
        'topic': 'นับเลขไปข้างหน้าอีก 3 จำนวน'
      },
      {
        'exam': 6,
        'ans': [-1, 7, 8, 9],
        'score': [0, 0.5, 1, 1.5],
        'topic': 'นับเลขไปข้างหน้าอีก 3 จำนวน'
      },
      {
        'exam': 12,
        'ans': [-1, 13, 14, 15],
        'score': [0, 0.5, 1, 1.5],
        'topic': 'นับเลขไปข้างหน้าอีก 3 จำนวน'
      },
      {
        'exam': 10,
        'ans': [-1, 7, 8, 9],
        'score': [0, 1.5, 1, 0.5],
        'example': true,
        'topic': 'นับเลขถอยหลังไปอีก 3 จำนวน'
      },
      {
        'exam': 4,
        'ans': [-1, 1, 2, 3],
       'score': [0, 1.5, 1, 0.5],
        'topic': 'นับเลขถอยหลังไปอีก 3 จำนวน'
      },
       {
        'exam': 15,
        'ans': [-1, 12, 13, 14],
       'score': [0, 1.5, 1, 0.5],
        'topic': 'นับเลขถอยหลังไปอีก 3 จำนวน'
      },
    ]
  },
  {
    'game': 'test_counting_d_task',
    'passScore': 3.5,
    'exams': [
      {
        'exam': 3,
        'example': true,
        'pic':'แมว'
      },
      {
        'exam': 6,
        'pic':'สิงโต'
      },
      {
        'exam': 9,
        'pic':'สุนัข'
      },
      {
        'exam': 15,
        'pic':'หมี'
        
      },
    ]
  },
];
