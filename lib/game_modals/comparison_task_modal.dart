import 'dart:math';
import 'dart:convert';
import 'package:hive/hive.dart';

class ComparisonTaskExam {
  int level;
  int numExam;
  int numExam2;
  int ratio1_1;
  int ratio1_2;
  int ratio2_1;
  int ratio2_2;
  int max;
  List<Map<String, dynamic>> exams = [];

  int indexExam;
  int correct;
  int fail;
  int countTime;
  List<int> ans = []; //เก็บคำตอบ
  List<int> sol = []; //เก็บเฉลย

  
  ComparisonTaskExam() {
    //ไว้เรียกแบบฝึกที่ยังค้างไว้อยู่
    // var data = Hive.box('comparison_task').get('unfinished_game');
    // if (data != null) {
    //   print('data ' + data['exams']);
    //   fromJson(data);
    //   return;
    // }
    level = Hive.box('appData').get('comparison_task_level');
    print('userlevel :$level');
    final Map<dynamic, dynamic> setting =
        (Hive.box('comparison_task').get('level') as List<dynamic>)[level - 1];
    print(setting);
    numExam = setting['numExam'];
    numExam2 = setting['numExam2'];
    ratio1_1 = setting['ratio1_1'];
    ratio1_2 = setting['ratio1_2'];
    ratio2_1 = setting['ratio2_1'];
    ratio2_2 = setting['ratio2_2'];
    max=setting['max'];
    indexExam = 0;
    correct = 0;
    fail = 0;
    countTime = 0;
    ans = [];
    sol = [];
    genRang();
    print('max $max');
    print(exams);
  }

  void fromJson(Map<dynamic, dynamic> data) {
    level = data['level'];
    numExam = data['numExam'];
    numExam2 = data['numExam2'];
    ratio1_1 = data['ratio1_1'];
    ratio1_2 = data['ratio1_2'];
    ratio2_1 = data['ratio2_1'];
    ratio2_2 = data['ratio2_2'];
    max=data['max'];
    indexExam = data['indexExam'];
    correct = data['correct'];
    fail = data['fail'];
    sol = data['sol'];
    ans = data['ans'];
    countTime = data['countTime'];
    
    List<dynamic> temdata = json.decode(data['exams']);
    temdata.forEach((element) {
      exams.add({"exam": element['exam'], "ans": element['ans']});
    });
  }

  Map<String, dynamic> toJson() => {
        'level': level,
        'numExam': numExam,
        'numExam2': numExam2,
        'ratio1_1': ratio1_1,
        'ratio1_2': ratio1_2,
        'ratio2_1': ratio2_1,
        'ratio2_2': ratio2_2,
        'max':max,
        'indexExam': indexExam,
        'correct': correct,
        'fail': fail,
        'ans': ans,
        'sol': sol,
        'countTime': countTime,
        'exams': json.encode(exams)
      };

  void genRang() {
    Random rd = Random();
    int num1 = 0;
    int num2 = 0;
    int lastNum1 = 0;
    int lastNum2 = 0;
    List<int> p1 = List<int>.generate(12, (i) => i==0?1:(i) * ratio1_1)..removeWhere((element) => element > max);
    List<int> p2 = List<int>.generate(12, (i) => i==0?1:(i) * ratio1_2)..removeWhere((element) => element > max);
     print(p1);
    print(p2);
    for (int i = 0; i < numExam; i++) { 
      do {
        int tem = rd.nextInt(p1.length);
        int tem2 = rd.nextInt(p2.length);
        num1 = p1[tem];
        num2 = p2[tem2];
      } while (num1 == lastNum1 || num2 == lastNum2 || num1==num2);
      lastNum1 = num1;
      lastNum2 = num2;
      int maxNum = num1 > num2 ? num1 : num2;
      rd.nextBool()
          ? exams.add({
              'exam': [num1, num2],
              'ans': maxNum
            })
          : exams.add({
              'exam': [num2, num1],
              'ans': maxNum
            });
    }
     p1 = List<int>.generate(12, (i) =>i==0?1: (i) * ratio2_1)..removeWhere((element) => element > max);
     p2 = List<int>.generate(12, (i) => i==0?1:(i) * ratio2_2)..removeWhere((element) => element > max);
    for (int i = 0; i < numExam2; i++) {
      do {
         int tem = rd.nextInt(p1.length);
        int tem2 = rd.nextInt(p2.length);
        num1 = p1[tem];
        num2 = p2[tem2];
      } while (num1 == lastNum1 || num2 == lastNum2|| num1==num2);

      lastNum1 = num1;
      lastNum2 = num2;
      int maxNum = num1 > num2 ? num1 : num2;
      rd.nextBool()
          ? exams.add({
              'exam': [num1, num2],
              'ans': maxNum
            })
          : exams.add({
              'exam': [num2, num1],
              'ans': maxNum
            });
    }
  }
}
