import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math' as math show pi, Random;
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:bordered_text/bordered_text.dart';
import 'package:http/http.dart' as http;
import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:just_audio/just_audio.dart';
import 'package:liquid_progress_indicator/liquid_progress_indicator.dart';
import 'package:smart_math/game_modals/log_game.dart';
import 'package:smart_math/game_modals/ordinal_task_modal.dart';
import 'package:smart_math/screens/size_config.dart';
import 'package:smart_math/utility/custom_star.dart';
import 'package:smart_math/utility/feedback_dialog.dart';
import 'package:smart_math/utility/reward_dialog.dart';
import 'package:smart_math/utility/setting.dart';

class OrdinalTask extends StatefulWidget {
  @override
  _OrdinalTaskState createState() => _OrdinalTaskState();
}

class _OrdinalTaskState extends State<OrdinalTask>
    with AfterLayoutMixin<OrdinalTask> {
  final String namegame = "ordinal_task";
  final player = AudioPlayer();
  math.Random rd = new math.Random();
  int numExam = 5;
  int countFail = 0;
  List<double> postinonExam = [155, 275, 400, 525, 645];
  List<Alignment> postinonAlignExam = [
    Alignment(-0.7, 0.3),
    Alignment(-0.45, 0.3),
    Alignment(-0.23, 0.3),
    Alignment(0.0, 0.3),
    Alignment(0.25, 0.3)
  ];

  bool lastAnswer = false;
  int lastnumAnswer;
  Timer timer;
  DateTime countTime = new DateTime.now(); //ไว้นับเวลา
  OrdinalTaskExam exams;
  int indexExam = 0; //index exams
  List<int> choice = [];
  List<Widget> getExam() {
    lastAnswer = false;
    List<Widget> exam = new List<Widget>();
    Map<String, dynamic> ex = exams.exams.elementAt(indexExam);
    print('index : $indexExam');
    choice = ex['choice'];
    for (int i = 0; i < numExam; i++) {
      int _num = ex['exam'][i];
      if (_num == -1) {
        exam.add(Align(
          alignment: postinonAlignExam[i],
          child: DragTarget(
            onAccept: (data) async {
              print(data);
              lastAnswer = true;
              if (data == ex['ans']) {
                exams.correct++;
                  countFail = 0;
                if (indexExam < exams.exams.length - 1)
                  showDialog(
                      context: context, builder: (context) => FeedbackDialog());
              } else {
                  countFail++;
                if (countFail == 3) {
                  exams.fail++;
                  countFail = 0;
                  await playSound('assets/audio/tryagain.mp3');
                } else {
                  await playSound('assets/audio/falsesound.mp3');
                  return;
                }
              }
              exams.ans.add(data);
              exams.sol.add(ex['ans']);
              checkPromote(namegame);
              playSound('assets/audio/ordinal_task/numberOnTheBox.mp3');
              timer = Timer(Duration(seconds: 2), () async {
                await playSound('assets/audio/clapping.mp3');
                setState(() {
                  lastAnswer = false;
                  timer.cancel();
                });
              });
            },
            onWillAccept: (data) {
              lastnumAnswer = data;
              return true;
            },
            builder: (context, List<int> candidateData, rejectedData) {
              return lastAnswer
                  ? Container(
                      height: 10.0 * SizeConfig.heightMultiplier,
                      width: 12.0 * SizeConfig.widthMultiplier,
                      color: Colors.brown[400],
                      child: Center(
                          child: BorderedText(
                        strokeWidth: 6.0,
                        strokeColor: Colors.white,
                        child: Text(
                          lastnumAnswer.toString(),
                          style: TextStyle(
                              fontSize: 7.0 * SizeConfig.textMultiplier,
                              fontFamily: 'Itim'),
                        ),
                      )),
                    )
                  : Container(
                      height: 25.0 * SizeConfig.heightMultiplier,
                      width: 25.0 * SizeConfig.widthMultiplier,
                      child: Center(
                        child: Text(
                          "",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 9.0 * SizeConfig.textMultiplier,
                              fontFamily: 'Itim'),
                        ),
                      ),
                    );
            },
          ),
        ));
      } else {
        exam.add(
          Align(
            alignment: postinonAlignExam[i],
            child: BorderedText(
              strokeWidth: 6.0,
              strokeColor: Colors.white,
              child: Text(
                _num.toString(),
                style: TextStyle(
                    fontSize: 7.0 * SizeConfig.textMultiplier,
                    fontFamily: 'Itim'),
              ),
            ),
          ),
        );
      }
    }
    playSound('assets/audio/ordinal_task/questions.mp3');
    return exam;
  }

  void playSound(String sound) async {
    await player.setAsset(sound);
    await player.play();
  }

  @override
  void dispose() async {
    super.dispose();
    await player.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        title: Text(
          'Ordinal Task',
          style: TextStyle(color: Colors.grey,  fontSize: 1.5 * SizeConfig.textMultiplier),
        ),
        centerTitle: false,
          actions: [
          Align(
            alignment: Alignment.centerRight,
            child: Row(
              children: [
                 Container(
                  width: 25*SizeConfig.widthMultiplier,
                  height: 3*SizeConfig.heightMultiplier,
                  padding: EdgeInsets.symmetric(horizontal: 2.0),
                  child: LiquidLinearProgressIndicator(
                    value: indexExam / exams.numExam,
                    backgroundColor: Colors.white.withOpacity(0.2),
                    valueColor: AlwaysStoppedAnimation(Colors.yellowAccent),
                    borderColor: Colors.blue,
                    borderWidth: 2.5,
                  ),
                ),
                Text('level : ${exams.level} ',
                    style: TextStyle(
                        color: Colors.grey,
                        fontSize: 1.5 * SizeConfig.textMultiplier)),
              ],
            ),
          )
        ],
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        // title: Text(
        //   'Ordinal Task',
        //   style: TextStyle(color: Colors.black),
        // ),
        // actions: <Widget>[
        //   IconButton(
        //     icon: Icon(Icons.arrow_back),
        //     onPressed: () {
        //       try {
        //         exams.indexExam = indexExam;
        //         exams.countTime +=
        //             DateTime.now().difference(this.countTime).inSeconds;
        //         print(exams.toJson());
        //         Hive.box('ordinal_task').put('unfinished_game', exams.toJson());
        //         Navigator.pop(context);
        //       } catch (e) {
        //         Navigator.pop(context);
        //       }
        //     },
        //   ),
        // ],
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(
                  'assets/games/counting/backgrounds/background-moutain.png'),
              fit: BoxFit.cover),
        ),
        child: SafeArea(
        child: exams != null
            ? Stack(
                children: <Widget>[
                  // Align(
                  //   alignment: Alignment.topRight,
                  //   child: Padding(
                  //     padding: EdgeInsets.only(
                  //       left: 3.0 * SizeConfig.heightMultiplier,
                  //       right: 3.0 * SizeConfig.heightMultiplier,
                  //       top: 3.5 * SizeConfig.heightMultiplier,
                  //     ),
                  //     //bottom: 300 * SizeConfig.heightMultiplier),
                  //     child: CustomStar(indexExam / exams.numExam),
                  //   ),
                  // ),
                
                  Column(children: <Widget>[
                    Expanded(
                      flex: 7,
                      child: Stack(
                        fit: StackFit.expand,
                        children: <Widget>[
                          Image.asset(
                            'assets/games/ordinal/train1.png',
                            width: double.infinity,
                            fit: BoxFit.fitWidth,
                          ),
                          ...getExam().toList(),
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: choice.map((x) {
                            int angleRd =
                                rd.nextInt(10) + 20 * (rd.nextBool() ? 1 : -1);
                            return Draggable(
                              data: x,
                              child: Transform.rotate(
                                angle: (math.pi / angleRd),
                                child: Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 1*SizeConfig.widthMultiplier),
                                  child: Container(
                                    height: 10.0 * SizeConfig.heightMultiplier,
                                    width: 12.0 * SizeConfig.widthMultiplier,
                                    color: Colors.brown[300],
                                    child: Center(
                                        child: BorderedText(
                                      strokeWidth: 6.0,
                                      strokeColor: Colors.white,
                                      child: Text(
                                        x.toString(),
                                        style: TextStyle(
                                            fontSize:
                                                7.0 * SizeConfig.textMultiplier,
                                            fontFamily: 'Itim'),
                                      ),
                                    )),
                                  ),
                                ),
                              ),
                              childWhenDragging: Container(
                                height: 10.0 * SizeConfig.heightMultiplier,
                                width: 10.0 * SizeConfig.widthMultiplier,
                              ),
                              feedback: Material(
                                child: Container(
                                  height: 10.0 * SizeConfig.heightMultiplier,
                                  width: 12.0 * SizeConfig.widthMultiplier,
                                  color: Colors.brown[300],
                                  child: Center(
                                      child: BorderedText(
                                    strokeWidth: 6.0,
                                    strokeColor: Colors.white,
                                    child: Text(
                                      x.toString(),
                                      style: TextStyle(
                                          fontSize:
                                              7.0 * SizeConfig.textMultiplier,
                                          fontFamily: 'Itim'),
                                    ),
                                  )),
                                ),
                              ),
                            );
                          }).toList(),
                        ),
                      ),
                    ),
                  ]),
                   Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: Padding(
                            padding: EdgeInsets.only(
                              left: 15.0 * SizeConfig.heightMultiplier,
                              right: 15.0 * SizeConfig.heightMultiplier,
                              bottom: 50.0 * SizeConfig.heightMultiplier,
                              //top: 1.0 * SizeConfig.heightMultiplier,
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    height: 8 * SizeConfig.heightMultiplier,
                                    padding: EdgeInsets.all(10),
                                    decoration: BoxDecoration(
                                        color: Color(0xffffd6c4),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10.0)),
                                        ),
                                    child: Center(
                                      child: Text(
                                        'ให้หนูเลือกว่าเลขที่หายไปน่าจะเป็นอะไร',
                                        style: TextStyle(
                                          fontFamily: 'Itim',
                                          fontSize:
                                              4 * SizeConfig.textMultiplier,
                                           color: Color(0xff17478c),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),

                ],
              )
            : Center(child: CircularProgressIndicator()),
        ),
      ),
    );
  }

  @override
  void afterFirstLayout(BuildContext context) {
    checkTimeLimit();
    setState(() {
      exams = OrdinalTaskExam();
      //  await player.setAsset("assets/audio/ordinal_task/questions.mp3");
      //  await player.play();
    });
  }

  void checkTimeLimit() {
    if (Hive.box('appData').get('timeUse') == null) return;
    if (Hive.box('appData').get('timeUse') >=
        (Hive.box('appData').get('time') * 60)) {
      AwesomeDialog(
              context: context,
              dialogType: DialogType.INFO,
              animType: AnimType.BOTTOMSLIDE,
              btnCancel: null,
              tittle: 'เวลาหมดแล้ว',
              desc: 'ไว้มาเล่นพรุ่งนี้นะจ้ะ',
              btnOkOnPress: () {})
          .show()
          .then((_) {
        Navigator.pop(context);
      });
    }
    print(Hive.box('appData').get('timeUse'));
    print(Hive.box('appData').get('time'));
  }

  void addTimeLimit(int time) {
    String today =
        DateTime.now().day.toString() + DateTime.now().month.toString();
    if (Hive.box('appData').get('dayLimit') != today) {
      Hive.box('appData').put('dayLimit', today);
      Hive.box('appData').put('timeUse', 0);
    }
    {
      Hive.box('appData')
          .put('timeUse', Hive.box('appData').get('timeUse') + time);
    }
  }

  void checkPromote(String game) async {
    //ถ้าเล่นเกมส์ผิด 3 ข้อเลเวลลง
    if (exams.fail == 3) {
      String note = 'Fail 3 time';
      Hive.box(game).delete('unfinished_game');
      int level = Hive.box('appData').get(game + '_level');
      if (level > 1) {
        note = 'Level Down';
        Hive.box('appData').put(game + '_level', level - 1);
        print('Level Down: $game ${Hive.box('appData').get(game)}');
      }
      LogGame test = LogGame(
          userid: Hive.box('appData').get('_id'),
          level: Hive.box('appData').get(game + '_level'),
          correct: exams.correct,
          fail: exams.fail,
          time: exams.countTime +
              DateTime.now().difference(this.countTime).inSeconds,
          note: note,
          solve: exams.sol,
          answer: exams.ans);
      http.post(
        '$serverUrl/$game/add',
        body: json.encode(test.toJson()),
        headers: {
          "content-type": "application/json",
          "accept": "application/json",
        },
      );
      Hive.box(game + '_data').add({
        'date': '${countTime.day}-${countTime.month}-${countTime.year}',
        'time': test.time,
        'correct': exams.correct,
        'fail': exams.fail
      });
      Hive.box('appData')
          .put('allTime', Hive.box('appData').get('allTime') + test.time);
      Hive.box('appData').put(
          'allCorrect', Hive.box('appData').get('allCorrect') + exams.correct);
      Hive.box('appData')
          .put('allFail', Hive.box('appData').get('allFail') + exams.fail);
      exams = OrdinalTaskExam();
      choice.clear();
      indexExam = 0;
      countTime = new DateTime.now();
     // playSound('assets/audio/tryagain.mp3');
      return;
    }
    //ถ้าเล่นเกมส์ครบชุด
    if (indexExam == exams.exams.length - 1) {
      print('upload log');
      String note = 'Max level';
      //เล่นเกมส์ครบชุด ส่ง log
      Hive.box(game).delete('unfinished_game');

      // เล่นเกมส์ครบชุด เช็คเงื่อนไขการอัพเลเวล
      if (Hive.box('appData').get(game + '_level') <
          Hive.box(game).get('numLevel')) {
        note = 'Level up';
        Hive.box('appData')
            .put(game + '_level', Hive.box('appData').get(game + '_level') + 1);
        print('Level Up: $game ${Hive.box('appData').get(game + '_level')}');
      }
      LogGame test = LogGame(
          userid: Hive.box('appData').get('_id'),
          level: Hive.box('appData').get(game + '_level'),
          correct: exams.correct,
          fail: exams.fail,
          time: exams.countTime +
              DateTime.now().difference(this.countTime).inSeconds,
          note: note,
          solve: exams.sol,
          answer: exams.ans);
      http.post(
        '$serverUrl/$game/add',
        body: json.encode(test.toJson()),
        headers: {
          "content-type": "application/json",
          "accept": "application/json",
        },
      );
      Hive.box(game + '_data').add({
        'date': '${countTime.day}-${countTime.month}-${countTime.year}',
        'time': test.time,
        'correct': exams.correct,
        'fail': exams.fail
      });
      Hive.box('appData')
          .put('allTime', Hive.box('appData').get('allTime') + test.time);
      Hive.box('appData').put(
          'allCorrect', Hive.box('appData').get('allCorrect') + exams.correct);
      Hive.box('appData')
          .put('allFail', Hive.box('appData').get('allFail') + exams.fail);
      exams = OrdinalTaskExam();
      choice.clear();
      indexExam = 0;
      countTime = new DateTime.now();
      addTimeLimit(test.time);
      showDialog(
          context: context,
          builder: (context) => RewardDialog(rd.nextInt(8))).then((_) {
        checkTimeLimit();
      });
      return;
    }
    //เล่นยังไม่ครบ
    indexExam++;
  }
}
