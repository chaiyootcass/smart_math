import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:smart_math/providers/UserRepository.dart';

class StickersRoom extends StatelessWidget {

  const StickersRoom({Key key}) : super(key: key);
  List<Widget> getSticker(int number){
    print('re $number');
     List<Widget> sticker = new List<Widget>();
     for(int i=1;i<=number;i++){
       sticker.add(Image.asset('assets/stickers/$i.png',scale: 2,),);
     }
    return sticker.toList();
  }
  @override
  Widget build(BuildContext context) {
    final auth = Provider.of<UserRepository>(context,listen: false);
    print(auth.countStickers);
    return Scaffold(
        appBar: AppBar(
          title: Text('StckersRoom'),
        ),
        body: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
            image:
                DecorationImage(image: AssetImage('assets/bg/sticker_bg.jpg'),fit: BoxFit.cover),
          ),
          child:Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children :getSticker(auth.countStickers)
              ,
            ),
          ),
        )
      );
  }
}
