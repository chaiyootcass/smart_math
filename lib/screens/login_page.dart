import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:smart_math/screens/size_config.dart';
import '../providers/UserRepository.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextStyle style = TextStyle(fontSize: 40);
  TextEditingController _email;
  TextEditingController _password;
  final _formKey = GlobalKey<FormState>();
  final _key = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    _email = TextEditingController(text: "");
    _password = TextEditingController(text: "");
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserRepository>(context);
    return Scaffold(
      key: _key,
      body: Container(
         width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/bg/home.png'),
                  fit: BoxFit.cover),
            ),
        padding: EdgeInsets.symmetric(horizontal: 18*SizeConfig.widthMultiplier),
        child: Form(
          key: _formKey,
          child: Center(
            child: Container(
              decoration: BoxDecoration( 
                color: Colors.white,
                borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),),
              padding: EdgeInsets.all(20),
              child: ListView(
                shrinkWrap: true,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(16.0),
                    child: TextFormField(
                      controller: _email,
                      validator: (value) =>
                          (value.isEmpty) ? "กรุณาใส่ชื่อผู้ใช้" : null,
                      style: style,
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.people,size: 8*SizeConfig.widthMultiplier,),
                          labelText: "ชื่อผู้ใช้",
                          border: OutlineInputBorder()),
                    ),
                  ),
                  Padding(
                    padding:  EdgeInsets.all(16.0),
                    child: TextFormField(
                     obscureText: true,
                      controller: _password,
                      validator: (value) =>
                          (value.isEmpty) ? "กรุณาใส่รหัสผ่าน" : null,
                      style: style,
                      decoration: InputDecoration(
                          
                          prefixIcon: Icon(Icons.lock,size: 8*SizeConfig.widthMultiplier,),
                          labelText: "รหัสผ่าน",
                          border: OutlineInputBorder()),
                    ),
                  ),
                  user.status == Status.Authenticating
                      ? Center(child: CircularProgressIndicator())
                      : Padding(
                          padding: EdgeInsets.symmetric(horizontal:3*SizeConfig.widthMultiplier),
                          child: Material(
                            elevation: 5.0,
                            borderRadius: BorderRadius.circular(30.0),
                            color: Colors.red,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                MaterialButton(
                                  onPressed: () async {
                                    if (_formKey.currentState.validate()) {
                                      if (!await user.signIn(
                                          _email.text + "@gmail.com",
                                          _password.text))
                                        _key.currentState.showSnackBar(SnackBar(
                                          content: Text("กรุณาตรวจสอบชื่อผู้ใช้/รหัสผ่านให้ถูกต้อง"),
                                        ));
                                    }
                                  },
                                  child: Text(
                                    "เข้าสู่ระบบ",
                                    style: style.copyWith(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                MaterialButton(
                                  onPressed: () async {
                                    if (_formKey.currentState.validate()) {
                                      if (!await user.signUp(
                                          _email.text + "@gmail.com",
                                          _password.text))
                                        _key.currentState.showSnackBar(SnackBar(
                                          content: Text("กรุณาตรวจสอบชื่อผู้ใช้/รหัสผ่านให้ถูกต้อง"),
                                        ));
                                    }
                                  },
                                  child: Text(
                                    "สมัครสมาชิก",
                                    style: style.copyWith(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                  SizedBox(height: 2*SizeConfig.heightMultiplier),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _email.dispose();
    _password.dispose();
    super.dispose();
  }
}
