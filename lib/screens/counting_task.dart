import 'dart:convert' show json;
import 'dart:math' show Random;

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:bordered_text/bordered_text.dart';
import 'package:flutter/material.dart';
import 'package:after_layout/after_layout.dart';
import 'package:hive/hive.dart';
import 'package:http/http.dart' as http;
import 'package:just_audio/just_audio.dart';
import 'package:liquid_progress_indicator/liquid_progress_indicator.dart';

import 'package:smart_math/game_modals/log_game.dart';
import 'package:smart_math/screens/size_config.dart';
import 'package:smart_math/utility/custom_star.dart';
import 'package:smart_math/utility/feedback_dialog.dart';
import 'package:smart_math/utility/reward_dialog.dart';
import 'package:smart_math/utility/setting.dart';
import '../game_modals/counting_task_modal.dart';

class CountingTask extends StatefulWidget {
  @override
  _CountingTaskState createState() => _CountingTaskState();
}

class _CountingTaskState extends State<CountingTask>
    with AfterLayoutMixin<CountingTask> {
  final String namegame = "counting_task";
  final player = AudioPlayer();
  DateTime countTime = new DateTime.now(); //ไว้นับเวลา
  CountingTaskExam exams;
  int lv;
  int indexExam; //index exams
  Map<String, dynamic> ex = {};
  Random rd = new Random(); //for random colors
  int lastIndex = -1;
int countFail = 0;
  List<int> itemVisible = List<int>.generate(20, (_) => 1);
  List<int> itemRetore = [];

  Widget _getExam() {
    ex = exams.exams.elementAt(indexExam);
    // int count = exams.level <= 2 ? 12 : exams.level <= 4 ? 15 : 25;
    // if(genItem){
    //   itemVisible = List.generate(count, (_) => 1);
    //   genItem =false;
    // }
    print('index : $indexExam');
    if (indexExam != lastIndex)
      playSound('assets/audio/counting_task/sound_ques_${ex['ans']}.mp3');
    lastIndex = indexExam;
    return Row(
      children: <Widget>[
        Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(50, 0, 15, 50),
              child: GridView.builder(
                physics: NeverScrollableScrollPhysics(),
                itemCount: 10,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 5,
                    crossAxisSpacing: 3.0,
                    mainAxisSpacing: 3.0),
                itemBuilder: (_, int index) {
                  return GestureDetector(
                      onDoubleTap: () {
                        if (ex['sound'] == 1)
                          playSound(
                              'assets/audio/counting_task/num_${exams.itemRetore.length + 1}.mp3');
                        print('item $index');
                        if (exams.itemVisible[index] == 1) {
                          setState(() {
                            exams.itemVisible[index] = 0;
                            exams.itemRetore.add(index);
                          });
                        }
                      },
                      child: AnimatedOpacity(
                        opacity: exams.itemVisible[index] == 1 ? 1.0 : 0.0,
                        duration: Duration(milliseconds: 500),
                        child: Image.asset(
                            'assets/games/counting/resource/countcandylv${lv.toString()}.png',
                            fit: BoxFit.contain),
                      ));
                },
              ),
            ),
            flex: 2),
        Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(15, 50, 50, 0),
              child: GridView.builder(
                physics: NeverScrollableScrollPhysics(),
                itemCount: 10,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 5,
                    crossAxisSpacing: 3.0,
                    mainAxisSpacing: 3.0),
                itemBuilder: (_, int index) {
                  return GestureDetector(
                      onDoubleTap: () {
                        if (ex['sound'] == 1)
                          playSound(
                              'assets/audio/counting_task/num_${exams.itemRetore.length + 1}.mp3');
                        print('item ${index + 10}');
                        if (exams.itemVisible[index + 10] == 1) {
                          setState(() {
                            exams.itemVisible[index + 10] = 0;
                            exams.itemRetore.add(index + 10);
                          });
                        }
                      },
                      child: AnimatedOpacity(
                        opacity: exams.itemVisible[index + 10] == 1 ? 1.0 : 0.0,
                        duration: Duration(milliseconds: 500),
                        child: Image.asset(
                            'assets/games/counting/resource/countcandylv${lv.toString()}.png',
                            fit: BoxFit.cover),
                      ));
                },
              ),
            ),
            flex: 2),
      ],
    );
  }

  void playSound(String sound) async {
    await player.setAsset(sound);
    await player.play();
  }

  @override
  void dispose() {
    super.dispose();
    player.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
             title: Text(
          'Counting Task',
          style: TextStyle(color: Colors.grey,  fontSize: 1.5 * SizeConfig.textMultiplier),
        ),
        centerTitle: false,
         actions: [
          Align(
            alignment: Alignment.centerRight,
            child: Row(
              children: [
                 Container(
                  width: 25*SizeConfig.widthMultiplier,
                  height: 3*SizeConfig.heightMultiplier,
                  padding: EdgeInsets.symmetric(horizontal: 2.0),
                  child: LiquidLinearProgressIndicator(
                    value: indexExam / exams.numExam,
                    backgroundColor: Colors.white.withOpacity(0.2),
                    valueColor: AlwaysStoppedAnimation(Colors.yellowAccent),
                    borderColor: Colors.blue,
                    borderWidth: 2.5,
                  ),
                ),
                Text('level : ${exams.level} ',
                    style: TextStyle(
                        color: Colors.grey,
                        fontSize: 1.5 * SizeConfig.textMultiplier)),
              ],
            ),
          )
        ],
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        // title: Text('Counting Task',
        //   style: TextStyle(color: Colors.black),),
        // leading: IconButton(
        //   icon: Icon(Icons.arrow_back),
        //   onPressed: () {
        //     try {
        //        exams.indexExam = indexExam;
        //        exams.countTime += DateTime.now().difference(this.countTime).inSeconds;
        //         print(exams.toJson());
        //         Hive.box('counting_task').put('unfinished_game',exams.toJson());
        //         Navigator.pop(context);
        //     } catch (e) {
        //        Navigator.pop(context);
        //     }
        //   },
        // ),
      ),
      body: Container(
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(
                    'assets/games/counting/resource/countbglv${lv.toString()}.png'),
                fit: BoxFit.cover),
          ),
          child: SafeArea(
            child: exams != null
                ? Stack(children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          height: 1 * SizeConfig.heightMultiplier,
                        ),
                        Expanded(
                            flex: 1,
                            child: Container(
                                width: 120 * SizeConfig.widthMultiplier,
                                height: 10 * SizeConfig.heightMultiplier,
                                decoration: BoxDecoration(
                                    color: Color(0xffffd6c4),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10.0)),
                                   ),
                                child: Center(
                                    child: Text(
                                      'นำขนมให้พี่หมี  ${exams.exams[indexExam]['ans']}  ชิ้น แล้วกดที่รูปบ้าน',
                                      style: TextStyle(
                                        fontFamily: 'Itim',
                                        fontSize:
                                            4 * SizeConfig.textMultiplier,
                                        color: Color(0xff17478c),
                                      ),
                                    )))),
                        SizedBox(
                          height: 1 * SizeConfig.heightMultiplier,
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(top: 1),
                            child: _getExam(),
                          ),
                          flex: 4,
                        ),
                        Expanded(
                          flex: 4,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(
                                width: 20 * SizeConfig.widthMultiplier,
                              ),
                              Container(
                                width: 90 * SizeConfig.widthMultiplier,
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                  fit: BoxFit.contain,
                                  image: AssetImage(
                                    'assets/games/counting/resource/counttray.png',
                                  ),
                                )),
                                child: Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal:
                                          18 * SizeConfig.widthMultiplier,
                                      vertical:
                                          1 * SizeConfig.heightMultiplier),
                                  child: GridView.builder(
                                    physics: NeverScrollableScrollPhysics(),
                                    itemCount: 20,
                                    gridDelegate:
                                        SliverGridDelegateWithFixedCrossAxisCount(
                                            crossAxisCount: 6,
                                            crossAxisSpacing: 1.0,
                                            mainAxisSpacing: 1.0),
                                    itemBuilder: (_, int indexs) {
                                      if (indexs < exams.itemRetore.length) {
                                        return Image.asset(
                                            'assets/games/counting/resource/countcandylv${lv.toString()}.png',
                                            fit: BoxFit.cover);
                                      } else {
                                        return null;
                                      }
                                    },
                                  ),
                                ),
                              ),
                              GestureDetector(
                                onTap: () async {
                                  if (exams.itemRetore.length == 0) {
                                    playSound('assets/audio/falsesound.mp3');
                                    return;
                                  }
                                  if (exams.itemRetore.length == ex['ans']) {
                                    exams.correct++;
                                      countFail = 0;
                                    if (indexExam < exams.exams.length - 1)
                                      showDialog(
                                          context: context,
                                          builder: (context) =>
                                              FeedbackDialog());
                                  } else {
                                     countFail++;
                                      if (countFail == 3) {
                                        exams.fail++;
                                        countFail = 0;
                                        await playSound('assets/audio/tryagain.mp3');
                                      } else {
                                        await playSound('assets/audio/falsesound.mp3');
                                        return;
                                      }
                                  }
                                  await playSound(
                                      'assets/audio/buttonClick.mp3');
                                  setState(() {
                                    exams.ans.add(exams.itemRetore.length);
                                    exams.sol.add(ex['ans']);
                                    exams.itemVisible =
                                        List<int>.generate(20, (_) => 1);
                                    exams.itemRetore = [];
                                    checkPromote('counting_task');
                                  });
                                },
                                child: Image.asset(
                                  'assets/games/counting/resource/countok.png',
                                  height: 30 * SizeConfig.widthMultiplier,
                                  fit: BoxFit.contain,
                                ),
                              ),
                              SizedBox(
                                width: SizeConfig.widthMultiplier*2,
                              )
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 2 * SizeConfig.heightMultiplier,
                        ),
                      ],
                    ),
                    Align(
                      alignment: Alignment(-0.9,0.8),
                      child: Image.asset(
                        'assets/animals/หมี.png',
                        height: 30 * SizeConfig.heightMultiplier,
                        fit: BoxFit.contain,
                      ),
                    ),
                    Align(
                        alignment: Alignment(-0.9,0.9),
                        child: Padding(
                          padding: const EdgeInsets.all(50.0),
                          child: Container(
                            width: 10 * SizeConfig.widthMultiplier,
                            height: 10 * SizeConfig.heightMultiplier,
                            child: FittedBox(
                              //ปุ่มกดย้อนกลับ
                              child: FloatingActionButton(
                                  backgroundColor: Colors.blueAccent,
                                  onPressed: () {
                                    print(itemRetore);
                                    if (exams.itemRetore.length > 0) {
                                      setState(() {
                                        exams.itemVisible[
                                            exams.itemRetore.last] = 1;
                                        exams.itemRetore.removeLast();
                                      });
                                    }
                                  },
                                  child: Icon(
                                    Icons.restore,
                                  )),
                            ),
                          ),
                        )),
                    // Align(
                    //   alignment: Alignment.topRight,
                    //   child: Padding(
                    //     padding: EdgeInsets.only(
                    //       left: 3.0 * SizeConfig.heightMultiplier,
                    //       right: 3.0 * SizeConfig.heightMultiplier,
                    //       //top: 1.5 * SizeConfig.heightMultiplier,
                    //     ),
                    //     //bottom: 300 * SizeConfig.heightMultiplier),
                    //     child: CustomStar(indexExam / exams.numExam),
                    //   ),
                    // ),
                  ])
                : Center(
                    child: CircularProgressIndicator(),
                  ),
            // )
          )),
    );
  }

  void checkPromote(String game) {
    //ถ้าเล่นเกมส์ผิด 3 ข้อเลเวลลง
    if (exams.fail == 3) {
      String note = 'Fail 3 time';
      Hive.box(game).delete('unfinished_game');
      int level = Hive.box('appData').get(game + '_level');
      if (level > 1) {
        note = 'Level Down';
        Hive.box('appData').put(game + '_level', level - 1);
        print('Level Down: $game ${Hive.box('appData').get(game)}');
      }
      LogGame test = LogGame(
          userid: Hive.box('appData').get('_id'),
          level: Hive.box('appData').get(game + '_level'),
          correct: exams.correct,
          fail: exams.fail,
          time: exams.countTime +
              DateTime.now().difference(this.countTime).inSeconds,
          note: note,
          solve: exams.sol,
          answer: exams.ans);
      http.post(
        '$serverUrl/$game/add',
        body: json.encode(test.toJson()),
        headers: {
          "content-type": "application/json",
          "accept": "application/json",
        },
      );
      Hive.box(game + '_data').add({
        'date': '${countTime.day}-${countTime.month}-${countTime.year}',
        'time': test.time,
        'correct': exams.correct,
        'fail': exams.fail
      });
      Hive.box('appData')
          .put('allTime', Hive.box('appData').get('allTime') + test.time);
      Hive.box('appData').put(
          'allCorrect', Hive.box('appData').get('allCorrect') + exams.correct);
      Hive.box('appData')
          .put('allFail', Hive.box('appData').get('allFail') + exams.fail);
      exams = CountingTaskExam();
      countTime = new DateTime.now();
      indexExam = exams.indexExam;
      lv = exams.level;
     // playSound('assets/audio/tryagain.mp3');
      return;
    }
    //ถ้าเล่นเกมส์ครบชุด
    if (indexExam == exams.exams.length - 1) {
      print('upload log');
      String note = 'Max level';
      //เล่นเกมส์ครบชุด ส่ง log
      Hive.box(game).delete('unfinished_game');

      // เล่นเกมส์ครบชุด เช็คเงื่อนไขการอัพเลเวล
      if (Hive.box('appData').get(game + '_level') <
          Hive.box(game).get('numLevel')) {
        note = 'Level up';
        Hive.box('appData')
            .put(game + '_level', Hive.box('appData').get(game + '_level') + 1);
        print('Level Up: $game ${Hive.box('appData').get(game + '_level')}');
      }
      LogGame test = LogGame(
          userid: Hive.box('appData').get('_id'),
          level: Hive.box('appData').get(game + '_level'),
          correct: exams.correct,
          fail: exams.fail,
          time: exams.countTime +
              DateTime.now().difference(this.countTime).inSeconds,
          note: note,
          solve: exams.sol,
          answer: exams.ans);
      http.post(
        '$serverUrl/$game/add',
        body: json.encode(test.toJson()),
        headers: {
          "content-type": "application/json",
          "accept": "application/json",
        },
      );
      Hive.box(game + '_data').add({
        'date': '${countTime.day}-${countTime.month}-${countTime.year}',
        'time': test.time,
        'correct': exams.correct,
        'fail': exams.fail
      });
      Hive.box('appData')
          .put('allTime', Hive.box('appData').get('allTime') + test.time);
      Hive.box('appData').put(
          'allCorrect', Hive.box('appData').get('allCorrect') + exams.correct);
      Hive.box('appData')
          .put('allFail', Hive.box('appData').get('allFail') + exams.fail);
      exams = CountingTaskExam();
      countTime = new DateTime.now();
      indexExam = exams.indexExam;
      lv = exams.level;
      addTimeLimit(test.time);
      showDialog(
          context: context,
          builder: (context) => RewardDialog(rd.nextInt(8))).then((_) {
        checkTimeLimit();
      });
      return;
    }
    //เล่นยังไม่ครบ
    lastIndex = indexExam;
    indexExam++;
  }

  void checkTimeLimit() {
    if (Hive.box('appData').get('timeUse') == null) return;
    if (Hive.box('appData').get('timeUse') >=
        (Hive.box('appData').get('time') * 60)) {
      AwesomeDialog(
              context: context,
              dialogType: DialogType.INFO,
              animType: AnimType.BOTTOMSLIDE,
              btnCancel: null,
              tittle: 'เวลาหมดแล้ว',
              desc: 'ไว้มาเล่นพรุ่งนี้นะจ้ะ',
              btnOkOnPress: () {})
          .show()
          .then((_) {
        Navigator.pop(context);
      });
    }
    print(Hive.box('appData').get('timeUse'));
    print(Hive.box('appData').get('time'));
  }

  void addTimeLimit(int time) {
    String today =
        DateTime.now().day.toString() + DateTime.now().month.toString();
    if (Hive.box('appData').get('dayLimit') != today) {
      Hive.box('appData').put('dayLimit', today);
      Hive.box('appData').put('timeUse', 0);
    }
    {
      Hive.box('appData')
          .put('timeUse', Hive.box('appData').get('timeUse') + time);
    }
  }

  @override
  void afterFirstLayout(BuildContext context) {
    checkTimeLimit();
    setState(() {
      exams = CountingTaskExam();
      indexExam = exams.indexExam;
      lv = exams.level;
    });
  }
}
