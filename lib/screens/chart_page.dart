import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:hive/hive.dart';
import 'package:smart_math/screens/size_config.dart';
import 'package:smart_math/utility/list_game.dart';

class ChartPage extends StatefulWidget {
  @override
  _ChartPageState createState() => _ChartPageState();
}

class _ChartPageState extends State<ChartPage> {
  final List<Map<String, dynamic>> listGameAndSumGame = [
    // {
    //   'game': 'all_task',
    // },
    ...listgames
  ];

  Map<String, dynamic> selectedGame;

  @override
  void initState() {
    super.initState();
    selectedGame = listGameAndSumGame[0];
  }

  Key chart = new Key('chartkey');
  GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: scaffoldKey,
        appBar: AppBar(
          title: Text('สรุป'),
        ),
        body: Container(
          child: Column(
            children: [
              SizedBox(
                height: 1*SizeConfig.heightMultiplier,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                                      child: Row(
                                         mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'ข้อที่ทำถูกต้อง',
                          style: TextStyle(fontSize: 2.5*SizeConfig.textMultiplier),
                        ),
                        Icon(
                          Icons.check_circle_outline,
                          size:  2.5*SizeConfig.textMultiplier,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          Hive.box('appData').get('allCorrect').toString(),
                         style: TextStyle(fontSize: 2.5*SizeConfig.textMultiplier),
                        )
                      ],
                    ),
                  ),
                  Expanded(
                                      child: Row(
                                         mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'ข้อที่ทำผิด',
                           style: TextStyle(fontSize: 2.5*SizeConfig.textMultiplier),
                        ),
                        Icon(
                          Icons.close,
                         size:  2.5*SizeConfig.textMultiplier,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          Hive.box('appData').get('allFail').toString(),
                          style: TextStyle(fontSize: 2.5*SizeConfig.textMultiplier),
                        )
                      ],
                    ),
                  ),
                  Expanded(
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'เวลาทั้งหมด',
                           style: TextStyle(fontSize: 2.5*SizeConfig.textMultiplier),
                        ),
                        Icon(
                          Icons.access_time,
                        size:  2.5*SizeConfig.textMultiplier,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          (Hive.box('appData').get('allTime') / 60)
                                  .toStringAsFixed(2) +
                              ' นาที',
                          style: TextStyle(fontSize: 2.5*SizeConfig.textMultiplier),
                        )
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 1*SizeConfig.heightMultiplier,
              ),
              DropdownButton<Map<String, dynamic>>(
                value: selectedGame,
                onChanged: (Map<String, dynamic> game) {
                  setState(() {
                    chart = new Key(game['game']);
                    selectedGame = game;
                  });
                },
                items: listGameAndSumGame.map((Map<String, dynamic> game) {
                  return DropdownMenuItem<Map<String, dynamic>>(
                    value: game,
                    child: Row(
                      children: <Widget>[
                        Text(
                          game['game'],
                          style: TextStyle(color: Colors.black, fontSize: 30),
                        ),
                      ],
                    ),
                  );
                }).toList(),
              ),
              Container(
                  key: chart,
                  height: MediaQuery.of(context).size.height - 200,
                  child: GameChart(game: selectedGame['game']))
            ],
          ),
        ));
  }
}

class GameChart extends StatefulWidget {
  final String game;
  GameChart({Key key, this.game});

  @override
  _GameChartState createState() => _GameChartState();
}

class _GameChartState extends State<GameChart> {
  List<FlSpot> spots = [
    FlSpot(0.0, 1.0),
  ];
  List<String> titles = ['loading'];
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    print(widget.game);
    if (widget.game == 'all_task') {
      Map<String, dynamic> temCorrect = {};
      titles.clear();
      listgames.forEach((element) {
        print(element);
        Hive.box(element['game'] + '_data').values.forEach((data) {
          if (temCorrect[data['date']] != null) {
            temCorrect[data['date']] += data['correct'];
          } else {
            titles.add(data['date']);
            temCorrect[data['date']] = data['correct'];
          }
        });
      });
      spots.clear();
      int index = 0;
      int allvalue = 0;
      temCorrect.forEach((key, value) {
        allvalue += value;
        spots.add(FlSpot(index.toDouble(), allvalue.toDouble()));
        index++;
      });
      if (temCorrect.isNotEmpty) setState(() {});
    } else {
      Map<String, dynamic> temCorrect = {};
      titles.clear();
      Hive.box(widget.game + '_data').values.forEach((data) {
        if (temCorrect[data['date']] != null) {
          temCorrect[data['date']] += data['correct'];
        } else {
          titles.add(data['date']);
          temCorrect[data['date']] = data['correct'];
        }
      });
      spots.clear();
      int index = 0;
      int allvalue = 0;
      temCorrect.forEach((key, value) {
        allvalue += value;
        spots.add(FlSpot(index.toDouble(), allvalue.toDouble()));
        index++;
      });
      if (temCorrect.isNotEmpty) {
        setState(() {});
      } else {
        spots = [
          FlSpot(0.0, 1.0),
        ];
      }
    }
  }

  List<Color> gradientColors = [
    const Color(0xff23b6e6),
    const Color(0xff02d39a),
  ];

  bool showAvg = false;

  @override
  Widget build(BuildContext context) {
    if (widget.game == 'all_task') return Text('data');
    return Stack(
      children: <Widget>[
        AspectRatio(
          aspectRatio: 1.80,
          child: Container(
            decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(18),
                ),
                color: Color(0xff232d37)),
            child: Padding(
              padding: const EdgeInsets.only(
                  right: 18.0, left: 12.0, top: 24, bottom: 12),
              child: LineChart(
                mainData(),
              ),
            ),
          ),
        ),
      ],
    );
  }

  LineChartData mainData() {
    return LineChartData(
      gridData: FlGridData(
        show: false,
        drawVerticalLine: true,
        getDrawingHorizontalLine: (value) {
          return FlLine(
            color: const Color(0xff37434d),
            strokeWidth: 1,
          );
        },
        getDrawingVerticalLine: (value) {
          return FlLine(
            color: const Color(0xff37434d),
            strokeWidth: 1,
          );
        },
      ),
      titlesData: FlTitlesData(
        show: true,
        bottomTitles: SideTitles(
          showTitles: true,
          reservedSize: 22,
          textStyle: const TextStyle(
              color: Color(0xff68737d),
              fontWeight: FontWeight.bold,
              fontSize: 16),
          getTitles: (value) {
            int val = value.toInt();
            if (val < titles.length && val % 5 == 0) {
              return titles[val];
            }
            return '';
          },
          margin: 8,
        ),
        leftTitles: SideTitles(
          showTitles: true,
          textStyle: const TextStyle(
            color: Color(0xff67727d),
            fontWeight: FontWeight.bold,
            fontSize: 15,
          ),
          getTitles: (value) {
            if (value.toInt() % 10 == 0) {
              return value.toString();
            } else {
              return '';
            }
          },
          reservedSize: 40,
          margin: 10,
        ),
      ),
      borderData: FlBorderData(
          show: true,
          border: Border.all(color: const Color(0xff37434d), width: 1)),
      minX: 0,
      maxX: spots.length.toDouble(),
      minY: spots.first.y,
      maxY: spots.last.y,
      lineBarsData: [
        LineChartBarData(
          spots: spots,
          isCurved: true,
          colors: gradientColors,
          barWidth: 5,
          isStrokeCapRound: true,
          dotData: FlDotData(
            show: true,
          ),
          belowBarData: BarAreaData(
            show: true,
            colors:
                gradientColors.map((color) => color.withOpacity(0.3)).toList(),
          ),
        ),
      ],
    );
  }
}
