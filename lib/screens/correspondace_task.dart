import 'dart:convert' show json;
import 'dart:math' show Random;

import 'package:after_layout/after_layout.dart';
import 'package:awesome_dialog/awesome_dialog.dart';

import 'package:bordered_text/bordered_text.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:http/http.dart' as http;
import 'package:just_audio/just_audio.dart';
import 'package:liquid_progress_indicator/liquid_progress_indicator.dart';
import 'package:smart_math/screens/size_config.dart';
import 'package:smart_math/utility/custom_star.dart';
import 'package:smart_math/utility/feedback_dialog.dart';
import 'package:smart_math/utility/reward_dialog.dart';
import 'package:smart_math/utility/setting.dart';
import '../game_modals/correspondence_task_modal.dart';
import 'package:smart_math/game_modals/log_game.dart';
import '../utility/numberToThai.dart';

class CorrespondanceTask extends StatefulWidget {
  @override
  _CorrespondanceTaskState createState() => _CorrespondanceTaskState();
}

class _CorrespondanceTaskState extends State<CorrespondanceTask>
    with AfterLayoutMixin<CorrespondanceTask> {
  final String namegame = "correspondence_task";
  final player = AudioPlayer();
  CorrespondenceTaskExam exams;
  DateTime countTime = new DateTime.now(); //ไว้นับเวลา
  int indexExam = 0; //index exams
  Random rd = new Random(); //for random colors
  List<int> choice = [];
int countFail = 0;
  List<Widget> _getExam() {
    List<Widget> exam = new List<Widget>();
    Map<String, dynamic> ex = exams.exams.elementAt(indexExam);
    choice = ex['exam'].cast<int>();
    int indexItem = rd.nextInt(8) + 1;
    print('ex $ex');
    for (int i = 0; i < choice.length; i++) {
      exam.add(GestureDetector(
        onTap: () async {
          if (choice[i] == ex['ans']) {
            exams.correct++;
              countFail = 0;
            await playSound('assets/audio/buttonClick.mp3');
            if (indexExam < exams.exams.length - 1)
              showDialog(
                  context: context, builder: (context) => FeedbackDialog());
          } else {
            countFail++;
                if (countFail == 3) {
                  exams.fail++;
                  countFail = 0;
                  await playSound('assets/audio/tryagain.mp3');
                } else {
                  await playSound('assets/audio/falsesound.mp3');
                  return;
                }
          }
          exams.ans.add(choice[i]);
          exams.sol.add(ex['ans']);
          setState(() {
            checkPromote(namegame);
          });
        },
        child: Container(
          padding:
              EdgeInsets.only(top: 3.5 * SizeConfig.widthMultiplier,left:  3 * SizeConfig.widthMultiplier,right:  3 * SizeConfig.widthMultiplier),
          width: 60.0 * SizeConfig.widthMultiplier,
          height: 80.0 * SizeConfig.heightMultiplier,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image:
                      AssetImage('assets/games/comparisontark/box-empty.png'),
                  fit: BoxFit.cover)),
          child: ex['suffuse'] == 0
              ? GridView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: choice[i],
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 6,
                      crossAxisSpacing: 1.0,
                      mainAxisSpacing: 1.0),
                  itemBuilder: (_, int index) {
                    return Image.asset(
                      'assets/fruits/Asset$indexItem.png',
                    );
                  },
                )
              : Stack(
                  children:
                      List<int>.generate(choice[i], (index) => index).map((e) {
                    return Align(
                      alignment: Alignment(
                          rd.nextDouble() * (rd.nextBool() ? 1 : -1),
                          rd.nextDouble() * (rd.nextBool() ? 1 : -1)),
                      child: Container(
                        height: 8.5 * SizeConfig.heightMultiplier,
                        width: 8.5 * SizeConfig.widthMultiplier,
                        child: Image.asset('assets/fruits/Asset$indexItem.png',
                            fit: BoxFit.scaleDown),
                      ),
                    );
                  }).toList(),
                ),
        ),
      ));
    }
    playSound('assets/audio/correspondence_task/countAndAnswer.mp3');
    return exam;
  }

  void playSound(String sound) async {
    await player.setAsset(sound);
    await player.play();
  }

  @override
  void dispose() async {
    super.dispose();
    await player.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0.0,
            title: Text(
          'Coreespondace Task',
          style: TextStyle(color: Colors.grey,  fontSize: 1.5 * SizeConfig.textMultiplier),
        ),
        centerTitle: false,
         actions: [
          Align(
            alignment: Alignment.centerRight,
            child: Row(
              children: [
                 Container(
                  width: 25*SizeConfig.widthMultiplier,
                  height: 3*SizeConfig.heightMultiplier,
                  padding: EdgeInsets.symmetric(horizontal: 2.0),
                  child: LiquidLinearProgressIndicator(
                    value: indexExam / exams.numExam,
                    backgroundColor: Colors.white.withOpacity(0.2),
                    valueColor: AlwaysStoppedAnimation(Colors.yellowAccent),
                    borderColor: Colors.blue,
                    borderWidth: 2.5,
                  ),
                ),
                Text('level : ${exams.level} ',
                    style: TextStyle(
                        color: Colors.grey,
                        fontSize: 1.5 * SizeConfig.textMultiplier)),
              ],
            ),
          )
        ],
          //   title: Text('Coreespondace Task',
          //   style: TextStyle(color: Colors.black),),
          //   actions: <Widget>[
          //     IconButton(
          //   icon: Icon(Icons.arrow_back),
          //   onPressed: () {
          //     try {
          //         Hive.box('correspondence_task').delete('unfinished_game');
          //         Navigator.pop(context);
          //     } catch (e) {
          //        Navigator.pop(context);
          //     }
          //   },
          // ),
          //   ],
          //   leading: IconButton(
          //   icon: Icon(Icons.arrow_back),
          //   onPressed: () {
          //     try {
          //        exams.indexExam = indexExam;
          //        exams.countTime += DateTime.now().difference(this.countTime).inSeconds;
          //         print(exams.toJson());
          //         Hive.box('correspondence_task').put('unfinished_game',exams.toJson());
          //         Navigator.pop(context);
          //     } catch (e) {
          //        Navigator.pop(context);
          //     }
          //   },
          // ),
        ),
        body: Container(
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(
                    'assets/games/counting/backgrounds/background-moutain.png'),
                fit: BoxFit.cover),
          ),
          //child: SafeArea(
          child: exams != null
              ? SafeArea(
                              child: Stack(
                    children: <Widget>[
                      Center(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              flex: 2,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Expanded(
                                    flex: 1,
                                    child: Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal:
                                              20 * SizeConfig.widthMultiplier),
                                      child: Container(
                                        height: 9 * SizeConfig.heightMultiplier,
                                        decoration: BoxDecoration(
                                             color: Color(0xffffd6c4),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10.0)),
                                          ),
                                        child: Text(
                                          'ให้หนูเลือกกล่องที่มีผลไม้${numberToThai(exams.exams.elementAt(indexExam)['ans'])}ลูก',
                                          style: TextStyle(
                                            fontFamily: 'Itim',
                                            fontSize:
                                                4 * SizeConfig.textMultiplier,
                                            color: Color(0xff17478c),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Container(
                                width: 40 * SizeConfig.widthMultiplier,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.black87),
                                child: Center(
                                  child: Text(
                                    exams.exams
                                        .elementAt(indexExam)['ans']
                                        .toString(),
                                    style: TextStyle(
                                        fontSize: 5.5 * SizeConfig.textMultiplier,
                                        color: Colors.white,
                                        fontFamily: 'Itim'),
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 5,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: _getExam(),
                              ),
                            ),
                            Expanded(flex: 1, child: Container()),
                          ],
                        ),
                      ),
                    ],
                  ),
              )
              : Center(child: CircularProgressIndicator()),
          //),
        ));
  }

  void checkPromote(String game) {
    //ถ้าเล่นเกมส์ผิด 3 ข้อเลเวลลง
    if (exams.fail == 3) {
      String note = 'Fail 3 time';
      Hive.box(game).delete('unfinished_game');
      int level = Hive.box('appData').get(game + '_level');
      if (level > 1) {
        note = 'Level Down';
        Hive.box('appData').put(game + '_level', level - 1);
        print('Level Down: $game ${Hive.box('appData').get(game)}');
      }
      LogGame test = LogGame(
          userid: Hive.box('appData').get('_id'),
          level: Hive.box('appData').get(game + '_level'),
          correct: exams.correct,
          fail: exams.fail,
          time: exams.countTime +
              DateTime.now().difference(this.countTime).inSeconds,
          note: note,
          solve: exams.sol,
          answer: exams.ans);
      http.post(
        '$serverUrl/$game/add',
        body: json.encode(test.toJson()),
        headers: {
          "content-type": "application/json",
          "accept": "application/json",
        },
      );
      Hive.box(game + '_data').add({
        'date': '${countTime.day}-${countTime.month}-${countTime.year}',
        'time': test.time,
        'correct': exams.correct,
        'fail': exams.fail
      });
      Hive.box('appData')
          .put('allTime', Hive.box('appData').get('allTime') + test.time);
      Hive.box('appData').put(
          'allCorrect', Hive.box('appData').get('allCorrect') + exams.correct);
      Hive.box('appData')
          .put('allFail', Hive.box('appData').get('allFail') + exams.fail);
      exams = CorrespondenceTaskExam();
      choice.clear();
      countTime = new DateTime.now();
      indexExam = 0;
     // playSound('assets/audio/tryagain.mp3');
      return;
    }
    //ถ้าเล่นเกมส์ครบชุด
    if (indexExam == exams.exams.length - 1) {
      print('upload log');
      String note = 'Max level';
      //เล่นเกมส์ครบชุด ส่ง log
      Hive.box(game).delete('unfinished_game');

      // เล่นเกมส์ครบชุด เช็คเงื่อนไขการอัพเลเวล
      if (Hive.box('appData').get(game + '_level') <
          Hive.box(game).get('numLevel')) {
        note = 'Level up';
        Hive.box('appData')
            .put(game + '_level', Hive.box('appData').get(game + '_level') + 1);
        print('Level Up: $game ${Hive.box('appData').get(game + '_level')}');
      }
      LogGame test = LogGame(
          userid: Hive.box('appData').get('_id'),
          level: Hive.box('appData').get(game + '_level'),
          correct: exams.correct,
          fail: exams.fail,
          time: exams.countTime +
              DateTime.now().difference(this.countTime).inSeconds,
          note: note,
          solve: exams.sol,
          answer: exams.ans);
      http.post(
        '$serverUrl/$game/add',
        body: json.encode(test.toJson()),
        headers: {
          "content-type": "application/json",
          "accept": "application/json",
        },
      );
      Hive.box(game + '_data').add({
        'date': '${countTime.day}-${countTime.month}-${countTime.year}',
        'time': test.time,
        'correct': exams.correct,
        'fail': exams.fail
      });
      Hive.box('appData')
          .put('allTime', Hive.box('appData').get('allTime') + test.time);
      Hive.box('appData').put(
          'allCorrect', Hive.box('appData').get('allCorrect') + exams.correct);
      Hive.box('appData')
          .put('allFail', Hive.box('appData').get('allFail') + exams.fail);
      exams = CorrespondenceTaskExam();
      choice.clear();
      countTime = new DateTime.now();
      indexExam = exams.indexExam;
      addTimeLimit(test.time);
      showDialog(
          context: context,
          builder: (context) => RewardDialog(rd.nextInt(8))).then((_) {
        checkTimeLimit();
      });
      return;
    }
    //เล่นยังไม่ครบ
    indexExam++;
  }

  void checkTimeLimit() {
    if (Hive.box('appData').get('timeUse') == null) return;
    if (Hive.box('appData').get('timeUse') >=
        (Hive.box('appData').get('time') * 60)) {
      AwesomeDialog(
              context: context,
              dialogType: DialogType.INFO,
              animType: AnimType.BOTTOMSLIDE,
              btnCancel: null,
              tittle: 'เวลาหมดแล้ว',
              desc: 'ไว้มาเล่นพรุ่งนี้นะจ้ะ',
              btnOkOnPress: () {})
          .show()
          .then((_) {
        Navigator.pop(context);
      });
    }
    print(Hive.box('appData').get('timeUse'));
    print(Hive.box('appData').get('time'));
  }

  void addTimeLimit(int time) {
    String today =
        DateTime.now().day.toString() + DateTime.now().month.toString();
    if (Hive.box('appData').get('dayLimit') != today) {
      Hive.box('appData').put('dayLimit', today);
      Hive.box('appData').put('timeUse', 0);
    }
    {
      Hive.box('appData')
          .put('timeUse', Hive.box('appData').get('timeUse') + time);
    }
  }

  @override
  void afterFirstLayout(BuildContext context) {
    checkTimeLimit();
    setState(() {
      exams = CorrespondenceTaskExam();
      indexExam = exams.indexExam;
    });
  }
}
