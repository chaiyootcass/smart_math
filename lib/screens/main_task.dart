import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:smart_math/providers/UserRepository.dart';
import 'package:smart_math/screens/size_config.dart';

class MainTask extends StatefulWidget {
  @override
  _MainTaskState createState() => _MainTaskState();
}

class _MainTaskState extends State<MainTask> {
  @override
  Widget build(BuildContext context) {
   final user =  Provider.of<UserRepository>(context, listen: false);
    return Scaffold(
      backgroundColor: Color(0xFFf2f2f2),
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Card(
                color: Colors.transparent,
                elevation: 0,
                child: GestureDetector(
                child: Container(
                  height: 30*SizeConfig.heightMultiplier,
                  decoration: BoxDecoration(color: Color.fromRGBO(251, 219, 212, 1),borderRadius: BorderRadius.circular(24)),
                  child: Stack(
                    children: [
                      Align(
                         alignment: Alignment.center,
                        child: Container(
                          height: 45*SizeConfig.imageSizeMultiplier,
                          width: 45*SizeConfig.imageSizeMultiplier,
                           decoration: BoxDecoration(
                         image: DecorationImage(
                          image: AssetImage('assets/menu/แบบฝึกหัด.jpg'),
                          fit: BoxFit.contain))
                        ),
                      ),
                      Align(
                    alignment: Alignment.bottomCenter,
                    child: Padding(
                      padding: EdgeInsets.only(bottom:1.2*SizeConfig.heightMultiplier),
                      child: Text(
                        '',
                        style: TextStyle(
                          fontFamily: 'Itim',
                          fontSize: 2.3
                                   * SizeConfig.textMultiplier,
                          color: Colors.grey,
                        ),
                      ),
                    ))
                    ],
                  ),
                ),
                onTap: () => Navigator.of(context).pushNamed('/game_menu'),
                  )),
                    Card(
                color: Colors.transparent,
                elevation: 0,
                child: GestureDetector(
                child: Container(
                   height: 30*SizeConfig.heightMultiplier,
                  decoration: BoxDecoration(color: Color.fromRGBO(251, 219, 212, 1),borderRadius: BorderRadius.circular(24)),
                  child: Stack(
                    children: [
                      Align(
                         alignment: Alignment.center,
                        child: Container(
                          height: 45*SizeConfig.imageSizeMultiplier,
                          width: 45*SizeConfig.imageSizeMultiplier,
                           decoration: BoxDecoration(
                         image: DecorationImage(
                          image: AssetImage('assets/menu/แบบทดสอบ.jpg'),
                          fit: BoxFit.contain))
                        ),
                      ),
                      Align(
                    alignment: Alignment.bottomCenter,
                    child: Padding(
                      padding: EdgeInsets.only(bottom:1.2*SizeConfig.heightMultiplier),
                      child: Text(
                        '',
                        style: TextStyle(
                          fontFamily: 'Itim',
                          fontSize: 2.3
                                   * SizeConfig.textMultiplier,
                          color: Colors.grey,
                        ),
                      ),
                    ))
                    ],
                  ),
                ),
                onTap: () => Navigator.of(context).pushNamed('/test_task'),
                  )),
          ],
        ),
      ),
    );
  }
}