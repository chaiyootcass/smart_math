import 'dart:async';
import 'dart:convert' show json;
import 'dart:math' show Random;

import 'package:after_layout/after_layout.dart';
import 'package:awesome_dialog/awesome_dialog.dart';

import 'package:bordered_text/bordered_text.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:http/http.dart' as http;
import 'package:just_audio/just_audio.dart';
import 'package:liquid_progress_indicator/liquid_progress_indicator.dart';
import 'package:smart_math/game_modals/comparison_task_modal.dart';
import 'package:smart_math/game_modals/log_game.dart';
import 'package:smart_math/screens/size_config.dart';
import 'package:smart_math/utility/custom_star.dart';
import 'package:smart_math/utility/feedback_dialog.dart';
import 'package:smart_math/utility/reward_dialog.dart';
import 'package:smart_math/utility/setting.dart';

class ComparisonTask extends StatefulWidget {
  @override
  _ComparisonTaskState createState() => _ComparisonTaskState();
}

class _ComparisonTaskState extends State<ComparisonTask>
    with AfterLayoutMixin<ComparisonTask> {
  final String namegame = "comparison_task";
  final player = AudioPlayer();
  DateTime countTime = new DateTime.now(); //ไว้นับเวลา
  ComparisonTaskExam exams;
  int indexExam = 0; //index exams
  int countFail = 0;
  int lastIndex = -1;
  Random rd = new Random(); //for random colors
  List<int> choice = [];
  bool boxempty = false;
  Timer timeForBox;
  int indexImage = 1;

  List<Widget> _getExam() {
    player.setVolume(3.0);
    List<Widget> exam = new List<Widget>();
    Map<String, dynamic> ex = exams.exams.elementAt(indexExam);
    print('index : $indexExam');
    choice = ex['exam'].cast<int>();
    int indexItem = rd.nextInt(8) + 1;
    for (int i = 0; i < choice.length; i++) {
      exam.add(Expanded(
          flex: 1,
          child: GestureDetector(
            onTap: () async {
              if (choice[i] == ex['ans']) {
                exams.correct++;
                 countFail = 0;
                if (indexExam < exams.exams.length - 1)
                  showDialog(
                      context: context, builder: (context) => FeedbackDialog());
                await playSound('assets/audio/soundclickans.mp3');
              } else {
                countFail++;
                if (countFail == 3) {
                  exams.fail++;
                  countFail = 0;
                  await playSound('assets/audio/tryagain.mp3');
                } else {
                  await playSound('assets/audio/falsesound.mp3');
                  return;
                }
              }
              exams.ans.add(choice[i]);
              exams.sol.add(ex['ans']);

              setState(() {
                indexImage = rd.nextInt(23) + 1;
                checkPromote('comparison_task');
              });
            },
            child: boxempty
                ? Container(
                    // padding: EdgeInsets.only(top: 7.0*SizeConfig.heightMultiplier, left: 1.0*SizeConfig.widthMultiplier, right: 1.0*SizeConfig.widthMultiplier),
                    height: 40 * SizeConfig.heightMultiplier,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(
                                'assets/games/comparisontark/box-closed.png'),
                            fit: BoxFit.contain)),
                    child: null)
                : Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(
                        top: 7 * SizeConfig.heightMultiplier,
                        left: 9 * SizeConfig.widthMultiplier,
                        right: 9 * SizeConfig.widthMultiplier),
                    height: 40 * SizeConfig.heightMultiplier,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(
                                'assets/games/comparisontark/box-empty.png'),
                            fit: BoxFit.contain)),
                    child: GridView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: choice[i],
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 5,
                          crossAxisSpacing: 2.0,
                          mainAxisSpacing: 2.0),
                      itemBuilder: (_, int index) {
                        return Image.asset('assets/fruits/Asset$indexItem.png',
                            fit: BoxFit.scaleDown);
                      },
                    ),
                  ),
          )));
    }
    if (indexExam != lastIndex)
      playSound('assets/audio/comparison_task/chooseGreaterBoxFemale.mp3');
    lastIndex = indexExam;
    return exam;
  }

  void playSound(String sound) async {
    await player.setAsset(sound);
    await player.play();
  }

  @override
  void dispose() async {
    super.dispose();
    await player.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        title: Text(
          'Comparison Task',
          style: TextStyle(
              color: Colors.grey, fontSize: 1.5 * SizeConfig.textMultiplier),
        ),
        centerTitle: false,
        actions: [
          Align(
            alignment: Alignment.centerRight,
            child: Row(
              children: [
                 Container(
                  width: 25*SizeConfig.widthMultiplier,
                  height: 3*SizeConfig.heightMultiplier,
                  padding: EdgeInsets.symmetric(horizontal: 2.0),
                  child: LiquidLinearProgressIndicator(
                    value: indexExam / exams.numExam,
                    backgroundColor: Colors.white.withOpacity(0.2),
                    valueColor: AlwaysStoppedAnimation(Colors.yellowAccent),
                    borderColor: Colors.blue,
                    borderWidth: 2.5,
                  ),
                ),
                Text('level : ${exams.level} ',
                    style: TextStyle(
                        color: Colors.grey,
                        fontSize: 1.5 * SizeConfig.textMultiplier)),
              ],
            ),
          )
        ],
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        // leading: IconButton(
        //   icon: Icon(Icons.arrow_back),
        //   onPressed: () {
        //     try {
        //       exams.indexExam = indexExam;
        //       exams.countTime +=
        //           DateTime.now().difference(this.countTime).inSeconds;
        //       print(exams.toJson());
        //       Hive.box('comparison_task')
        //           .put('unfinished_game', exams.toJson());
        //       Navigator.pop(context);
        //     } catch (e) {
        //       Navigator.pop(context);
        //     }
        //   },
        // ),
      ),
      body: Container(
          height: MediaQuery.of(context).size.height,
          width: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(
                    'assets/games/counting/backgrounds/background-moutain.png'),
                fit: BoxFit.cover),
          ),
          child: SafeArea(
            child: exams != null
                ? Stack(
                    children: <Widget>[
                      Center(
                        child: SingleChildScrollView(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(
                                  left: 20.0 * SizeConfig.widthMultiplier,
                                  right: 20.0 * SizeConfig.widthMultiplier,
                                  bottom: 50.0 * SizeConfig.heightMultiplier,
                                  //top: 1.0 * SizeConfig.heightMultiplier,
                                ),
                                child: Container(
                                  height: 8 * SizeConfig.heightMultiplier,
                                  padding: EdgeInsets.symmetric(horizontal: 20),
                                  decoration: BoxDecoration(
                                      color: Color(0xffffd6c4),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(10.0)),
                                      ),
                                  child: Center(
                                    child: Text(
                                      'ให้เด็กๆเลือกกล่องที่มีของมากกว่า',
                                      style: TextStyle(
                                        fontFamily: 'Itim',
                                        fontSize: 4 * SizeConfig.textMultiplier,
                                        color: Color(0xff17478c),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            top: 15 * SizeConfig.heightMultiplier),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            ..._getExam()
                          ],
                        ),
                      ),
                      // Align(
                      //   alignment: Alignment.topRight,
                      //   child: Padding(
                      //     padding: EdgeInsets.only(
                      //         // left: 3.0 * SizeConfig.heightMultiplier,
                      //         // right: 3.0 * SizeConfig.heightMultiplier,
                      //         // top: 3.5 * SizeConfig.heightMultiplier,
                      //         ),
                      //     //bottom: 300 * SizeConfig.heightMultiplier),
                      //     child: CustomStar(indexExam / exams.numExam),
                      //   ),
                      // ),
                    ],
                  )
                : Center(
                    child: CircularProgressIndicator(),
                  ),
          )),
    );
  }

  void checkPromote(String game) {
    boxempty = false;
    timeForBox?.cancel();
    //ถ้าเล่นเกมส์ผิด 3 ข้อเลเวลลง
    if (exams.fail == 3) {
      String note = 'Fail 3 time';
      print(note);
      Hive.box(game).delete('unfinished_game');
      int level = Hive.box('appData').get(game + '_level');
      if (level > 1) {
        note = 'Level Down';
        Hive.box('appData').put(game + '_level', level - 1);
        print('Level Down: $game ${Hive.box('appData').get(game)}');
      }
      LogGame test = LogGame(
          userid: Hive.box('appData').get('_id'),
          level: Hive.box('appData').get(game + '_level'),
          correct: exams.correct,
          fail: exams.fail,
          time: exams.countTime +
              DateTime.now().difference(this.countTime).inSeconds,
          note: note,
          solve: exams.sol,
          answer: exams.ans);
      http.post(
        '$serverUrl/$game/add',
        body: json.encode(test.toJson()),
        headers: {
          "content-type": "application/json",
          "accept": "application/json",
        },
      );
      Hive.box(game + '_data').add({
        'date': '${countTime.day}-${countTime.month}-${countTime.year}',
        'time': test.time,
        'correct': exams.correct,
        'fail': exams.fail
      });
      Hive.box('appData')
          .put('allTime', Hive.box('appData').get('allTime') + test.time);
      Hive.box('appData').put(
          'allCorrect', Hive.box('appData').get('allCorrect') + exams.correct);
      Hive.box('appData')
          .put('allFail', Hive.box('appData').get('allFail') + exams.fail);
      exams = ComparisonTaskExam();
      countTime = new DateTime.now();
      choice.clear();
      indexExam = 0;
      countTime = new DateTime.now();
      checkTimeLimit();
      // playSound('assets/audio/tryagain.mp3');
      return;
    }
    //ถ้าเล่นเกมส์ครบชุด
    if (indexExam == exams.exams.length - 1) {
      print('upload log');
      String note = 'Max level';
      //เล่นเกมส์ครบชุด ส่ง log
      Hive.box(game).delete('unfinished_game');

      // เล่นเกมส์ครบชุด เช็คเงื่อนไขการอัพเลเวล
      if (Hive.box('appData').get(game + '_level') <
          Hive.box(game).get('numLevel')) {
        note = 'Level up';
        Hive.box('appData')
            .put(game + '_level', Hive.box('appData').get(game + '_level') + 1);
        print('Level Up: $game ${Hive.box('appData').get(game + '_level')}');
      }
      LogGame test = LogGame(
          userid: Hive.box('appData').get('_id'),
          level: Hive.box('appData').get(game + '_level'),
          correct: exams.correct,
          fail: exams.fail,
          time: exams.countTime +
              DateTime.now().difference(this.countTime).inSeconds,
          note: note,
          solve: exams.sol,
          answer: exams.ans);
      http.post(
        '$serverUrl/$game/add',
        body: json.encode(test.toJson()),
        headers: {
          "content-type": "application/json",
          "accept": "application/json",
        },
      );
      Hive.box(game + '_data').add({
        'date': '${countTime.day}-${countTime.month}-${countTime.year}',
        'time': test.time,
        'correct': exams.correct,
        'fail': exams.fail
      });
      Hive.box('appData')
          .put('allTime', Hive.box('appData').get('allTime') + test.time);
      Hive.box('appData').put(
          'allCorrect', Hive.box('appData').get('allCorrect') + exams.correct);
      Hive.box('appData')
          .put('allFail', Hive.box('appData').get('allFail') + exams.fail);
      exams = ComparisonTaskExam();
      countTime = new DateTime.now();
      choice.clear();
      indexExam = exams.indexExam;
      addTimeLimit(test.time);
      showDialog(
          context: context,
          builder: (context) => RewardDialog(rd.nextInt(8))).then((_) {
        checkTimeLimit();
        timeForBox = Timer(Duration(milliseconds: 4200), () {
          setState(() {
            boxempty = true;
          });
        });
      });
      return;
    }
    //เล่นยังไม่ครบ
    lastIndex = indexExam;
    indexExam++;
    //ตั้งเวลาเปิดกล้อง
    timeForBox = Timer(Duration(milliseconds: 4200), () {
      setState(() {
        boxempty = true;
      });
    });
  }

  void checkTimeLimit() {
    if (Hive.box('appData').get('timeUse') == null) return;
    if (Hive.box('appData').get('timeUse') >=
        (Hive.box('appData').get('time') * 60)) {
      AwesomeDialog(
              context: context,
              dialogType: DialogType.INFO,
              animType: AnimType.BOTTOMSLIDE,
              btnCancel: null,
              tittle: 'เวลาหมดแล้ว',
              desc: 'ไว้มาเล่นพรุ่งนี้นะจ้ะ',
              btnOkOnPress: () {})
          .show()
          .then((_) {
        Navigator.pop(context);
      });
    }
    print(Hive.box('appData').get('timeUse'));
    print(Hive.box('appData').get('time'));
  }

  void addTimeLimit(int time) {
    String today =
        DateTime.now().day.toString() + DateTime.now().month.toString();
    if (Hive.box('appData').get('dayLimit') != today) {
      Hive.box('appData').put('dayLimit', today);
      Hive.box('appData').put('timeUse', 0);
    }
    {
      Hive.box('appData')
          .put('timeUse', Hive.box('appData').get('timeUse') + time);
    }
  }

  @override
  void afterFirstLayout(BuildContext context) {
    checkTimeLimit();
    setState(() {
      exams = ComparisonTaskExam();
      indexExam = exams.indexExam;
    });
    timeForBox = Timer(Duration(seconds: 3), () {
      setState(() {
        boxempty = true;
      });
    });
  }
}
