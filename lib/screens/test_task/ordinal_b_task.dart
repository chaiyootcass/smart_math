import 'dart:async';
import 'dart:math' show Random;
import 'package:after_layout/after_layout.dart';
import 'package:bordered_text/bordered_text.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:just_audio/just_audio.dart';
import 'package:smart_math/game_modals/test_task.dart';
import 'package:smart_math/game_modals/test_task_exams.dart';
import 'package:smart_math/utility/custom_dialog.dart';
import 'package:tutorial_coach_mark/animated_focus_light.dart';
import 'package:tutorial_coach_mark/tutorial_coach_mark.dart';

import '../size_config.dart';

class TestOrdinalBTask extends StatefulWidget {
  @override
  _TestOrdinalBTaskState createState() => _TestOrdinalBTaskState();
}

class _TestOrdinalBTaskState extends State<TestOrdinalBTask>
    with AfterLayoutMixin<TestOrdinalBTask> {
  final String namegame = "test_ordinal_b_task";
  final player = AudioPlayer();
  Map<String, dynamic> additionAScore;
//for example
  List<TargetFocus> targets = List();
  GlobalKey keyButton = GlobalKey();
  GlobalKey keyButton2 = GlobalKey();

  DateTime countTime = new DateTime.now(); //ไว้นับเวลา
  TestTask exams;

  int indexExam = 0; //index exams
  Random rd = new Random(); //for random colors
  List<int> choice = [];
  int lastnumAnswer = -1;
  bool lastAnswer = false;
  double passScore = 0.0;
  Timer timer;
  List<Widget> _getExam() {
    List<Widget> exam = new List<Widget>();
    Map<String, dynamic> ex = exams.exams.elementAt(indexExam);
    print('index : $indexExam');
    choice = ex['exam'];
    for (int i = 0; i < choice.length; i++) {
      exam.add(Container(
        decoration: BoxDecoration(
            color: Colors.grey[200],
            boxShadow: const [BoxShadow(blurRadius: 5)],
            borderRadius: BorderRadius.all(Radius.circular(18))),
        child: Center(
            child: choice[i] == -1
                ? DragTarget<int>(
                    onAccept: (data) {
                      lastAnswer = true;
                      if (exams.exams[indexExam]['example'] == true) {
                      } else {
                        if (data == ex['ans']) {
                          exams.correct++;
                        } else {
                          exams.fail++;
                        }
                        exams.time.add(DateTime.now()
                                .difference(this.countTime)
                                .inMilliseconds /
                            1000);
                        exams.ans.add(data);
                        exams.sol.add(ex['ans']);
                      }
                      playSound('assets/audio/ordinal_task/numberOnTheBox.mp3');
                      checkPromote();
                    },
                    onWillAccept: (data) {
                      lastnumAnswer = data;
                      return true;
                    },
                    builder: (context, List<int> candidateData, rejectedData) {
                      return lastAnswer
                          ? textWithStroke(
                              fontSize: 14 * SizeConfig.textMultiplier,
                              text: lastnumAnswer.toString(),
                              textColor: Colors.blue,
                              strokeWidth: 10,
                              strokeColor: Colors.blueGrey)
                          : Text(
                              " ",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 14 * SizeConfig.textMultiplier),
                            );
                    },
                  )
                : textWithStroke(
                    fontSize: 14 * SizeConfig.textMultiplier,
                    text: choice[i].toString(),
                    textColor: Colors.blue,
                    strokeWidth: 10,
                    strokeColor: Colors.blueGrey)),
      ));
    }
    if (ex['example'] == true) {
      playSound('assets/audio/letSeeExample.mp3',
          'assets/audio/ordinal_task/pickNumberPlaceInBox.mp3');
    } else {
      playSound('assets/audio/ordinal_task/pickNumberPlaceInBox.mp3');
    }
    return exam;
  }

  Widget _showChoice() {
    List<int> ex = exams.exams.elementAt(indexExam)['choice'];
    return Column(
      children: <Widget>[
        Row(
          children: ex.sublist(0, 4).map((e) {
            return Padding(
              padding: EdgeInsets.only(left: 6 * SizeConfig.widthMultiplier),
              child: numberExam(e),
            );
          }).toList(),
        ),
        Row(
          children: ex.sublist(4, 8).map((e) {
            return Padding(
              padding: EdgeInsets.only(left: 6 * SizeConfig.widthMultiplier),
              child: numberExam(e),
            );
          }).toList(),
        )
      ],
    );
  }

  Draggable<int> numberExam(int data) {
    return Draggable<int>(
      data: data,
      childWhenDragging: SizedBox(
        height: 10 * SizeConfig.heightMultiplier,
        width: 14 * SizeConfig.widthMultiplier,
      ),
      feedback: Material(
        type: MaterialType.transparency,
        child: textWithStroke(
            fontSize: 14 * SizeConfig.textMultiplier,
            text: data.toString(),
            textColor: Colors.blue,
            strokeWidth: 10,
            strokeColor: Colors.blueGrey),
      ),
      child: textWithStroke(
          fontSize: 14 * SizeConfig.textMultiplier,
          text: data.toString(),
          textColor: Colors.blue,
          strokeWidth: 10,
          strokeColor: Colors.blueGrey),
    );
  }

//  if (exams.exams[indexExam]['example'] == true) {

//             }else{
//                if (choice[i] == ex['ans']) {
//                 exams.correct++;
//               } else {
//                 exams.fail++;
//               }
//               exams.ans.add(choice[i]);
//               exams.sol.add(ex['ans']);
//             }
//             setState(() {
//               checkPromote();
//             });
  Stack textWithStroke(
      {String text,
      double fontSize: 12,
      double strokeWidth: 1,
      Color textColor: Colors.white,
      Color strokeColor: Colors.black}) {
    return Stack(
      children: <Widget>[
        Text(
          text,
          style: TextStyle(
            fontSize: fontSize,
            foreground: Paint()
              ..style = PaintingStyle.stroke
              ..strokeWidth = strokeWidth
              ..color = strokeColor,
          ),
        ),
        Text(text, style: TextStyle(fontSize: fontSize, color: textColor)),
      ],
    );
  }

  Text textOutile(String txt) {
    return Text(
      txt,
      style: TextStyle(
        fontSize: 80,
        color: Color.fromRGBO(102, 68, 0, 1),
        foreground: Paint()
          ..style = PaintingStyle.stroke
          ..strokeWidth = 6
          ..color = Colors.blue[700],
      ),
    );
  }

  @override
  void dispose() async {
    super.dispose();
    await player.dispose();
  }

  void playSound(String sound, [String sound2]) async {
    await player.setAsset(sound);
    await player.play();
    if (sound2 != null) {
      await player.setAsset(sound2);
      await player.play();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        // title: Text(
        //   'Test ordinal B Task',
        //   style: TextStyle(color: Colors.black),
        // ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
            return;
          },
        ),
      ),
      body: Container(
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/bg/game_bg.jpg'), fit: BoxFit.cover),
          ),
          child: SafeArea(
            child: exams != null
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 15 * SizeConfig.widthMultiplier),
                          child: Container(
                            decoration: BoxDecoration(
                                color: Color(0xffffd6c4),
                                borderRadius: BorderRadius.circular(10.0),
                               ),
                            child: Center(
                              child: Text(
                                'หาเลขที่หายไปใส่กล่อง',
                                style: TextStyle(
                                  fontFamily: 'Itim',
                                  fontSize: 3 * SizeConfig.textMultiplier,
                                  color: Color(0xff17478c),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                          flex: 3,
                          child: GridView.count(
                            padding: EdgeInsets.only(
                                top: 3 * SizeConfig.widthMultiplier),
                            key: keyButton2,
                            physics: NeverScrollableScrollPhysics(),
                            crossAxisCount: 5,
                            mainAxisSpacing: 6 * SizeConfig.widthMultiplier,
                            crossAxisSpacing: 6 * SizeConfig.widthMultiplier,
                            children: _getExam(),
                          )),
                      Expanded(
                          flex: 5,
                          child: SingleChildScrollView(child: _showChoice()))
                    ],
                  )
                : Center(
                    child: CircularProgressIndicator(),
                  ),
          )),
    );
  }

  void checkPromote() {
    //ถ้าเล่นเกมส์ครบชุด
    if (indexExam == exams.exams.length - 1) {
      //task check correct null before put
      // List<dynamic> data = Hive.box('test_task').get('data') as List<dynamic>;
      // data.add({
      //   'namegame': namegame,
      //   'correct': exams.correct,
      //   'ans': exams.ans,
      //   'counttime': DateTime.now().difference(this.countTime).inSeconds
      // });
      //Hive.box('test_task').put('data', data);
      Hive.box('test_task').delete(namegame);
      Hive.box('test_task').put(namegame, {
        'userid': Hive.box('appData').get('_id'),
        'namegame': namegame,
        'correct': exams.correct  + additionAScore['correct'],
        'score': exams.correct,
        'ans': exams.ans,
        'time': exams.time,
        'counttime': exams.allTime()+ additionAScore['time']
      });
      //TestTaskExams();
      print(Hive.box('test_task').get(namegame));
      // print('indexttest :' + Hive.box('test_task').get('index').toString());
      // Navigator.of(context).pop();
      // Future.delayed(const Duration(milliseconds: 300), () {
      //   Navigator.of(context)
      //       .pushNamed("/" + Hive.box('test_task').get('testgame'));
      // });
      //เปลี่ยนเป็นโชว์สรุป
      Future.delayed(const Duration(seconds: 2), () {
        showDialog(
            context: context,
            builder: (BuildContext context) => resultCustomDialog(
                context: context,
                score: exams.correct + additionAScore['correct'],
                lenght: exams.fail + exams.correct + additionAScore['lenght'],
                passScore: passScore)).then((_) => Navigator.of(context).pop());
      });
      return;
    }
    //เล่นยังไม่ครบ
    indexExam++;
    timer = new Timer(new Duration(seconds: 2), () {
      setState(() {
        lastAnswer = false;
        timer.cancel();
      });
    });
    countTime = new DateTime.now();
    //if (exams.exams[indexExam]['example'] == true) {
      // Future.delayed(Duration(milliseconds: 100), () {
      //   showTutorial();
      // });
    //}
    //  Hive.box('test_task').put('index', Hive.box('test_task').get('index') + 1);
  }

  @override
  void afterFirstLayout(BuildContext context) {
    additionAScore = ModalRoute.of(context).settings.arguments;
    setState(() {
      indexExam = 0;
      exams = TestTask();
      Map<String, dynamic> data = listexams.firstWhere((x) {
        return x['game'] == namegame;
      });
      exams.exams = data['exams'];
      passScore = data['passScore'];
      countTime = new DateTime.now();
      if (exams.exams[indexExam]['example'] == true) {
        // initTargets();
        // Future.delayed(Duration(milliseconds: 100), () {
        //   showTutorial();
        // });
      }
      print('passScore ' + passScore.toString());
    });
  }

  void initTargets() {
    targets.add(TargetFocus(
      identify: "Target 1",
      keyTarget: keyButton,
      contents: [
        ContentTarget(
            align: AlignContent.bottom,
            child: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "อ่านโจทย์ที่กำหนดให้",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 40.0),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: Text(
                      "หาเลขที่หายไปใส่กล่อง",
                      style: TextStyle(color: Colors.white, fontSize: 40.0),
                    ),
                  )
                ],
              ),
            ))
      ],
      shape: ShapeLightFocus.RRect,
    ));
    targets.add(TargetFocus(
      identify: "Target 2",
      keyTarget: keyButton2,
      contents: [
        ContentTarget(
            align: AlignContent.bottom,
            child: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "หาเลขที่หายไปใส่กล่อง",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 40.0),
                  ),
                ],
              ),
            ))
      ],
      shape: ShapeLightFocus.RRect,
    ));
  }

  void showTutorial() {
    TutorialCoachMark(context,
        targets: targets,
        colorShadow: Colors.blueGrey,
        textSkip: "ข้าม",
        paddingFocus: 10,
        opacityShadow: 0.8, finish: () {
      print("finish");
      countTime = new DateTime.now();
    }, clickTarget: (target) {
      print(target);
    }, clickSkip: () {
      print("skip");
      countTime = new DateTime.now();
    })
      ..show();
  }
}
