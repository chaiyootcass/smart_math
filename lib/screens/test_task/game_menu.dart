import 'package:bordered_text/bordered_text.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:smart_math/screens/size_config.dart';

class TestGameMenu extends StatefulWidget {
  TestGameMenu({Key key}) : super(key: key);
  @override
  _TestGameMenuState createState() => _TestGameMenuState();
}

class _TestGameMenuState extends State<TestGameMenu> {
  final List<Map<String, String>> listGame = [
    {
      'Name': 'Counting',
      'Icon': 'assets/menu/count.jpg',
      'route': 'test_counting_bc_task',
      'route2': 'test_counting_d_task'
    },
    {
      'Name': 'Number identification',
      'Icon': 'assets/menu/numiden.jpg',
      'route': 'test_numberident_task'
    },
    {
      'Name': 'Comparison',
      'Icon': 'assets/menu/compareobj.jpg',
      'route': 'test_comparison_task'
    },
    {
      'Name': 'Comparison Number',
      'Icon': 'assets/menu/comparenumber.jpg',
      'route': 'test_comparisonnumber_task'
    },
    {
      'Name': 'Correspondance',
      'Icon': 'assets/menu/numcorres.jpg',
      'route': 'test_correspondence_task'
    },
    {
      'Name': 'Ordinal',
      'Icon': 'assets/menu/ordinality.jpg',
      'route': 'test_ordinal_a_task',
      'route2': 'test_ordinal_b_task'
    },
    {
      'Name': 'Numberline',
      'Icon': 'assets/menu/numberline.jpg',
      'route': 'test_numberline_task'
    },
    {
      'Name': 'Addition',
      'Icon': 'assets/menu/addition.jpg',
      'route': 'test_addition_a_task',
      'route2': 'test_addition_b_task',
    },
    {
      'Name': 'Subtraction',
      'Icon': 'assets/menu/sub.jpg',
      'route': 'test_subtraction_a_task',
      'route2': 'test_subtraction_b_task'
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFf2f2f2),
      appBar: AppBar(
        title: Text('แบบทดสอบ'),
        actions: [
          IconButton(
              icon: Icon(Icons.report),
              onPressed: () {
                showDialog(
                    context: context,
                    builder: (context) => resultAllScoreDialog());
              })
        ],
      ),
      body: Container(
          padding: EdgeInsets.all(15.0),
          child: GridView.count(
              scrollDirection: Axis.horizontal,
              crossAxisCount: 2,
              padding: EdgeInsets.all(15),
              crossAxisSpacing: 10,
              mainAxisSpacing: 5,
              children: <Widget>[
                ...listGame.map((data) {
                  return Card(
                color: Colors.transparent,
                elevation: 0,
                child: GestureDetector(
                child: Container(
                  decoration: BoxDecoration(color: Color.fromRGBO(251, 219, 212, 1),borderRadius: BorderRadius.circular(24)),
                  child: Stack(
                    children: [
                      Align(
                         alignment: Alignment.center,
                        child: Container(
                          height: 35*SizeConfig.imageSizeMultiplier,
                          width: 35*SizeConfig.imageSizeMultiplier,
                           decoration: BoxDecoration(
                         image: DecorationImage(
                          image: AssetImage(data['Icon']),
                          fit: BoxFit.contain))
                        ),
                      ),
                      Align(
                    alignment: Alignment.bottomCenter,
                    child: Padding(
                      padding: EdgeInsets.only(bottom:1.2*SizeConfig.heightMultiplier),
                      child: Text(
                        data['Name'],
                        style: TextStyle(
                          fontFamily: 'Itim',
                          fontSize: 2.3
                                   * SizeConfig.textMultiplier,
                          color: Colors.grey,
                        ),
                      ),
                    ))
                    ],
                  ),
                ),
                onTap: () => Navigator.of(context).pushNamed('/' + data['route']),
                  ));
                }).toList(),
              ])
          /*child: Column(
        children: <Widget>[
          ...listGame.map((data) {
            return Card(
              child: ListTile(
                trailing: ValueListenableBuilder(
                  valueListenable: Hive.box('test_task').listenable(),
                  builder: (_, box, __) {
                    return Text(box.get(data['route']) != null
                        ? 'เคยทดสอบแล้ว'
                        : 'ยังไม่เคยสอบ');
                  },
                ),
                leading: FittedBox(
                    fit: BoxFit.fill,
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: 5, width: 10),
                        Image.asset(data['Icon']),
                      ],
                    )),
                title: Text(data['Name']),
                onTap: () =>
                    Navigator.of(context).pushNamed('/' + data['route']),
              ),
            );
          }).toList()
        ],
      )*/
          ),
    );
  }

  List<Map<String, dynamic>> calAllTestScore() {
    List<Map<String, dynamic>> testScore = [];
    double allScore = 0.0, allTime = 0.0;
    listGame.forEach((data) {
      double score = 0.0;
      double time = 0.0;
      Map<dynamic, dynamic> tem = Hive.box('test_task').get(data['route']);
      if (tem != null) {
        time += tem['counttime'];
        score += tem['score'];
        if (data['route2'] != null) {
          Map<dynamic, dynamic> tem = Hive.box('test_task').get(data['route2']);
          if (tem != null) {
            time += tem['counttime'];
            score += tem['score'];
          }
        }
        allScore += score;
        allTime += time;
      }
      testScore.add({'Name': data['Name'], 'counttime': time, 'score': score});
    });
    testScore.add({'Name': 'ผลสรุป', 'counttime': allTime, 'score': allScore});
    return testScore;
  }

  Dialog resultAllScoreDialog() {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12.0),
      ),
      child: GestureDetector(
        onTap: null,
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20.0),
          ),
          // height: 550.0,
          width: MediaQuery.of(context).size.width-50,
          child: Wrap(
            children: <Widget>[
              Container(
                width: double.infinity,
                height: 8*SizeConfig.heightMultiplier,
                alignment: Alignment.bottomCenter,
                decoration: BoxDecoration(
                  color: Colors.greenAccent,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(12),
                    topRight: Radius.circular(12),
                  ),
                ),
                child: Align(
                  alignment: Alignment.center,
                  child: Text(
                    "ผลการทดสอบ",
                    style: TextStyle(
                        fontFamily: 'Itim',
                        color: Colors.black87,
                        fontSize: 35,
                        fontWeight: FontWeight.w600),
                  ),
                ),
              ),
              Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: Center(
                        child: Text(
                          'ชื่อเกมส์',
                          style: TextStyle(
                              fontFamily: 'Itim',
                            color: Colors.black87,
                            fontSize: 28,
                          )),
                      ),
                    ),
                      Expanded(
                      child:  Center(
                        child: Text(
                          'คะแนน',
                          style: TextStyle(
                              fontFamily: 'Itim',
                            color: Colors.black87,
                            fontSize: 28,
                          )),
                      ),
                    ),
                      Expanded(
                      child:  Center(
                        child: Text(
                          'เวลา',
                          style: TextStyle(
                              fontFamily: 'Itim',
                            color: Colors.black87,
                            fontSize: 28,
                          )),
                      ),
                    ),
                  
                  ],
                ),
              ...calAllTestScore().map((data) {
                return Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: Center(
                        child: Text(
                          '${data['Name']}',
                          style: TextStyle(
                              fontFamily: 'Itim',
                            color: Colors.black87,
                            fontSize: 28,
                          )),
                      ),
                    ),
                      Expanded(
                      child:  Center(
                        child: Text(
                          ' ${data['score'].toStringAsFixed(2)}',
                          style: TextStyle(
                              fontFamily: 'Itim',
                            color: Colors.black87,
                            fontSize: 28,
                          )),
                      ),
                    ),
                      Expanded(
                      child:  Center(
                        child: Text(
                          '${data['counttime'].toStringAsFixed(2)}',
                          style: TextStyle(
                              fontFamily: 'Itim',
                            color: Colors.black87,
                            fontSize: 28,
                          )),
                      ),
                    ),
                  
                  ],
                );
              }).toList()
            ],
          ),
        ),
      ),
    );
  }
}
