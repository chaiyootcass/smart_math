import 'dart:math' show Random;
import 'package:after_layout/after_layout.dart';

import 'package:bordered_text/bordered_text.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:just_audio/just_audio.dart';
import 'package:smart_math/game_modals/test_task.dart';
import 'package:smart_math/game_modals/test_task_exams.dart';
import 'package:smart_math/utility/custom_dialog.dart';
import 'package:tutorial_coach_mark/animated_focus_light.dart';
import 'package:tutorial_coach_mark/tutorial_coach_mark.dart';

import '../size_config.dart';

class TestCorrespondenceTask extends StatefulWidget {
  @override
  _TestAdditionTaskState createState() => _TestAdditionTaskState();
}

class _TestAdditionTaskState extends State<TestCorrespondenceTask>
    with AfterLayoutMixin<TestCorrespondenceTask> {
  final String namegame = "test_correspondence_task";
  final player = AudioPlayer();
//for example
  List<TargetFocus> targets = List();
  GlobalKey keyButton = GlobalKey();
  GlobalKey keyButton2 = GlobalKey();

  DateTime countTime = new DateTime.now(); //ไว้นับเวลา
  TestTask exams;
  int indexExam = 0; //index exams
  int lastIndex = -1;
  Random rd = new Random(); //for random colors
  List<int> choice = [];
  double passScore = 0.0;

  Container _woodnumber(int number) {
    return Container(
      height: 15*SizeConfig.heightMultiplier,
      width: 15*SizeConfig.widthMultiplier,
      child: Image.asset(
        'assets/games/numberident/number-circle-${number.toString()}.png',
        fit: BoxFit.contain,
      ),
    );
  }

  List<Widget> _getExam() {
    List<Widget> exam = new List<Widget>();
    Map<String, dynamic> ex = exams.exams.elementAt(indexExam);
    print('index : $indexExam');
    choice = ex['choice'];
    for (int i = 0; i < choice.length; i++) {
      exam.add(GestureDetector(
          onTap: () async {
            if (exams.exams[indexExam]['example'] == true) {
            } else {
              if (choice[i] == ex['ans']) {
                exams.correct++;
              } else {
                exams.fail++;
              }
              exams.time.add(
                  DateTime.now().difference(this.countTime).inMilliseconds /
                      1000);
              exams.ans.add(choice[i]);
              exams.sol.add(ex['ans']);
            }
            await playSound('assets/audio/buttonClick.mp3');
            setState(() {
              checkPromote();
            });
          },
          child: _woodnumber(choice[i])));
    }
     if (lastIndex != indexExam) {
      if (exams.exams[indexExam]['example'] == true) {
        playSound('assets/audio/example.mp3',
            'assets/audio/correspondence_task/countAndAnswer.mp3');
      } else {
        if (exams.exams[indexExam - 1]['example'] == true) {
          playSound('assets/audio/gotoRealQuestion.mp3',
              'assets/audio/correspondence_task/countAndAnswer.mp3');
        } else {
          playSound('assets/audio/correspondence_task/countAndAnswer.mp3');
        }
      }
    }
    lastIndex = indexExam;
    return exam;
  }

  Widget _showExam() {
    int count = exams.exams[indexExam]['ans'];
    if (count <= 3) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: List.generate(count, (_) {
          return Container(
            height: 35*SizeConfig.heightMultiplier,
            width: 35*SizeConfig.widthMultiplier,
            child: Image.asset(
              'assets/fruits/Asset${count - 1}.png',
              fit: BoxFit.contain,
            ),
          );
        }),
      );
    } else if (count <= 6) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: List.generate(3, (_) {
                return Container(
                  height: 17*SizeConfig.heightMultiplier,
                  width: 17*SizeConfig.widthMultiplier,
                  child: Image.asset(
                    'assets/fruits/Asset${count - 2}.png',
                    fit: BoxFit.contain,
                  ),
                );
              })),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: List.generate(count - 3, (_) {
              return Container(
                 height: 17*SizeConfig.heightMultiplier,
                  width: 17*SizeConfig.widthMultiplier,
                child: Image.asset(
                  'assets/fruits/Asset${count - 2}.png',
                  fit: BoxFit.contain,
                ),
              );
            }),
          )
        ],
      );
    } else if (count <= 13) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: List.generate(5, (_) {
                return Container(
                 height: 10*SizeConfig.heightMultiplier,
                  width: 10*SizeConfig.widthMultiplier,
                  child: Image.asset(
                    'assets/fruits/Asset5.png',
                    fit: BoxFit.scaleDown,
                  ),
                );
              })),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: List.generate(4, (_) {
              return Container(
                height: 10*SizeConfig.heightMultiplier,
                  width: 10*SizeConfig.widthMultiplier,
                child: Image.asset(
                  'assets/fruits/Asset5.png',
                  fit: BoxFit.scaleDown,
                ),
              );
            }),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: List.generate(4, (_) {
              return Container(
               height:10*SizeConfig.heightMultiplier,
                  width: 10*SizeConfig.widthMultiplier,
                child: Image.asset(
                  'assets/fruits/Asset5.png',
                  fit: BoxFit.scaleDown,
                ),
              );
            }),
          )
        ],
      );
    } else if (count <= 30) {
      int randomFruit = count == 27 ? 8 : 7;
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: List.generate(10, (_) {
                return Container(
                  height:10*SizeConfig.heightMultiplier,
                  width: 10*SizeConfig.widthMultiplier,
                  child: Image.asset(
                    'assets/fruits/Asset$randomFruit.png',
                    fit: BoxFit.scaleDown,
                  ),
                );
              })),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: List.generate(10, (_) {
              return Container(
                 height:10*SizeConfig.heightMultiplier,
                  width: 10*SizeConfig.widthMultiplier,
                child: Image.asset(
                  'assets/fruits/Asset$randomFruit.png',
                  fit: BoxFit.scaleDown,
                ),
              );
            }),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: List.generate(count - 20, (_) {
              return Container(
               height:10*SizeConfig.heightMultiplier,
                  width: 10*SizeConfig.widthMultiplier,
                child: Image.asset(
                  'assets/fruits/Asset$randomFruit.png',
                  fit: BoxFit.scaleDown,
                ),
              );
            }),
          )
        ],
      );
    }
  }

  @override
  void dispose() async {
    super.dispose();
    await player.dispose();
  }

void playSound(String sound,[String sound2]) async {
    await player.setAsset(sound);
    await player.play();
    if(sound2!=null){
      await player.setAsset(sound2);
      await player.play();
    }
  }
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        // title: Text(
        //   'Test Correspondence Task',
        //   style: TextStyle(color: Colors.black),
        // ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
            return;
          },
        ),
      ),
      body: Container(
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/bg/game_bg.jpg'), fit: BoxFit.cover),
          ),
          child: SafeArea(
            child: exams != null
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Container(
                              width: 90 * SizeConfig.widthMultiplier,
                          height: 7 * SizeConfig.heightMultiplier,
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                color: Color(0xffffd6c4),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                                ),
                            child: Center(
                              child: Text(
                                'ให้เด็กๆนับแล้วเลือกคำตอบ',
                                style: TextStyle(
                                  fontFamily: 'Itim',
                                  fontSize: 4 * SizeConfig.textMultiplier,
                                  color: Color(0xff17478c),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                            flex: 3,
                            child: exams != null
                                ? _showExam()
                                : Center(child: CircularProgressIndicator())),
                        Expanded(
                          flex: 1,
                          child: Row(
                            key: keyButton2,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: _getExam(),
                          ),
                        )
                      ])
                : Center(
                    child: CircularProgressIndicator(),
                  ),
          )),
    );
  }

  void checkPromote() {
    //ถ้าเล่นเกมส์ครบชุด
    if (indexExam == exams.exams.length - 1) {
      //task check correct null before put
      // List<dynamic> data = Hive.box('test_task').get('data') as List<dynamic>;
      // data.add({
      //   'namegame': namegame,
      //   'correct': exams.correct,
      //   'ans': exams.ans,
      //   'counttime': DateTime.now().difference(this.countTime).inSeconds
      // });
      //Hive.box('test_task').put('data', data);
      Hive.box('test_task').delete(namegame);
      Hive.box('test_task').put(namegame, {
        'userid': Hive.box('appData').get('_id'),
        'namegame': namegame,
        'correct': exams.correct,
        'score': exams.correct,
        'ans': exams.ans,
        'time': exams.time,
        'counttime': exams.allTime()
      });
      //TestTaskExams();
      print(Hive.box('test_task').get(namegame));
      showDialog(
          context: context,
          builder: (BuildContext context) => resultCustomDialog(
              context: context,
              score: exams.correct,
              lenght: exams.fail + exams.correct,
              passScore: passScore)).then((_) => Navigator.of(context).pop());
      return;
    }
    //เล่นยังไม่ครบ
    lastIndex = indexExam;
    indexExam++;
    countTime = new DateTime.now();
    if (exams.exams[indexExam]['example'] == true) {
      // Future.delayed(Duration(milliseconds: 100), () {
      //   showTutorial();
      // });
    }
    //  Hive.box('test_task').put('index', Hive.box('test_task').get('index') + 1);
  }

  @override
  void afterFirstLayout(BuildContext context) {
    setState(() {
      indexExam = 0;
      exams = TestTask();
      Map<String, dynamic> data = listexams.firstWhere((x) {
        return x['game'] == namegame;
      });
      exams.exams = data['exams'];
      passScore = data['passScore'];
      countTime = new DateTime.now();
      if (exams.exams[indexExam]['example'] == true) {
        // initTargets();
        // Future.delayed(Duration(milliseconds: 100), () {
        //   showTutorial();
        // });
      }
      print('passScore ' + passScore.toString());
    });
  }

  void initTargets() {
    targets.add(TargetFocus(
      identify: "Target 1",
      keyTarget: keyButton,
      contents: [
        ContentTarget(
            align: AlignContent.bottom,
            child: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "อ่านโจทย์ที่กำหนดให้",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 40.0),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: Text(
                      "เช็คคำสั่งของโจทย์ว่าให้เลือกอะไร",
                      style: TextStyle(color: Colors.white, fontSize: 40.0),
                    ),
                  )
                ],
              ),
            ))
      ],
      shape: ShapeLightFocus.RRect,
    ));
    targets.add(TargetFocus(
      identify: "Target 2",
      keyTarget: keyButton2,
      contents: [
        ContentTarget(
            align: AlignContent.bottom,
            child: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "ให้เด็กๆนับแล้วเลือกคำตอบ",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 40.0),
                  ),
                ],
              ),
            ))
      ],
      shape: ShapeLightFocus.RRect,
    ));
  }

  void showTutorial() {
    TutorialCoachMark(context,
        targets: targets,
        colorShadow: Colors.blueGrey,
        textSkip: "ข้าม",
        paddingFocus: 10,
        opacityShadow: 0.8, finish: () {
      print("finish");
      countTime = new DateTime.now();
    }, clickTarget: (target) {
      print(target);
    }, clickSkip: () {
      print("skip");
      countTime = new DateTime.now();
    })
      ..show();
  }
}
