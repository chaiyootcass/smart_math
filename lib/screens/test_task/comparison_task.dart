import 'dart:async';
import 'dart:convert';
import 'dart:math' show Random;

import 'package:bordered_text/bordered_text.dart';
import 'package:http/http.dart' as http;
import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:just_audio/just_audio.dart';
import 'package:smart_math/game_modals/test_task.dart';
import 'package:smart_math/game_modals/test_task_exams.dart';
import 'package:smart_math/utility/custom_dialog.dart';
import 'package:smart_math/utility/setting.dart';
import 'package:tutorial_coach_mark/animated_focus_light.dart';
import 'package:tutorial_coach_mark/tutorial_coach_mark.dart';

import '../size_config.dart';

class TestComparisonTask extends StatefulWidget {
  @override
  _TestComparisonTaskState createState() => _TestComparisonTaskState();
}

class _TestComparisonTaskState extends State<TestComparisonTask>
    with AfterLayoutMixin<TestComparisonTask> {
  final String namegame = "test_comparison_task";
  final player = AudioPlayer();
//for example
  List<TargetFocus> targets = List();
  GlobalKey keyButton = GlobalKey();
  GlobalKey keyButton2 = GlobalKey();
  bool boxempty = false;
  double passScore = 0.0;
  DateTime countTime = new DateTime.now(); //ไว้นับเวลา
  TestTask exams;
  Timer timeForBox;
  int indexExam = 0; //index exams
  int lastIndex = -1;
  Random rd = new Random(); //for random colors
  List<int> choice = [];

  List<Widget> _getExam(BuildContext context) {
    List<Widget> exam = new List<Widget>();
    Map<String, dynamic> ex = exams.exams.elementAt(indexExam);
    print('index : $indexExam');
    choice = ex['exam'].cast<int>();
    for (int i = 0; i < choice.length; i++) {
      exam.add(Expanded(
          flex: 1,
          child: GestureDetector(
            onTap: () async {
              if (exams.exams[indexExam]['example'] == true) {
              } else {
                if (choice[i] == ex['ans']) {
                  exams.correct++;
                } else {
                  exams.fail++;
                }
                exams.time.add(
                    DateTime.now().difference(this.countTime).inMilliseconds /
                        1000);
                exams.ans.add(choice[i]);
                exams.sol.add(ex['ans']);
              }
              await playSound('assets/audio/buttonClick.mp3');
              setState(() {
                checkPromote(context);
              });
            },
            child: boxempty
                ? Container(
                    // padding: EdgeInsets.only(top: 90, left: 10, right: 10),
                    height: 55 * SizeConfig.heightMultiplier,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(
                                'assets/games/comparisontark/box-closed.png'),
                            fit: BoxFit.contain)),
                    child: null)
                : Container(
                    padding: EdgeInsets.only(
                        top: 15.0 * SizeConfig.heightMultiplier,
                        left: 3.0 * SizeConfig.widthMultiplier,
                        right: 3.0 * SizeConfig.widthMultiplier),
                    height: 55 * SizeConfig.heightMultiplier,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(
                                'assets/games/comparisontark/box-empty.png'),
                            fit: BoxFit.contain)),
                    child: Center(
                      child: GridView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: choice[i],
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 5,
                            crossAxisSpacing: 2.0,
                            mainAxisSpacing: 2.0),
                        itemBuilder: (_, int index) {
                          return Image.asset(
                              'assets/fruits/Asset${indexExam + 1}.png',
                              fit: BoxFit.scaleDown);
                        },
                      ),
                    ),
                  ),
          )));
    }
    if (lastIndex != indexExam) {
      if (exams.exams[indexExam]['example'] == true) {
        playSound('assets/audio/example.mp3',
            'assets/audio/comparison_task/chooseGreaterBoxFemale.mp3');
      } else {
        if (exams.exams[indexExam - 1]['example'] == true) {
          playSound('assets/audio/gotoRealQuestion.mp3',
              'assets/audio/comparison_task/chooseGreaterBoxFemale.mp3');
        } else {
          playSound('assets/audio/comparison_task/chooseGreaterBoxFemale.mp3');
        }
      }
    }
    lastIndex = indexExam;
    return exam;
  }

  @override
  void dispose() async {
    super.dispose();
    await player.dispose();
  }

  void playSound(String sound, [String sound2]) async {
    await player.setAsset(sound);
    await player.play();
    if (sound2 != null) {
      await player.setAsset(sound2);
      await player.play();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        // title: Text(
        //   'Test Comparison Task',
        //   style: TextStyle(color: Colors.black),
        // ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
            return;
          },
        ),
      ),
      body: Container(
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(
                    'assets/games/counting/backgrounds/background-moutain.png'),
                fit: BoxFit.cover),
          ),
          child: SafeArea(
            child: exams != null
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          width: 90 * SizeConfig.widthMultiplier,
                          height: 7 * SizeConfig.heightMultiplier,
                          decoration: BoxDecoration(
                               color: Color(0xffffd6c4),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                              ),
                          child: Center(
                            child: Text(
                              'ให้เด็กๆเลือกกล่องที่มีของมากกว่า',
                              style: TextStyle(
                                fontFamily: 'Itim',
                                fontSize: 4 * SizeConfig.textMultiplier,
                                color: Color(0xff17478c),
                              ),
                            ),
                          ),
                        ),
                      ),
                      // SizedBox(
                      //   height: 5*SizeConfig.heightMultiplier,
                      // ),
                      Expanded(
                        flex: 5,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                                flex: 1,
                                child: Container(
                                    height: 40 * SizeConfig.heightMultiplier,
                                    child: Image.asset(
                                        'assets/games/comparisontark/char-${indexExam + 1}.png'))),
                            Expanded(
                              flex: 2,
                              key: keyButton2,
                              child: Row(
                                children: _getExam(context),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  )
                : Center(
                    child: CircularProgressIndicator(),
                  ),
          )),
    );
  }

  void checkPromote(BuildContext context) {
    boxempty = false;
    timeForBox?.cancel();
    //ถ้าเล่นเกมส์ครบชุด
    if (indexExam == exams.exams.length - 1) {
      //task check correct null before put
      // List<dynamic> data = Hive.box('test_task').get('data') as List<dynamic>;
      // data.add({
      //   'namegame': namegame,
      //   'correct': exams.correct,
      //   'ans': exams.ans,
      //   'counttime': DateTime.now().difference(this.countTime).inSeconds
      // });
      //Hive.box('test_task').put('data', data);
      Hive.box('test_task').delete(namegame);
      Hive.box('test_task').put(namegame, {
        'userid': Hive.box('appData').get('_id'),
        'namegame': namegame,
        'correct': exams.correct,
        'score': exams.correct,
        'ans': exams.ans,
        'sol': exams.sol,
        'time': exams.time,
        'counttime': exams.allTime()
      });
      //TestTaskExams();
      print(Hive.box('test_task').get(namegame));
      http.post(
        '$serverUrl/test_task/add',
        body: json.encode(Hive.box('test_task').get(namegame)),
        headers: {
          "content-type": "application/json",
          "accept": "application/json",
        },
      );
      //  print('indexttest :' + Hive.box('test_task').get('index').toString());
      //เปลี่ยนเป็นโชว์สรุป
      showDialog(
          context: context,
          builder: (BuildContext context) => resultCustomDialog(
              context: context,
              score: exams.correct,
              lenght: exams.fail + exams.correct,
              passScore: passScore)).then((_) => Navigator.of(context).pop());
      return;
    }
    //เล่นยังไม่ครบ
    indexExam++;
    countTime = new DateTime.now();
    if (exams.exams[indexExam]['example'] == true) {
      // Future.delayed(Duration(milliseconds: 100), () {
      //   showTutorial();
      // });
    }
    //ให้ปิดกล้องหลังเวลาผ่านไป 3 วินาที
    timeForBox = Timer(Duration(seconds: 3), () {
      setState(() {
        boxempty = true;
      });
    });
    //  Hive.box('test_task').put('index', Hive.box('test_task').get('index') + 1);
  }

  @override
  void afterFirstLayout(BuildContext context) {
    setState(() {
      indexExam = 0;
      exams = TestTask();
      Map<String, dynamic> data = listexams.firstWhere((x) {
        return x['game'] == namegame;
      });
      exams.exams = data['exams'];
      passScore = data['passScore'];
      countTime = new DateTime.now();
      if (exams.exams[indexExam]['example'] == true) {
        // initTargets();
        // Future.delayed(Duration(milliseconds: 100), () {
        //   showTutorial();
        // });
      }
      print('passScore ' + passScore.toString());
    });
  }

  void initTargets() {
    targets.add(TargetFocus(
      identify: "Target 1",
      keyTarget: keyButton,
      contents: [
        ContentTarget(
            align: AlignContent.bottom,
            child: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "อ่านโจทย์ที่กำหนดให้",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 40.0),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: Text(
                      "เช็คคำสั่งของโจทย์ว่าให้เลือกอะไร ต้องเลือกกล่องส้มที่มีมากกว่า",
                      style: TextStyle(color: Colors.white, fontSize: 40.0),
                    ),
                  )
                ],
              ),
            ))
      ],
      shape: ShapeLightFocus.RRect,
    ));
    targets.add(TargetFocus(
      identify: "Target 2",
      keyTarget: keyButton2,
      contents: [
        ContentTarget(
            align: AlignContent.bottom,
            child: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "เลือกกล่องที่มีส้มมากกว่า",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 40.0),
                  ),
                ],
              ),
            ))
      ],
      shape: ShapeLightFocus.RRect,
    ));
  }

  void showTutorial() {
    TutorialCoachMark(context,
        targets: targets,
        colorShadow: Colors.blueGrey,
        textSkip: "ข้าม",
        paddingFocus: 10,
        opacityShadow: 0.8, finish: () {
      print("finish");
      countTime = new DateTime.now();
    }, clickTarget: (target) {
      print(target);
    }, clickSkip: () {
      print("skip");
      countTime = new DateTime.now();
    })
      ..show();
  }
}
