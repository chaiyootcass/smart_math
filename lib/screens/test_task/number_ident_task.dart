import 'dart:convert';
import 'dart:math' show Random;
import 'package:bordered_text/bordered_text.dart';
import 'package:http/http.dart' as http;
import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:just_audio/just_audio.dart';
import 'package:smart_math/game_modals/test_task.dart';
import 'package:smart_math/game_modals/test_task_exams.dart';
import 'package:smart_math/utility/custom_dialog.dart';
import 'package:smart_math/utility/numberToThai.dart';
import 'package:smart_math/utility/setting.dart';
import 'package:tutorial_coach_mark/animated_focus_light.dart';
import 'package:tutorial_coach_mark/tutorial_coach_mark.dart';

import '../size_config.dart';

class TestNumberidentTask extends StatefulWidget {
  @override
  _TestNumberidentTaskState createState() => _TestNumberidentTaskState();
}

class _TestNumberidentTaskState extends State<TestNumberidentTask>
    with AfterLayoutMixin<TestNumberidentTask> {
  final String namegame = "test_numberident_task";
final player = AudioPlayer();
//for example
  List<TargetFocus> targets = List();
  GlobalKey keyButton = GlobalKey();
  GlobalKey keyButton2 = GlobalKey();

  DateTime countTime = new DateTime.now(); //ไว้นับเวลา
  TestTask exams;
  int indexExam = 0; //index exams
  int lastIndex = -1;
  Random rd = new Random(); //for random colors
  List<int> choice = [];
  int chooseAns = -1;
  double passScore = 0.0;

  Container _woodnumber(int number) {
    return Container(
        height: 11*SizeConfig.heightMultiplier,
        width: 18*SizeConfig.widthMultiplier,
        child: chooseAns == number
            ? Image.asset(
                'assets/games/numberident/number-circle-${number.toString()}.png',
                fit: BoxFit.contain,
                color: Colors.brown.withOpacity(0.4),
                colorBlendMode: BlendMode.srcATop)
            : Image.asset(
                'assets/games/numberident/number-circle-${number.toString()}.png',
                fit: BoxFit.contain,
              ));
  }

  List<Widget> _getExam() {
    List<Widget> exam = new List<Widget>();
    List<Widget> tem = new List<Widget>();
    Map<String, dynamic> ex = exams.exams.elementAt(indexExam);
    print('index : $indexExam');
    if (ex['ans'] <= 9) {
      print(ex);
      for (int i = 1; i < 10; i++) {
        tem.add(GestureDetector(
            onTap: () async {
              setState(() {
                chooseAns = i;
                playSound('assets/audio/buttonClick.mp3');
              });
            },
            child: _woodnumber(i)));
        if (i == 3 || i == 6 || i == 9) {
          exam.add(Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: tem,
          ));
          tem = [];
        }
      }
      exam.add(Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          GestureDetector(
              onTap: () {
                setState(() {
                  chooseAns = 0;
                });
              },
              child: _woodnumber(0))
        ],
      ));
    } else {
      exam.add(Container(
        height: 40*SizeConfig.heightMultiplier,
        width: double.infinity,
        child: GridView.builder(
          physics: NeverScrollableScrollPhysics(),
          itemCount: 40,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 10, crossAxisSpacing: 2.0, mainAxisSpacing: 2.0),
          itemBuilder: (_, int index) {
            return GestureDetector(
                onTap: () {
                  setState(() {
                    chooseAns = index + 1;
                  });
                },
                child: Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage(
                          'assets/games/numberident/bird-cartoon.png',
                        ),
                        fit: BoxFit.cover,
                        colorFilter: chooseAns == (index + 1)
                            ? ColorFilter.mode(Colors.brown.withOpacity(0.4),
                                BlendMode.srcATop)
                            : null),
                  ),
                  child: Center(
                    child: Text(
                      (index + 1).toString(),
                      style:
                          TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                    ),
                  ),
                ));
          },
        ),
      ));
    }
     if (lastIndex != indexExam) {
      if (ex['example'] == true) {
        playSound('assets/audio/numberident_task/pressThisNumberFemale.mp3',
        'assets/audio/numberident_task/number-${ex['ans']}.mp3','assets/audio/letSeeExample.mp3');
      } else {
       playSound('assets/audio/numberident_task/pressThisNumberFemale.mp3',
        'assets/audio/numberident_task/number-${ex['ans']}.mp3');
      }
    }
    lastIndex = indexExam;
    return exam;
  }

@override
  void dispose() async {
    super.dispose();
    await player.dispose();
  }

void playSound(String sound,[String sound2,sound1]) async {
   if(sound1!=null){
      await player.setAsset(sound1);
      await player.play();
    }
    await player.setAsset(sound);
    await player.play();
    if(sound2!=null){
      await player.setAsset(sound2);
      await player.play();
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(

        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
            return;
          },
        ),
      ),
      body: Container(
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/bg/game_bg.jpg'), fit: BoxFit.cover),
          ),
          child: SafeArea(
            child: exams != null
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                        Expanded(
                            flex:1,
                            child: Container(
                              margin: EdgeInsets.symmetric(
                                  vertical: 2 * SizeConfig.heightMultiplier,
                                  horizontal: 20 * SizeConfig.widthMultiplier),
                              width: double.infinity,
                              height: 7 * SizeConfig.heightMultiplier,
                              decoration: BoxDecoration(
                                 color: Color(0xffffd6c4),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10.0)),
                                 ),
                              child: Center(
                                key: keyButton,
                                child: Text(
                                  'กดเลขที่กำหนดนะเด็กๆ ... ${numberToThai(exams.exams[indexExam]['ans'])}',
                                  style: TextStyle(
                                    fontFamily: 'Itim',
                                    fontSize: 4 * SizeConfig.textMultiplier,
                                    color: Color(0xff17478c),
                                  ),
                                ),
                              ),
                            )),
                        Expanded(
                            flex: 4,
                            child: Stack(
                              children: <Widget>[
                                Column(
                                  children: _getExam(),
                                ),
                                Align(
                                  alignment: Alignment(0.8,0.8),
                                  child: GestureDetector(
                                    onTap: () async {
                                      if (chooseAns == -1) return;
                                      await playSound('assets/audio/soundclickans.mp3');
                                      if (exams.exams[indexExam]['example'] ==
                                          true) {
                                      } else {
                                        if (chooseAns ==
                                            exams.exams
                                                .elementAt(indexExam)['ans']) {
                                          exams.correct++;
                                        } else {
                                          exams.fail++;
                                        }
                                        exams.time.add(DateTime.now()
                                                .difference(this.countTime)
                                                .inMilliseconds /
                                            1000);
                                        exams.ans.add(chooseAns);
                                        exams.sol.add(exams.exams
                                            .elementAt(indexExam)['ans']);
                                      }
                                      setState(() {
                                        checkPromote();
                                      });
                                    },
                                    child: Image.asset(
                                      'assets/buttons/ok.png',
                                      width: 20 * SizeConfig.widthMultiplier,
                                      fit: BoxFit.contain,
                                    ),
                                  ),
                                ),
                              ],
                            )),
                      ])
                : Center(
                    child: CircularProgressIndicator(),
                  ),
          )),
    );
  }

  void checkPromote() {
    //ถ้าเล่นเกมส์ครบชุด
    if (indexExam == exams.exams.length - 1) {
      //task check correct null before put
      // List<dynamic> data = Hive.box('test_task').get('data') as List<dynamic>;
      // data.add({
      //   'namegame': namegame,
      //   'correct': exams.correct,
      //   'ans': exams.ans,
      //   'counttime': DateTime.now().difference(this.countTime).inSeconds
      // });
      //Hive.box('test_task').put('data', data);
      Hive.box('test_task').delete(namegame);
      Hive.box('test_task').put(namegame, {
        'userid': Hive.box('appData').get('_id'),
        'namegame': namegame,
        'correct': exams.correct,
        'score': exams.correct,
        'ans': exams.ans,
        'sol': exams.sol,
        'time': exams.time,
        'counttime': exams.allTime()
      });
      //TestTaskExams();
      print(Hive.box('test_task').get(namegame));
      http.post(
        '$serverUrl/test_task/add',
        body: json.encode(Hive.box('test_task').get(namegame)),
        headers: {
          "content-type": "application/json",
          "accept": "application/json",
        },
      );
      showDialog(
          context: context,
          builder: (BuildContext context) => resultCustomDialog(
              context: context,
              score: exams.correct,
              lenght: exams.fail + exams.correct,
              passScore: passScore)).then((_) => Navigator.of(context).pop());
      return;
    }
    //เล่นยังไม่ครบ
    lastIndex = indexExam;
    indexExam++;
    chooseAns = -1;
    countTime = new DateTime.now();
    // if (exams.exams[indexExam]['example'] == true) {
    //   Future.delayed(Duration(milliseconds: 100), () {
    //     showTutorial();
    //   });
    // }
  }

  @override
  void afterFirstLayout(BuildContext context) {
    setState(() {
      indexExam = 0;
      exams = TestTask();
      Map<String, dynamic> data = listexams.firstWhere((x) {
        return x['game'] == namegame;
      });
      exams.exams = data['exams'];
      passScore = data['passScore'];
      countTime = new DateTime.now();
      if (exams.exams[indexExam]['example'] == true) {
        // initTargets();
        // Future.delayed(Duration(milliseconds: 100), () {
        //   showTutorial();
        // });
      }
      print('passScore ' + passScore.toString());
    });
  }

  void initTargets() {
    targets.add(TargetFocus(
      identify: "Target 1",
      keyTarget: keyButton,
      contents: [
        ContentTarget(
            align: AlignContent.bottom,
            child: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "อ่านโจทย์ที่กำหนดให้",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 40.0),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: Text(
                      "เช็คคำสั่งของโจทย์ว่าให้เลือกอะไร ต้องเลือกกล่องส้มที่มีมากกว่า",
                      style: TextStyle(color: Colors.white, fontSize: 40.0),
                    ),
                  )
                ],
              ),
            ))
      ],
      shape: ShapeLightFocus.RRect,
    ));
    targets.add(TargetFocus(
      identify: "Target 2",
      keyTarget: keyButton2,
      contents: [
        ContentTarget(
            align: AlignContent.bottom,
            child: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "เลือกกล่องที่มีส้มมากกว่า",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 40.0),
                  ),
                ],
              ),
            ))
      ],
      shape: ShapeLightFocus.RRect,
    ));
  }

  void showTutorial() {
    TutorialCoachMark(context,
        targets: targets,
        colorShadow: Colors.blueGrey,
        textSkip: "ข้าม",
        paddingFocus: 10,
        opacityShadow: 0.8, finish: () {
      print("finish");
      countTime = new DateTime.now();
    }, clickTarget: (target) {
      print(target);
    }, clickSkip: () {
      print("skip");
      countTime = new DateTime.now();
    })
      ..show();
  }
}
