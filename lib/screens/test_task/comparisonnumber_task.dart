import 'dart:async';
import 'dart:convert';
import 'dart:math' show Random;

import 'package:bordered_text/bordered_text.dart';
import 'package:http/http.dart' as http;
import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:random_color/random_color.dart';
import 'package:smart_math/game_modals/test_task.dart';
import 'package:smart_math/game_modals/test_task_exams.dart';
import 'package:smart_math/utility/custom_dialog.dart';
import 'package:smart_math/utility/setting.dart';
import 'package:tutorial_coach_mark/animated_focus_light.dart';
import 'package:tutorial_coach_mark/tutorial_coach_mark.dart';

import '../size_config.dart';

class TestComparisonNumber extends StatefulWidget {
  @override
  _TestComparisonNumberState createState() => _TestComparisonNumberState();
}

class _TestComparisonNumberState extends State<TestComparisonNumber>
    with AfterLayoutMixin<TestComparisonNumber> {
  final String namegame = "test_comparisonnumber_task";

//for example
  List<TargetFocus> targets = List();
  GlobalKey keyButton = GlobalKey();
  GlobalKey keyButton2 = GlobalKey();

  RandomColor _randomColor = RandomColor();
  List<Color> boxColors = [];
  DateTime countTime = new DateTime.now(); //ไว้นับเวลา
  TestTask exams;
  bool boxempty = false;
  Timer timeForBox;
  int indexExam = 0; //index exams
  double passScore = 0.0;

  Random rd = new Random(); //for random colors
  List<dynamic> choice = [];
  List<Widget> getExamContainer() {
    List<Widget> exam = new List<Widget>();
    Map<String, dynamic> ex = exams.exams.elementAt(indexExam);
    choice = ex['exam'];

    for (int i = 0; i < 2; i++) {
      switch (i) {
        case 0:
          boxColors[0] = _randomColor.randomColor(colorHue: ColorHue.red);
          break;
        case 1:
          boxColors[1] = _randomColor.randomColor(colorHue: ColorHue.green);
          break;
        default:
      }
      exam.add(GestureDetector(
        onTap: () {
          if (exams.exams[indexExam]['example'] == true) {
          } else {
            if (choice[i] == ex['ans']) {
              exams.correct++;
            } else {
              exams.fail++;
            }
            exams.time.add(
                DateTime.now().difference(this.countTime).inMilliseconds /
                    1000);
            exams.ans.add(choice[i]);
            exams.sol.add(ex['ans']);
          }
          setState(() {
            checkPromote();
          });
        },
        child: Container(
          width: 40 * SizeConfig.widthMultiplier,
          height: 45 * SizeConfig.heightMultiplier,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image:
                      AssetImage('assets/games/comparisontark/box-empty.png'),
                  fit: BoxFit.contain)),
          child: boxempty
              ? Container(
                  width: 40 * SizeConfig.widthMultiplier,
                  height: 45 * SizeConfig.heightMultiplier,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage(
                              'assets/games/comparisontark/box-closed.png'),
                          fit: BoxFit.contain)),
                  child: null)
              : Center(
                  child: BorderedText(
                    strokeWidth: 8.0,
                    strokeColor: Colors.black,
                    child: Text(
                      choice[i].toString(),
                      style: TextStyle(
                        fontSize: 20 * SizeConfig.textMultiplier,
                        color: _randomColor.randomColor(),
                      ),
                    ),
                  ),
                ),
        ),
      ));
    }
    return exam;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          // title: Text(
          //   'Test Comparison Number',
          //   style: TextStyle(color: Colors.black),
          // ),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Hive.box('test_task').delete(namegame);
              Navigator.pop(context);
              return;
            },
          ),
        ),
        body: Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/bg/game_bg.jpg'),
                  fit: BoxFit.cover),
            ),
            child: SafeArea(
              child: exams != null
                  ? Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          height: 7 * SizeConfig.heightMultiplier,
                          decoration: BoxDecoration(
                              color: Color(0xffffd6c4),
                              borderRadius: BorderRadius.circular(10.0),
                             ),
                          child: Center(
                            child: Text(
                              'เลือกตัวเลขในกล่องที่แทนจำนวนที่มากกว่านะจ๊ะ',
                              style: TextStyle(
                                fontFamily: 'Itim',
                                fontSize: 4 * SizeConfig.textMultiplier,
                                 color: Color(0xff17478c),
                              ),
                            ),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                                flex: 1,
                                child: Container(
                                    height: 35 * SizeConfig.heightMultiplier,
                                    child: Image.asset(
                                      'assets/games/comparisontark/char-${indexExam + 1}.png',
                                      fit: BoxFit.contain,
                                    ))),
                            Expanded(
                              flex: 2,
                              key: keyButton2,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: getExamContainer(),
                              ),
                            )
                          ],
                        ),
                      ],
                    )
                  : Center(child: CircularProgressIndicator()),
            )));
  }

  void checkPromote() {
    boxempty = false;
    timeForBox?.cancel();
    //ถ้าเล่นเกมส์ครบชุด
    if (indexExam == exams.exams.length - 1) {
      print('upload log');
      //เล่นเกมส์ครบชุด ส่ง log
      // List<dynamic> data = Hive.box('test_task').get('data') as List<dynamic>;
      // data.add({
      //   'namegame': namegame,
      //   'correct': exams.correct,
      //   'ans': exams.ans,
      //   'counttime': DateTime.now().difference(this.countTime).inSeconds
      // });
      //Hive.box('test_task').put('data', data);
      Hive.box('test_task').delete(namegame);
      Hive.box('test_task').put(namegame, {
        'userid': Hive.box('appData').get('_id'),
        'namegame': namegame,
        'correct': exams.correct,
        'score': exams.correct,
        'ans': exams.ans,
        'sol': exams.sol,
        'time': exams.time,
        'counttime': exams.allTime()
      });
      //TestTaskExams();
      print(Hive.box('test_task').get(namegame));
      http.post(
        '$serverUrl/test_task/add',
        body: json.encode(Hive.box('test_task').get(namegame)),
        headers: {
          "content-type": "application/json",
          "accept": "application/json",
        },
      );
      //เปลี่ยนเป็นโชว์สรุป
      showDialog(
          context: context,
          builder: (BuildContext context) => resultCustomDialog(
              context: context,
              score: exams.correct,
              lenght: exams.fail + exams.correct,
              passScore: passScore)).then((_) => Navigator.of(context).pop());
      return;
    }
    indexExam++;
    countTime = new DateTime.now();
    //if (exams.exams[indexExam]['example'] == true) {
      // Future.delayed(Duration(milliseconds: 100), () {
      //   showTutorial();
      // });
    //}
    timeForBox = Timer(Duration(seconds: 3), () {
      setState(() {
        boxempty = true;
      });
    });
    //Hive.box('test_task').put('index', Hive.box('test_task').get('index') + 1);
  }

  @override
  void afterFirstLayout(BuildContext context) {
    
    setState(() {
      indexExam = 0;
      boxColors.add(_randomColor.randomColor(colorHue: ColorHue.red));
      boxColors.add(_randomColor.randomColor(colorHue: ColorHue.green));
      exams = TestTask();
      Map<String, dynamic> data = listexams.firstWhere((x) {
        return x['game'] == namegame;
      });
      exams.exams = data['exams'];
      passScore = data['passScore'];
      countTime = new DateTime.now();
      if (exams.exams[indexExam]['example'] == true) {
        // initTargets();
        // Future.delayed(Duration(milliseconds: 100), () {
        //   showTutorial();
        // });
      }
      print('passScore ' + passScore.toString());
    });
  }

  void initTargets() {
    targets.add(TargetFocus(
      identify: "Target 1",
      keyTarget: keyButton,
      contents: [
        ContentTarget(
            align: AlignContent.bottom,
            child: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "อ่านโจทย์ที่กำหนด",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 40.0),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: Text(
                      "เช็คคำสั่งของโจทย์ว่าให้เลือกอะไร ต้องเลือกเลขที่มีค่ามากกว่า",
                      style: TextStyle(color: Colors.white, fontSize: 40.0),
                    ),
                  )
                ],
              ),
            ))
      ],
      shape: ShapeLightFocus.RRect,
    ));
    targets.add(TargetFocus(
      identify: "Target 2",
      keyTarget: keyButton2,
      contents: [
        ContentTarget(
            align: AlignContent.bottom,
            child: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "เลือกกล้องที่เลขมากกว่า",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 40.0),
                  ),
                ],
              ),
            ))
      ],
      shape: ShapeLightFocus.RRect,
    ));
  }

  void showTutorial() {
    TutorialCoachMark(context,
        targets: targets,
        colorShadow: Colors.blueGrey,
        textSkip: "ข้าม",
        paddingFocus: 10,
        opacityShadow: 0.8, finish: () {
      print("finish");
      countTime = new DateTime.now();
    }, clickTarget: (target) {
      print(target);
    }, clickSkip: () {
      print("skip");
      countTime = new DateTime.now();
    })
      ..show();
  }
}
