import 'dart:math' show Random;
import 'package:after_layout/after_layout.dart';
import 'package:bordered_text/bordered_text.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:just_audio/just_audio.dart';
import 'package:smart_math/game_modals/test_task.dart';
import 'package:smart_math/game_modals/test_task_exams.dart';
import 'package:smart_math/utility/numberToThai.dart';
import 'package:tutorial_coach_mark/animated_focus_light.dart';
import 'package:tutorial_coach_mark/tutorial_coach_mark.dart';

import '../size_config.dart';

class TestOrdinalATask extends StatefulWidget {
  @override
  _TestOrainalATaskState createState() => _TestOrainalATaskState();
}

class _TestOrainalATaskState extends State<TestOrdinalATask>
    with AfterLayoutMixin<TestOrdinalATask> {
  final String namegame = "test_ordinal_a_task";
  final player = AudioPlayer();

  //for example
  List<TargetFocus> targets = List();
  GlobalKey keyButton = GlobalKey();
  GlobalKey keyButton2 = GlobalKey();

  DateTime countTime = new DateTime.now(); //ไว้นับเวลา
  TestTask exams;
  int indexExam = 0; //index exams
  int lastIndex = -1;
  Random rd = new Random(); //for random colors
  List<int> choice = [];
  int chooseAns = -1;

  List<Widget> _getExam() {
    List<Widget> exam = new List<Widget>();
    Map<String, dynamic> ex = exams.exams.elementAt(indexExam);
    print('index : $indexExam');
    for (int i = 0; i < ex['exam']; i++) {
      exam.add(GestureDetector(
          onTap: () {
            setState(() {
              chooseAns = i;
              playSound('assets/audio/buttonClick.mp3');
            });
          },
          child: Column(
            children: <Widget>[
              Icon(
                Icons.arrow_downward,
                color: i == 0 ? Colors.red : Colors.transparent,
                size: 10 * SizeConfig.textMultiplier,
              ),
              Container(
                height: 14 * SizeConfig.heightMultiplier,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5.0),
                  color: i == chooseAns ? Colors.brown.withOpacity(0.6) : null,
                ),
                child: Image.asset(
                  'assets/animals/${ex['animal']}.png',
                  fit: BoxFit.contain,
                ),
              ),
            ],
          )));
    }

    if (lastIndex != indexExam) {
      if (ex['example'] == true) {
        playSound(
            'assets/audio/ordinal_task/question$indexExam.mp3',
            'assets/audio/ordinal_task/gotAnswerChooseAnimalBelow.mp3',
            'assets/audio/letSeeExample.mp3');
      } else {
        playSound('assets/audio/ordinal_task/question$indexExam.mp3',
            'assets/audio/ordinal_task/gotAnswerChooseAnimalBelow.mp3');
      }
    }
    lastIndex = indexExam;
    return exam;
  }

  @override
  void dispose() async {
    super.dispose();
    await player.dispose();
  }

  void playSound(String sound, [String sound2, sound1]) async {
    if (sound1 != null) {
      await player.setAsset(sound1);
      await player.play();
    }
    await player.setAsset(sound);
    await player.play();
    if (sound2 != null) {
      await player.setAsset(sound2);
      await player.play();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        // title: Text(
        //   'Test Ordinal A Task',
        //   style: TextStyle(color: Colors.black),
        // ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
            return;
          },
        ),
      ),
      body: Container(
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/bg/game_bg.jpg'), fit: BoxFit.cover),
          ),
          child: SafeArea(
            child: exams != null
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 20 * SizeConfig.widthMultiplier),
                            child: Container(
                              height: 5 * SizeConfig.heightMultiplier,
                              decoration: BoxDecoration(
                                   color: Color(0xffffd6c4),
                                  borderRadius: BorderRadius.circular(10.0),
                                  ),
                              child: Center(
                                child: Text(
                                  '${exams.exams[indexExam]['animal']}ตัวที่${numberToThai(exams.exams[indexExam]['ans'])}อยู่ตรงไหนเอ่ย',
                                  style: TextStyle(
                                    fontFamily: 'Itim',
                                    fontSize: 4 * SizeConfig.textMultiplier,
                                    color: Color(0xff17478c),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                            flex: 4,
                            child: Stack(
                              children: <Widget>[
                                exams.exams[indexExam]['exam']>9?
                                 SingleChildScrollView(
                                  scrollDirection: Axis.horizontal,
                                  child: Padding(
                                    padding: EdgeInsets.only(
                                        top: 9 * SizeConfig.heightMultiplier),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: _getExam(),
                                    ),
                                  ),
                                ):
                                 Padding(
                                   padding: EdgeInsets.only(
                                       top: 9 * SizeConfig.heightMultiplier),
                                   child: Row(
                                     mainAxisAlignment:
                                         MainAxisAlignment.center,
                                     crossAxisAlignment:
                                         CrossAxisAlignment.center,
                                     children: _getExam(),
                                   ),
                                 ),
                               
                                Align(
                                     alignment: Alignment(0.8,0.8),
                                  child: GestureDetector(
                                    onTap: () {
                                      if (chooseAns == -1) return;
                                      if (exams.exams[indexExam]['example'] ==
                                          true) {
                                      } else {
                                        if (chooseAns ==
                                            exams.exams
                                                .elementAt(indexExam)['ans']) {
                                          exams.correct++;
                                        } else {
                                          exams.fail++;
                                        }
                                        exams.time.add(DateTime.now()
                                                .difference(this.countTime)
                                                .inMilliseconds /
                                            1000);
                                        exams.ans.add(chooseAns);
                                        exams.sol.add(exams.exams
                                            .elementAt(indexExam)['ans']);
                                      }
                                      setState(() {
                                        checkPromote();
                                      });
                                    },
                                    child: Image.asset(
                                      'assets/buttons/ok.png',
                                      width: 20 * SizeConfig.widthMultiplier,
                                      fit: BoxFit.contain,
                                    ),
                                  ),
                                )
                              ],
                            )),
                      ])
                : Center(
                    child: CircularProgressIndicator(),
                  ),
          )),
    );
  }

  void checkPromote() {
    //ถ้าเล่นเกมส์ครบชุด
    if (indexExam == exams.exams.length - 1) {
      //task check correct null before put
      // List<dynamic> data = Hive.box('test_task').get('data') as List<dynamic>;
      // data.add({
      //   'namegame': namegame,
      //   'correct': exams.correct,
      //   'ans': exams.ans,
      //   'counttime': DateTime.now().difference(this.countTime).inSeconds
      // });
      //Hive.box('test_task').put('data', data);
      Hive.box('test_task').delete(namegame);
      Hive.box('test_task').put(namegame, {
        'userid': Hive.box('appData').get('_id'),
        'namegame': namegame,
        'correct': exams.correct,
        'score': exams.correct,
        'ans': exams.ans,
        'time': exams.time,
        'counttime': exams.allTime()
      });
      //TestTaskExams();
      print(Hive.box('test_task').get(namegame));
      //เปลี่ยนเป็นโชว์สรุป
      Navigator.of(context).popAndPushNamed('/test_ordinal_b_task', arguments: {
        'correct': exams.correct,
        'lenght': exams.correct + exams.fail,
        'time': exams.allTime()
      });
      return;
    }
    //เล่นยังไม่ครบ
    lastIndex = indexExam;
    chooseAns = -1;
    indexExam++;
    countTime = new DateTime.now();
    if (exams.exams[indexExam]['example'] == true) {
      // Future.delayed(Duration(milliseconds: 100), () {
      //   showTutorial();
      // });
    }
    //  Hive.box('test_task').put('index', Hive.box('test_task').get('index') + 1);
  }

  @override
  void afterFirstLayout(BuildContext context) {
    setState(() {
      indexExam = 0;
      print('indexExam ' + indexExam.toString());
      print('indextest ' + Hive.box('test_task').get('index').toString());
      exams = TestTask();
      exams.exams = listexams.firstWhere((x) {
        return x['game'] == namegame;
      })['exams'];
      countTime = new DateTime.now();
      if (exams.exams[indexExam]['example'] == true) {
        // initTargets();
        // Future.delayed(Duration(milliseconds: 100), () {
        //   showTutorial();
        // });
      }
    });
  }

  void initTargets() {
    targets.add(TargetFocus(
      identify: "Target 1",
      keyTarget: keyButton,
      contents: [
        ContentTarget(
            align: AlignContent.bottom,
            child: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "อ่านโจทย์ที่กำหนดให้",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 40.0),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: Text(
                      "เช็คคำสั่งของโจทย์ว่าให้เลือกอะไร",
                      style: TextStyle(color: Colors.white, fontSize: 40.0),
                    ),
                  )
                ],
              ),
            ))
      ],
      shape: ShapeLightFocus.RRect,
    ));
    targets.add(TargetFocus(
      identify: "Target 2",
      keyTarget: keyButton2,
      contents: [
        ContentTarget(
            align: AlignContent.bottom,
            child: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "เลือกผลรวมของแอปเปิ้ล",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 40.0),
                  ),
                ],
              ),
            ))
      ],
      shape: ShapeLightFocus.RRect,
    ));
  }

  void showTutorial() {
    TutorialCoachMark(context,
        targets: targets,
        colorShadow: Colors.blueGrey,
        textSkip: "ข้าม",
        paddingFocus: 10,
        opacityShadow: 0.8, finish: () {
      print("finish");
      countTime = new DateTime.now();
    }, clickTarget: (target) {
      print(target);
    }, clickSkip: () {
      print("skip");
      countTime = new DateTime.now();
    })
      ..show();
  }
}
