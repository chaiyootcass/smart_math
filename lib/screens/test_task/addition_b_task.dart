import 'dart:math' show Random;
import 'package:after_layout/after_layout.dart';

import 'package:bordered_text/bordered_text.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:just_audio/just_audio.dart';
import 'package:smart_math/game_modals/test_task.dart';
import 'package:smart_math/game_modals/test_task_exams.dart';
import 'package:smart_math/utility/custom_dialog.dart';
import 'package:tutorial_coach_mark/animated_focus_light.dart';
import 'package:tutorial_coach_mark/tutorial_coach_mark.dart';

import '../size_config.dart';

class TestAdditionBTask extends StatefulWidget {
  @override
  _TestAdditionBTaskState createState() => _TestAdditionBTaskState();
}

class _TestAdditionBTaskState extends State<TestAdditionBTask>
    with AfterLayoutMixin<TestAdditionBTask> {
  final String namegame = "test_addition_b_task";
  final player = AudioPlayer();

  Map<String, dynamic> additionAScore;
//for example
  List<TargetFocus> targets = List();
  GlobalKey keyButton = GlobalKey();
  GlobalKey keyButton2 = GlobalKey();

  DateTime countTime = new DateTime.now(); //ไว้นับเวลา
  TestTask exams;
  int indexExam = 0; //index exams
  int lastIndex = -1;
  Random rd = new Random(); //for random colors
  List<int> choice = [];
  int chooseAns = -1;
  double passScore = 0.0;

  List<Widget> _getExam() {
    List<Widget> exam = new List<Widget>();
    Map<String, dynamic> ex = exams.exams.elementAt(indexExam);
    print('index : $indexExam');
    choice = ex['choice'];
    for (int i = 0; i < choice.length; i++) {
      exam.add(GestureDetector(
          onTap: () {
            setState(() {
              chooseAns = choice[i];
              playSound('assets/audio/buttonClick.mp3');
            });
          },
          child: Container(
            decoration: BoxDecoration(
                color: chooseAns == choice[i]
                    ? Colors.orangeAccent
                    : Colors.grey[200],
                boxShadow: const [BoxShadow(blurRadius: 5)],
                borderRadius: BorderRadius.all(Radius.circular(18))),
            child: Center(
                child: textWithStroke(
                    fontSize: 12 * SizeConfig.textMultiplier,
                    text: choice[i].toString(),
                    textColor: Colors.blue,
                    strokeWidth: 10,
                    strokeColor: Colors.blueGrey)),
          )));
    }
     if (lastIndex != indexExam) {
      if (exams.exams[indexExam]['example'] == true) {
        playSound('assets/audio/example.mp3',
            'assets/audio/addition_task/question-number.mp3');
      } else {
        if (exams.exams[indexExam - 1]['example'] == true) {
          playSound('assets/audio/gotoRealQuestion.mp3',
              'assets/audio/addition_task/question-number.mp3');
        } else {
          playSound('assets/audio/addition_task/question-number.mp3');
        }
      }
    }
    lastIndex = indexExam;
    return exam;
  }

  Stack textWithStroke(
      {String text,
      double fontSize: 12,
      double strokeWidth: 1,
      Color textColor: Colors.white,
      Color strokeColor: Colors.black}) {
    return Stack(
      children: <Widget>[
        Text(
          text,
          style: TextStyle(
            fontSize: fontSize,
            foreground: Paint()
              ..style = PaintingStyle.stroke
              ..strokeWidth = strokeWidth
              ..color = strokeColor,
          ),
        ),
        Text(text, style: TextStyle(fontSize: fontSize, color: textColor)),
      ],
    );
  }

  Text textOutile(String txt) {
    return Text(
      txt,
      style: TextStyle(
        fontSize: 80,
        color: Color.fromRGBO(102, 68, 0, 1),
        foreground: Paint()
          ..style = PaintingStyle.stroke
          ..strokeWidth = 6
          ..color = Colors.blue[700],
      ),
    );
  }

@override
  void dispose() async {
    super.dispose();
    await player.dispose();
  }

void playSound(String sound,[String sound2]) async {
    await player.setAsset(sound);
    await player.play();
    if(sound2!=null){
      await player.setAsset(sound2);
      await player.play();
    }
  }
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        // title: Text(
        //   'Addition B Task',
        //   style: TextStyle(color: Colors.black),
        // ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
            return;
          },
        ),
      ),
      body: Container(
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(
                    'assets/games/addition/backgrounds/background-number.png'),
                fit: BoxFit.cover),
          ),
          child: SafeArea(
            child: exams != null
                ? Stack(
                                  children:[
                                      Align(
                                         alignment: Alignment(0.9,0.9),
                                                                              child: GestureDetector(
                                          onTap: () {
                                            if (chooseAns == -1) return;
                                            if (exams.exams[indexExam]['example'] ==
                                                true) {
                                            } else {
                                              if (chooseAns ==
                                                  exams.exams
                                                      .elementAt(indexExam)['ans']) {
                                                exams.correct++;
                                              } else {
                                                exams.fail++;
                                              }
                                              exams.time.add(DateTime.now()
                                                      .difference(this.countTime)
                                                      .inMilliseconds /
                                                  1000);
                                              exams.ans.add(chooseAns);
                                              exams.sol.add(exams.exams
                                                  .elementAt(indexExam)['ans']);
                                            }
                                            setState(() {
                                              checkPromote();
                                            });
                                          },
                                          child: Image.asset(
                                            'assets/buttons/ok.png',
                                            width: 20*SizeConfig.widthMultiplier,
                                            fit: BoxFit.contain,
                                          ),
                                        ),
                                      ),
                                     Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                            flex: 1,
                            child: Container(
                              margin: EdgeInsets.symmetric(
                                  vertical: 2 * SizeConfig.heightMultiplier,
                                  horizontal: 20 * SizeConfig.widthMultiplier),
                              width: double.infinity,
                              height: 5 * SizeConfig.heightMultiplier,
                              decoration: BoxDecoration(
                                   color: Color(0xffffd6c4),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10.0)),
                                 ),
                              child: Center(
                                child: Text(
                                  'บวกกันแล้วได้คำตอบเป็นเท่าไรจ๊ะ',
                                  style: TextStyle(
                                    fontFamily: 'Itim',
                                    fontSize: 4 * SizeConfig.textMultiplier,
                                    color: Color(0xff17478c),
                                  ),
                                ),
                              ),
                            )),
                        Expanded(
                          flex: 4,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              FittedBox(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    textWithStroke(
                                         fontSize: 14*SizeConfig.textMultiplier,
                                        text: exams.exams
                                            .elementAt(indexExam)['exam'][0]
                                            .toString(),
                                        textColor: Color.fromRGBO(77, 51, 0, 1),
                                        strokeWidth: 10,
                                        strokeColor:
                                            Color.fromRGBO(255, 204, 51, 1)),
                                    textWithStroke(
                                         fontSize: 14*SizeConfig.textMultiplier,
                                        text: exams.exams
                                            .elementAt(indexExam)['exam'][1]
                                            .toString(),
                                        textColor: Color.fromRGBO(77, 51, 0, 1),
                                        strokeWidth: 10,
                                        strokeColor:
                                            Color.fromRGBO(255, 204, 51, 1)),
                                    Text(
                                      '_____________________',
                                      style:
                                          TextStyle(fontWeight: FontWeight.w900),
                                    ),
                                    textWithStroke(
                                       fontSize: 14*SizeConfig.textMultiplier,
                                        text: '?',
                                        textColor: Color.fromRGBO(77, 51, 0, 1),
                                        strokeWidth: 10,
                                        strokeColor:
                                            Color.fromRGBO(255, 204, 51, 1)),
                                    Text(
                                      '_____________________',
                                      style:
                                          TextStyle(fontWeight: FontWeight.w900),
                                    ),
                                    Text(
                                      '_____________________',
                                      style:
                                          TextStyle(fontWeight: FontWeight.w900),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                  padding: EdgeInsets.only(bottom: 18*SizeConfig.heightMultiplier),
                                child: textWithStroke(
                                     fontSize: 13*SizeConfig.textMultiplier,
                                    text: '+',
                                    textColor: Color.fromRGBO(77, 51, 0, 1),
                                    strokeWidth: 10,
                                    strokeColor: Color.fromRGBO(255, 204, 51, 1)),
                              ),
                              SizedBox(
                                 width: 2*SizeConfig.heightMultiplier,
                              ),
                              Container(
                                width: 40*SizeConfig.heightMultiplier,
                              height: 40*SizeConfig.heightMultiplier,
                                child: GridView.count(
                                  padding: EdgeInsets.all(10),
                                  key: keyButton2,
                                  physics: NeverScrollableScrollPhysics(),
                                  crossAxisCount: 2,
                                   mainAxisSpacing: 2*SizeConfig.heightMultiplier,
                                crossAxisSpacing: 2*SizeConfig.heightMultiplier,
                                  children: _getExam(),
                                ),
                              )
                              ]
                            ,
                          ),
                        )
                      ]
                      ),
                                  ])
                : Center(
                    child: CircularProgressIndicator(),
                  ),
          )),
    );
  }

  void checkPromote() {
    //ถ้าเล่นเกมส์ครบชุด
    if (indexExam == exams.exams.length - 1) {
      //task check correct null before put
      // List<dynamic> data = Hive.box('test_task').get('data') as List<dynamic>;
      // data.add({
      //   'namegame': namegame,
      //   'correct': exams.correct,
      //   'ans': exams.ans,
      //   'counttime': DateTime.now().difference(this.countTime).inSeconds
      // });
      //Hive.box('test_task').put('data', data);
      Hive.box('test_task').delete(namegame);
      Hive.box('test_task').put(namegame, {
        'userid': Hive.box('appData').get('_id'),
        'namegame': namegame,
        'correct': exams.correct,
        'score': exams.correct + additionAScore['correct'],
        'ans': exams.ans,
        'time': exams.time,
        'counttime': exams.allTime()+ additionAScore['time']
      });
      //TestTaskExams();
      print(Hive.box('test_task').get(namegame));
      // print('indexttest :' + Hive.box('test_task').get('index').toString());
      // Navigator.of(context).pop();
      // Future.delayed(const Duration(milliseconds: 300), () {
      //   Navigator.of(context)
      //       .pushNamed("/" + Hive.box('test_task').get('testgame'));
      // });
      //เปลี่ยนเป็นโชว์สรุป
      showDialog(
          context: context,
          builder: (BuildContext context) => resultCustomDialog(
              context: context,
              score: exams.correct + additionAScore['correct'],
              lenght: exams.fail + exams.correct + additionAScore['lenght'],
              passScore: passScore)).then((_) => Navigator.of(context).pop());
      return;
    }
    //เล่นยังไม่ครบ
    lastIndex=indexExam;
    indexExam++;
    chooseAns = -1;
    countTime = new DateTime.now();
    if (exams.exams[indexExam]['example'] == true) {
      // Future.delayed(Duration(milliseconds: 100), () {
      //   showTutorial();
      // });
    }
  }

  @override
  void afterFirstLayout(BuildContext context) {
    additionAScore = ModalRoute.of(context).settings.arguments;
    setState(() {
      indexExam = 0;
      exams = TestTask();
      Map<String, dynamic> data = listexams.firstWhere((x) {
        return x['game'] == namegame;
      });
      exams.exams = data['exams'];
      passScore = data['passScore'];
      countTime = new DateTime.now();
      if (exams.exams[indexExam]['example'] == true) {
        // initTargets();
        // Future.delayed(Duration(milliseconds: 100), () {
        //   showTutorial();
        // });
      }
      print('passScore ' + passScore.toString());
    });
  }

  void initTargets() {
    targets.add(TargetFocus(
      identify: "Target 1",
      keyTarget: keyButton,
      contents: [
        ContentTarget(
            align: AlignContent.bottom,
            child: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "อ่านโจทย์ที่กำหนดให้",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 40.0),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: Text(
                      "เช็คคำสั่งของโจทย์ว่าให้เลือกอะไร ต้องเลือกกล่องส้มที่มีมากกว่า",
                      style: TextStyle(color: Colors.white, fontSize: 40.0),
                    ),
                  )
                ],
              ),
            ))
      ],
      shape: ShapeLightFocus.RRect,
    ));
    targets.add(TargetFocus(
      identify: "Target 2",
      keyTarget: keyButton2,
      contents: [
        ContentTarget(
            align: AlignContent.bottom,
            child: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "เลือกกล่องที่มีส้มมากกว่า",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 40.0),
                  ),
                ],
              ),
            ))
      ],
      shape: ShapeLightFocus.RRect,
    ));
  }

  void showTutorial() {
    TutorialCoachMark(context,
        targets: targets,
        colorShadow: Colors.blueGrey,
        textSkip: "ข้าม",
        paddingFocus: 10,
        opacityShadow: 0.8, finish: () {
      print("finish");
      countTime = new DateTime.now();
    }, clickTarget: (target) {
      print(target);
    }, clickSkip: () {
      print("skip");
      countTime = new DateTime.now();
    })
      ..show();
  }
}
