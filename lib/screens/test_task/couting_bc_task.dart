import 'dart:math' show Random;
import 'dart:convert';
import 'package:bordered_text/bordered_text.dart';
import 'package:http/http.dart' as http;
import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:just_audio/just_audio.dart';
import 'package:smart_math/game_modals/test_task.dart';
import 'package:smart_math/game_modals/test_task_exams.dart';
import 'package:smart_math/utility/setting.dart';
import 'package:tutorial_coach_mark/animated_focus_light.dart';
import 'package:tutorial_coach_mark/tutorial_coach_mark.dart';

import '../size_config.dart';

class TestCountingBCTask extends StatefulWidget {
  @override
  _TestOrainalATaskState createState() => _TestOrainalATaskState();
}

class _TestOrainalATaskState extends State<TestCountingBCTask>
    with AfterLayoutMixin<TestCountingBCTask> {
  final String namegame = "test_counting_bc_task";
  final player = AudioPlayer();

//for example
  List<TargetFocus> targets = List();
  GlobalKey keyButton = GlobalKey();
  GlobalKey keyButton2 = GlobalKey();

  DateTime countTime = new DateTime.now(); //ไว้นับเวลา
  TestTask exams;
  int indexExam = 0; //index exams
  int lastIndex = -1;
  Random rd = new Random(); //for random colors
  List<int> choice = [];
  bool checkAns = false;

  List<Widget> _getExam() {
    List<Widget> exam = new List<Widget>();
    Map<String, dynamic> ex = exams.exams.elementAt(indexExam);
    print('index : $indexExam');
    List<Widget> buttonNumber = new List<Widget>();
    List<int> choice = ex['ans'];
    for (int i = choice.length - 1; i >= 0; i--) {
      buttonNumber.add(FlatButton(
        child: Text(choice[i] == -1 ? 'X' : choice[i].toString(),
            style: TextStyle(fontSize: 50)),
        color: Colors.blue,
        onPressed: () async {
          if (exams.exams[indexExam]['example'] == true) {
          } else {
            if (ex['score'][i] >= 1.5) {
              exams.correct++;
              await playSound('assets/audio/clapping.mp3');
            } else {
              exams.fail++;
              await playSound('assets/audio/falsesound.mp3');
            }
            exams.score += ex['score'][i];
            exams.time.add(
                DateTime.now().difference(this.countTime).inMilliseconds /
                    1000);
            exams.ans.add(choice[i]);
            exams.sol.add(ex['ans'][3]);
          }
          setState(() {
            checkPromote();
          });
        },
      ));
      buttonNumber.add(SizedBox(
        height: 20,
      ));
    }
    exam.add(Expanded(
        flex: 3,
        child: Center(
          child: Padding(
            padding: const EdgeInsets.only(left: 100, bottom: 100),
            child: BorderedText(
              strokeWidth: 10.0,
            strokeColor: Colors.blueGrey,
                          child: Text(
                ex['exam'].toString(),
                style: TextStyle( 
                  color: Colors.blue,
                  fontFamily: 'Itim',
                      fontSize:  30 * SizeConfig.textMultiplier),
              ),
            ),
          ),
        )));
    exam.add(Expanded(
        flex: 1,
        child: checkAns == false
            ? Align(
                alignment: Alignment.center,
                child: GestureDetector(
                    onTap: () async {
                      await playSound('assets/audio/buttonClick.mp3');
                      setState(() {
                          checkAns = true;
                        });
                    },
                    child: Image.asset(
                      'assets/buttons/ok.png',
                      width: 20*SizeConfig.widthMultiplier,
                      fit: BoxFit.contain,
                    )),
              )
            : Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Text('เลขที่นับได้', style: TextStyle(
                        fontFamily: 'Itim',
                    fontSize:  5 * SizeConfig.textMultiplier)),
                  ...buttonNumber,
                  SizedBox(
                    height: 15*SizeConfig.heightMultiplier,
                  )
                ],
              )));
    if(indexExam!=lastIndex){
      if(exams.exams[indexExam]['example'] == true){
         if(exams.exams[indexExam]['topic']=='นับเลขไปข้างหน้าอีก 3 จำนวน'){
            playSound('assets/audio/example.mp3','assets/audio/counting_task/countFront.mp3');
         }else{
            playSound('assets/audio/example.mp3','assets/audio/counting_task/countBack.mp3');
         }
      }else{
        if(exams.exams[indexExam]['topic']=='นับเลขไปข้างหน้าอีก 3 จำนวน'){
          if(exams.exams[indexExam-1]['example'] == true){
            playSound('assets/audio/gotoRealQuestion.mp3','assets/audio/counting_task/countFront.mp3');
          }else{
            playSound('assets/audio/counting_task/countFront.mp3');
          }
         }else{
           if(exams.exams[indexExam-1]['example'] == true){
            playSound('assets/audio/gotoRealQuestion.mp3','assets/audio/counting_task/countBack.mp3');
          }else{
            playSound('assets/audio/counting_task/countBack.mp3');
          }
         }
      }
    }
    lastIndex = indexExam;
    return exam;
  }

void playSound(String sound,[String sound2]) async {
     await player.setAsset(sound);
     await player.play();
     if(sound2!=null){
        await player.setAsset(sound2);
       await player.play();
     }
  }
  @override
  void dispose() async {
    super.dispose();
    await player.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        // title: Text(
        //   'Test Counting B,C Task',
        //   style: TextStyle(color: Colors.black),
        // ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
            return;
          },
        ),
      ),
      body: Container(
          height: MediaQuery.of(context).size.height,
          width: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/bg/game_bg.jpg'), fit: BoxFit.cover),
          ),
          child: SafeArea(
            child: exams != null
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Container(
                            margin: EdgeInsets.symmetric(
                                 vertical: 3*SizeConfig.heightMultiplier,
                                 horizontal: 20*SizeConfig.widthMultiplier),
                            width: double.infinity,
                            height: 5*SizeConfig.heightMultiplier,
                          decoration: BoxDecoration(
                              color: Color(0xffffd6c4),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                              ),
                            child: Center(
                                key: keyButton,
                                child: Text(
                                  '${exams.exams[indexExam]['topic']}',
                                  style: TextStyle(
                                    color: Color(0xff17478c),
                                    fontFamily: 'Itim',
                                    fontSize: 5 * SizeConfig.textMultiplier,
                                  ),
                                )
                                ),
                          ),
                        ),
                        Expanded(
                            flex: 4,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: _getExam(),
                            )),
                      ])
                : Center(
                    child: CircularProgressIndicator(),
                  ),
          )),
    );
  }

  void checkPromote() {
    //ถ้าเล่นเกมส์ครบชุด
    if (indexExam == exams.exams.length - 1) {
      //task check correct null before put
      // List<dynamic> data = Hive.box('test_task').get('data') as List<dynamic>;
      // data.add({
      //   'namegame': namegame,
      //   'correct': exams.correct,
      //   'ans': exams.ans,
      //   'counttime': DateTime.now().difference(this.countTime).inSeconds
      // });
      //Hive.box('test_task').put('data', data);
      Hive.box('test_task').delete(namegame);
      Hive.box('test_task').put(namegame, {
        'userid': Hive.box('appData').get('_id'),
        'namegame': namegame,
        'correct': exams.correct,
        'score': exams.score,
        'ans': exams.ans,
        'time': exams.time,
        'counttime': exams.allTime()
      });
      //TestTaskExams();
      print(Hive.box('test_task').get(namegame));
      http.post(
        '$serverUrl/test_task/add',
        body: json.encode(Hive.box('test_task').get(namegame)),
        headers: {
          "content-type": "application/json",
          "accept": "application/json",
        },
      );
      //เปลี่ยนเป็นโชว์สรุป
      Navigator.of(context).popAndPushNamed('/test_counting_d_task',
          arguments: {
            'correct': exams.correct,
            'lenght': exams.correct + exams.fail,
            'time':exams.allTime(),
          });
      return;
    }
    //เล่นยังไม่ครบ
    lastIndex = indexExam;
    indexExam++;
    checkAns = false;
    countTime = new DateTime.now();
    //ไว้เรียกโชว์ tutorial
    if (exams.exams[indexExam]['example'] == true) {
      // Future.delayed(Duration(milliseconds: 100), () {
      //   showTutorial();
      // });
    }
    //  Hive.box('test_task').put('index', Hive.box('test_task').get('index') + 1);
  }

  @override
  void afterFirstLayout(BuildContext context) {
    setState(() {
      indexExam = 0;
      print('indexExam ' + indexExam.toString());
      print('indextest ' + Hive.box('test_task').get('index').toString());
      exams = TestTask();
      exams.exams = listexams.firstWhere((x) {
        return x['game'] == namegame;
      })['exams'];
      countTime = new DateTime.now();
      if (exams.exams[indexExam]['example'] == true) {
        // initTargets();
        // Future.delayed(Duration(milliseconds: 200), () {
        //   showTutorial();
        // });
      }
    });
  }

  void initTargets() {
    targets.add(TargetFocus(
      identify: "Target 1",
      keyTarget: keyButton,
      contents: [
        ContentTarget(
            align: AlignContent.bottom,
            child: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "อ่านโจทย์ที่กำหนดให้",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 40.0),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: Text(
                      "เช็คคำสั่งของโจทย์ว่าให้เลือกอะไร",
                      style: TextStyle(color: Colors.white, fontSize: 40.0),
                    ),
                  )
                ],
              ),
            ))
      ],
      shape: ShapeLightFocus.RRect,
    ));
    targets.add(TargetFocus(
      identify: "Target 2",
      keyTarget: keyButton2,
      contents: [
        ContentTarget(
            align: AlignContent.bottom,
            child: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "เลือกผลรวมของแอปเปิ้ล",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 40.0),
                  ),
                ],
              ),
            ))
      ],
      shape: ShapeLightFocus.RRect,
    ));
  }

  void showTutorial() {
    TutorialCoachMark(context,
        targets: targets,
        colorShadow: Colors.blueGrey,
        textSkip: "ข้าม",
        paddingFocus: 10,
        opacityShadow: 0.8, finish: () {
      print("finish");
      countTime = new DateTime.now();
    }, clickTarget: (target) {
      print(target);
    }, clickSkip: () {
      print("skip");
      countTime = new DateTime.now();
    })
      ..show();
  }
}
