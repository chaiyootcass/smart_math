import 'dart:convert';
import 'dart:io';
import 'dart:math' show Random;
import 'package:bordered_text/bordered_text.dart';
import 'package:http/http.dart' as http;
import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:just_audio/just_audio.dart';
import 'package:smart_math/game_modals/test_task.dart';
import 'package:smart_math/game_modals/test_task_exams.dart';
import 'package:smart_math/utility/custom_dialog.dart';
import 'package:smart_math/utility/setting.dart';
import 'package:tutorial_coach_mark/animated_focus_light.dart';
import 'package:tutorial_coach_mark/tutorial_coach_mark.dart';

import '../size_config.dart';

class TestNumberlineTask extends StatefulWidget {
  @override
  _TestNumberlineTaskState createState() => _TestNumberlineTaskState();
}

class _TestNumberlineTaskState extends State<TestNumberlineTask>
    with AfterLayoutMixin<TestNumberlineTask> {
  final String namegame = "test_numberline_task";
  final player = AudioPlayer();
//for example
  List<TargetFocus> targets = List();
  GlobalKey keyButton = GlobalKey();
  GlobalKey keyButton2 = GlobalKey();

  DateTime countTime = new DateTime.now(); //ไว้นับเวลา
  TestTask exams;
  int indexExam = 0; //index exams
  int lastIndex = -1;
  Random rd = new Random(); //for random colors
  List<int> choice = [];
  double passScore = 0.0;

  List<Widget> _getExam() {
    List<Widget> exam = new List<Widget>();
    Map<String, dynamic> ex = exams.exams.elementAt(indexExam);
    choice = ex['choice'].cast<int>();
    for (int i = 0; i < choice.length; i++) {
      exam.add(
        Expanded(
          flex: 1,
          child: DragTarget<int>(
            onAccept: (data) async {
              if (exams.exams[indexExam]['example'] == true) {
              } else {
                if (choice[i] == ex['ans']) {
                  exams.correct++;
                   await playSound('assets/audio/clapping.mp3');
                } else {
                  exams.fail++;
                  await playSound('assets/audio/falsesound.mp3');
                }
                exams.time.add(
                    DateTime.now().difference(this.countTime).inMilliseconds /
                        1000);
                exams.ans.add(choice[i]);
                exams.sol.add(ex['ans']);
              }
              setState(() {
                checkPromote();
              });
            },
            onWillAccept: (data) {
              return true;
            },
            builder: (context, List<int> candidateData, rejectedData) {
              return Row(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      BorderedText(
                        strokeWidth: 10.0,
                        strokeColor: Colors.green[900],
                        child: Text(
                          '|',
                          style: TextStyle(
                            fontSize: 3 * SizeConfig.heightMultiplier,
                            color: Colors.green[300],
                          ),
                        ),
                      ),
                      BorderedText(
                        strokeWidth: 10.0,
                        strokeColor: Colors.green[900],
                        child: Text(
                          choice[i].toString(),
                          style: TextStyle(
                            fontSize: 6 * SizeConfig.heightMultiplier,
                            color: Colors.green[300],
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              );
            },
          ),
        ),
      );
    }
    if (lastIndex != indexExam) {
      if (ex['example'] == true) {
        playSound('assets/audio/letSeeExample.mp3',
            'assets/audio/numberline_task/putTheRocketToTheRightLine.mp3');
      } else {
        playSound(
            'assets/audio/numberline_task/putTheRocketToTheRightLine.mp3');
      }
    }
    lastIndex = indexExam;
    return exam;
  }

  bool showByLineLv(int lv, int positon) {
    if (positon == 0 || positon == 10 || lv == 1) {
      return true;
    }
    if (lv == 2 && positon == 5) {
      return true;
    }
    return false;
  }

  @override
  void dispose() async {
    super.dispose();
    await player.dispose();
  }

void playSound(String sound,[String sound2]) async {
    await player.setAsset(sound);
    await player.play();
    if(sound2!=null){
      await player.setAsset(sound2);
      await player.play();
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        // title: Text('Test Numberline Task',
        //   style: TextStyle(color: Colors.black),),
        // leading: IconButton(
        //   icon: Icon(Icons.arrow_back),
        //   onPressed: () {
        //       Navigator.pop(context);
        //   },
        // ),
      ),
      body: Container(
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(
                    'assets/games/numberline/backgrounds/background0.jpg'),
                fit: BoxFit.cover),
          ),
          child: SafeArea(
            child: exams != null
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Container(
                          height: 7 * SizeConfig.heightMultiplier,
                          width: 125 * SizeConfig.widthMultiplier,
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              color: Color(0xffffd6c4),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                              ),
                          child: Center(
                            child: Text(
                              'ให้หนูเอาจรวดลงจอดบนเส้นจำนวนให้ถูกต้อง',
                              style: TextStyle(
                                fontFamily: 'Itim',
                                fontSize: 4 * SizeConfig.textMultiplier,
                                color: Color(0xff17478c),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 5,
                        child: Stack(
                          children: <Widget>[
                            Align(
                              alignment: Alignment(
                                  rd.nextDouble() * (rd.nextBool() ? 1 : -1),
                                  rd.nextDouble() * (rd.nextBool() ? 1 : -1)),
                              child: Draggable<int>(
                                data: exams.exams.elementAt(indexExam)['ans'],
                                onDragCompleted: () {
                                  print('Completed');
                                  sleep(Duration(seconds:2));
                                },
                                childWhenDragging: SizedBox(
                                  height: 20 * SizeConfig.heightMultiplier,
                                  width: 40 * SizeConfig.widthMultiplier,
                                ),
                                feedback: Container(
                                  height: 20 * SizeConfig.heightMultiplier,
                                  width: 40 * SizeConfig.widthMultiplier,
                                  child: Image.asset(
                                      'assets/games/numberline/rocket/${exams.exams.elementAt(indexExam)['type']}-rocket-${exams.exams.elementAt(indexExam)['ans']}.png',
                                      fit: BoxFit.scaleDown),
                                ),
                                child: Container(
                                  height: 20 * SizeConfig.heightMultiplier,
                                  width: 40 * SizeConfig.widthMultiplier,
                                  child: Image.asset(
                                      'assets/games/numberline/rocket/${exams.exams.elementAt(indexExam)['type']}-rocket-${exams.exams.elementAt(indexExam)['ans']}.png',
                                      fit: BoxFit.scaleDown),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Stack(
                          children: <Widget>[
                            Row(
                              children: '-----------------------------------'
                                  .split('')
                                  .map((f) {
                                return Expanded(
                                    flex: 1,
                                    child: Text(
                                      f,
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 8 * SizeConfig.textMultiplier,
                                      ),
                                    ));
                              }).toList(),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  left: 1.0 * SizeConfig.widthMultiplier,
                                  right: 2.0 * SizeConfig.widthMultiplier,
                                  top: 3 * SizeConfig.heightMultiplier),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: _getExam(),
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  )
                : Center(
                    child: CircularProgressIndicator(),
                  ),
          )),
    );
  }

  void checkPromote() {
    //ถ้าเล่นเกมส์ครบชุด
    if (indexExam == exams.exams.length - 1) {
      //task check correct null before put
      // List<dynamic> data = Hive.box('test_task').get('data') as List<dynamic>;
      // data.add({
      //   'namegame': namegame,
      //   'correct': exams.correct,
      //   'ans': exams.ans,
      //   'counttime': DateTime.now().difference(this.countTime).inSeconds
      // });
      //Hive.box('test_task').put('data', data);
      Hive.box('test_task').delete(namegame);
      Hive.box('test_task').put(namegame, {
        'userid': Hive.box('appData').get('_id'),
        'namegame': namegame,
        'correct': exams.correct,
        'score': exams.correct,
        'ans': exams.ans,
        'sol': exams.sol,
        'time': exams.time,
        'counttime': exams.allTime()
      });
      //TestTaskExams();
      print(Hive.box('test_task').get(namegame));
      http.post(
        '$serverUrl/test_task/add',
        body: json.encode(Hive.box('test_task').get(namegame)),
        headers: {
          "content-type": "application/json",
          "accept": "application/json",
        },
      );
      //  print('indexttest :' + Hive.box('test_task').get('index').toString());
      // Navigator.of(context).pop();
      // Future.delayed(const Duration(milliseconds: 300), () {
      //   Navigator.of(context)
      //       .pushNamed("/" + Hive.box('test_task').get('testgame'));
      // });
      //เปลี่ยนเป็นโชว์สรุป
      showDialog(
          context: context,
          builder: (BuildContext context) => resultCustomDialog(
              context: context,
              score: exams.correct,
              lenght: exams.fail + exams.correct,
              passScore: passScore)).then((_) => Navigator.of(context).pop());
      return;
    }
    //เล่นยังไม่ครบ
    indexExam++;
    countTime = new DateTime.now();
    // if (exams.exams[indexExam]['example'] == true) {
    //   Future.delayed(Duration(milliseconds: 100), () {
    //     showTutorial();
    //   });
    // }
    //  Hive.box('test_task').put('index', Hive.box('test_task').get('index') + 1);
  }

  @override
  void afterFirstLayout(BuildContext context) {
    setState(() {
      indexExam = 0;
      exams = TestTask();
      Map<String, dynamic> data = listexams.firstWhere((x) {
        return x['game'] == namegame;
      });
      exams.exams = data['exams'];
      passScore = data['passScore'];
      countTime = new DateTime.now();
     // if (exams.exams[indexExam]['example'] == true) {
       // initTargets();
        // Future.delayed(Duration(milliseconds: 100), () {
        //   showTutorial();
        // });
     // }
      print('passScore ' + passScore.toString());
    });
  }

  void initTargets() {
    targets.add(TargetFocus(
      identify: "Target 1",
      keyTarget: keyButton,
      contents: [
        ContentTarget(
            align: AlignContent.bottom,
            child: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "อ่านโจทย์ที่กำหนดให้",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 40.0),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: Text(
                      "เช็คคำสั่งของโจทย์ว่าให้เลือกอะไร ต้องเลือกกล่องส้มที่มีมากกว่า",
                      style: TextStyle(color: Colors.white, fontSize: 40.0),
                    ),
                  )
                ],
              ),
            ))
      ],
      shape: ShapeLightFocus.RRect,
    ));
    targets.add(TargetFocus(
      identify: "Target 2",
      keyTarget: keyButton2,
      contents: [
        ContentTarget(
            align: AlignContent.bottom,
            child: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "เลือกกล่องที่มีส้มมากกว่า",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 40.0),
                  ),
                ],
              ),
            ))
      ],
      shape: ShapeLightFocus.RRect,
    ));
  }

  void showTutorial() {
    TutorialCoachMark(context,
        targets: targets,
        colorShadow: Colors.blueGrey,
        textSkip: "ข้าม",
        paddingFocus: 10,
        opacityShadow: 0.8, finish: () {
      print("finish");
      countTime = new DateTime.now();
    }, clickTarget: (target) {
      print(target);
    }, clickSkip: () {
      print("skip");
      countTime = new DateTime.now();
    })
      ..show();
  }
}
