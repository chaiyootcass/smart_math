import 'dart:convert';
import 'dart:math';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:bordered_text/bordered_text.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:http/http.dart' as http;
import 'package:after_layout/after_layout.dart';
import 'package:just_audio/just_audio.dart';
import 'package:liquid_progress_indicator/liquid_progress_indicator.dart';
import 'package:smart_math/game_modals/addition_task_modal.dart';
import 'package:smart_math/game_modals/log_game.dart';
import 'package:smart_math/screens/size_config.dart';
import 'package:smart_math/utility/custom_star.dart';
import 'package:smart_math/utility/feedback_dialog.dart';
import 'package:smart_math/utility/reward_dialog.dart';
import 'package:smart_math/utility/setting.dart';

class AdditionTaskLv1and2 extends StatefulWidget {
  @override
  _AdditionTaskLv1and2State createState() => _AdditionTaskLv1and2State();
}

class _AdditionTaskLv1and2State extends State<AdditionTaskLv1and2>
    with AfterLayoutMixin<AdditionTaskLv1and2> {
  final String namegame = "addition_task";
  final player = AudioPlayer();
  DateTime countTime = new DateTime.now(); //ไว้นับเวลา
  AdditionTaskExam exams;
  int indexExam = 0; //index exams
  List<int> choice = [];
  Random rd = new Random();
int countFail = 0;
  List<Widget> _getExam() {
    List<Widget> exam = new List<Widget>();
    Map<String, dynamic> ex = exams.exams.elementAt(indexExam);
    print('index : $indexExam');
    choice = ex['choice'];
    for (int i = 0; i < choice.length; i++) {
      exam.add(GestureDetector(
          onTap: () async {
            if (choice[i] == ex['ans']) {
              exams.correct++;
               countFail = 0;
                if (indexExam < exams.exams.length - 1)
                  showDialog(
                      context: context, builder: (context) => FeedbackDialog());
               await playSound('assets/audio/clapping.mp3');
            } else {
              countFail++;
                if (countFail == 3) {
                  exams.fail++;
                  countFail = 0;
                  await playSound('assets/audio/tryagain.mp3');
                } else {
                  await playSound('assets/audio/falsesound.mp3');
                  return;
                }
            }
            exams.ans.add(choice[i]);
            exams.sol.add(ex['ans']);
            setState(() {
              checkPromote('addition_task');
            });
          },
          child: Image.asset(
            'assets/games/addition/apple/apple-choice-${choice[i]}.png',
            width: 30 * SizeConfig.heightMultiplier,
            height: 20 * SizeConfig.heightMultiplier,
            fit: BoxFit.contain,
          )));
    }
    playSound('assets/audio/addition_task/questionCloudApple.mp3');
    return exam;
  }
  void playSound(String sound) async {
     await player.setAsset(sound);
     await player.play();
  }
  @override
  void dispose() async {
    super.dispose();
    await player.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
       extendBodyBehindAppBar: true,
      appBar: AppBar(
         title: Text(
          'Addition Task',
          style: TextStyle(color: Colors.grey,  fontSize: 1.5 * SizeConfig.textMultiplier),
        ),
        centerTitle: false,
        actions: [
          Align(
            alignment: Alignment.centerRight,
            child:Row(
              children: [
                 Container(
                  width: 25*SizeConfig.widthMultiplier,
                  height: 3*SizeConfig.heightMultiplier,
                  padding: EdgeInsets.symmetric(horizontal: 2.0),
                  child: LiquidLinearProgressIndicator(
                    value: indexExam / exams.numExam,
                    backgroundColor: Colors.white.withOpacity(0.2),
                    valueColor: AlwaysStoppedAnimation(Colors.yellowAccent),
                    borderColor: Colors.blue,
                    borderWidth: 2.5,
                  ),
                ),
                Text('level : ${exams.level} ',
                    style: TextStyle(
                        color: Colors.grey,
                        fontSize: 1.5 * SizeConfig.textMultiplier)),
              ],
            ),
            ),
          
        ],
      backgroundColor: Colors.transparent,
      elevation: 0.0,
      ),
          body: Container(
        height: MediaQuery.of(context).size.height,
        width: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/bg/game_bg.jpg'), fit: BoxFit.cover),
        ),
        child: SafeArea(
        child: exams != null
            ? Stack(
                children: <Widget>[
                  Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Padding(
                              padding: EdgeInsets.only(
                                left: 7.0 * SizeConfig.widthMultiplier,
                                right: 7.0 * SizeConfig.widthMultiplier,
                                bottom: 50.0 * SizeConfig.heightMultiplier,
                                //top: 1.0 * SizeConfig.heightMultiplier,
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Expanded(
                                      child: Container(
                                    height: 10 * SizeConfig.heightMultiplier,
                                    // width:
                                    //     MediaQuery.of(context).size.width - 40.0,
                                    padding: EdgeInsets.all(10),
                                    decoration: BoxDecoration(
                                         color: Color(0xffffd6c4),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10.0)),
                                        ),
                                    child: Center(
                                      child: Text(
                                        'เก็บแอปเปิ้ลจากก้อนเมฆสองก้อนมารวมกันได้จานไหน',
                                        style: TextStyle(
                                          fontFamily: 'Itim',
                                          fontSize:
                                              4 * SizeConfig.textMultiplier,
                                        color: Color(0xff17478c),
                                        ),
                                      ),
                                    ),
                                  ))
                                ],
                              )
                              ),
                        )
                      ],
                    ),
                  ),
                  Column(
                    children: <Widget>[
                      Expanded(
                          flex: 2,
                          child: exams != null
                              ? Row(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 1,
                                      child: Container(
                                        // width: MediaQuery.of(context).size.width -
                                        //     40.0,
                                        height: 30 * SizeConfig.heightMultiplier,
                                        padding: EdgeInsets.only(
                                            top:
                                                10 * SizeConfig.heightMultiplier),
                                        child: Image.asset(
                                          'assets/games/addition/apple/apple-choice-${exams.exams.elementAt(indexExam)['exam'][0]}.png',
                                          fit: BoxFit.contain,
                                        ),
                                      ),
                                      /*child: Center(
                                        child: Image.asset(
                                          'assets/games/addition/apple/apple-choice-${exams.exams.elementAt(indexExam)['exam'][0]}.png',
                                          fit: BoxFit.contain,
                                        ),
                                      ),*/
                                    ),
                                    Expanded(
                                      flex: 1,
                                      child: Container(
                                        height: 30 * SizeConfig.heightMultiplier,
                                        padding: EdgeInsets.only(
                                            top:
                                                12 * SizeConfig.heightMultiplier),
                                        child: Image.asset(
                                          'assets/games/addition/apple/apple-choice-${exams.exams.elementAt(indexExam)['exam'][1]}.png',
                                          fit: BoxFit.contain,
                                        ),
                                      ),
                                    )
                                  ],
                                )
                              : Center(child: CircularProgressIndicator())),
                      Expanded(
                        flex: 1,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: _getExam(),
                        ),
                      )
                    ],
                  )
                ],
              )
            : Center(
                child: CircularProgressIndicator(),
              ),
        )
      ),
    );
  }

  void checkPromote(String game) {
    //ถ้าเล่นเกมส์ผิด 3 ข้อลดเลเวลลง
    if (exams.fail == 3) {
      String note = 'Fail 3 time';
      Hive.box(game).delete('unfinished_game');
      int level = Hive.box('appData').get(game + '_level');
      if (level > 1) {
        Hive.box('appData').put(game + '_level', level - 1);
        print('Level Down: $game ${Hive.box('appData').get(game)}');
      }
      LogGame test = LogGame(
          userid: Hive.box('appData').get('_id'),
          level: Hive.box('appData').get(game + '_level'),
          correct: exams.correct,
          fail: exams.fail,
          time: exams.countTime +
              DateTime.now().difference(this.countTime).inSeconds,
          note: note,
          solve: exams.sol,
          answer: exams.ans);
      http.post(
        '$serverUrl/$game/add',
        body: json.encode(test.toJson()),
        headers: {
          "content-type": "application/json",
          "accept": "application/json",
        },
      );
      Hive.box(game + '_data').add({
        'date': '${countTime.day}-${countTime.month}-${countTime.year}',
        'time': test.time,
        'correct': exams.correct,
        'fail': exams.fail
      });
      Hive.box('appData')
          .put('allTime', Hive.box('appData').get('allTime') + test.time);
      Hive.box('appData').put(
          'allCorrect', Hive.box('appData').get('allCorrect') + exams.correct);
      Hive.box('appData')
          .put('allFail', Hive.box('appData').get('allFail') + exams.fail);
      exams = AdditionTaskExam();
      choice.clear();
      indexExam = 0;
      countTime = new DateTime.now();
      return;
    }
    //ถ้าเล่นเกมส์ครบชุด
    if (indexExam == exams.exams.length - 1) {
      print('upload log');
      //เล่นเกมส์ครบชุด ส่ง log
      String note = 'Max level';
      //เล่นเกมส์ครบชุด ส่ง log
      Hive.box(game).delete('unfinished_game');

      // เล่นเกมส์ครบชุด เช็คเงื่อนไขการอัพเลเวล
      if (Hive.box('appData').get(game + '_level') <
          Hive.box(game).get('numLevel')) {
        note = 'Level up';
        Hive.box('appData')
            .put(game + '_level', Hive.box('appData').get(game + '_level') + 1);
        print('Level Up: $game ${Hive.box('appData').get(game + '_level')}');
      }
      LogGame test = LogGame(
          userid: Hive.box('appData').get('_id'),
          level: Hive.box('appData').get(game + '_level'),
          correct: exams.correct,
          fail: exams.fail,
          time: DateTime.now().difference(this.countTime).inSeconds,
          note: note,
          solve: exams.sol,
          answer: exams.ans);
      http.post(
        '$serverUrl/$game/add',
        body: json.encode(test.toJson()),
        headers: {
          "content-type": "application/json",
          "accept": "application/json",
        },
      );
      Hive.box(game + '_data').add({
        'date': '${countTime.day}-${countTime.month}-${countTime.year}',
        'time': test.time,
        'correct': exams.correct,
        'fail': exams.fail
      });
      Hive.box('appData')
          .put('allTime', Hive.box('appData').get('allTime') + test.time);
      Hive.box('appData').put(
          'allCorrect', Hive.box('appData').get('allCorrect') + exams.correct);
      Hive.box('appData')
          .put('allFail', Hive.box('appData').get('allFail') + exams.fail);
      Hive.box('appData').get(game + '_level') > 2
          ? Navigator.of(context).popAndPushNamed('/addition_task')
          : null;
      exams = AdditionTaskExam();
      choice.clear();
      indexExam = 0;
      countTime = new DateTime.now();
      addTimeLimit(test.time);
      showDialog(
          context: context, builder: (context) => RewardDialog(rd.nextInt(8))).then((_) {
            checkTimeLimit();
          });
      return;
    }
    //เล่นยังไม่ครบ
    indexExam++;
  }
 void checkTimeLimit(){
    if(Hive.box('appData').get('timeUse')==null) return;
    if(Hive.box('appData').get('timeUse')>=(Hive.box('appData').get('time')*60)){
      AwesomeDialog(context: context,
            dialogType: DialogType.INFO,
            animType: AnimType.BOTTOMSLIDE,
            btnCancel: null,
            tittle: 'เวลาหมดแล้ว',
            desc: 'ไว้มาเล่นพรุ่งนี้นะจ้ะ',
            btnOkOnPress: () {
            }).show().then((_) {
                Navigator.pop(context);
            });
    }
    print(Hive.box('appData').get('timeUse'));
     print(Hive.box('appData').get('time'));
  }

  void addTimeLimit(int time){
    String today = DateTime.now().day.toString()+DateTime.now().month.toString();
    if(Hive.box('appData').get('dayLimit')!=today){
       Hive.box('appData').put('dayLimit', today);
       Hive.box('appData').put('timeUse', 0);
    }{
      Hive.box('appData').put('timeUse', Hive.box('appData').get('timeUse')+time);
    }
  }
  @override
  void afterFirstLayout(BuildContext context) {
    setState(() {
      exams = AdditionTaskExam();
    });
  }
}
