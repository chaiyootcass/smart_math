import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:smart_math/screens/addition_task/addtion_task_lv1-2.dart';
import 'package:smart_math/screens/addition_task/addtion_task_lv3-4.dart';
import 'package:smart_math/screens/addition_task/addtion_task_lv5-6.dart';

import '../size_config.dart';

class AdditionTask extends StatelessWidget {

  Widget _chooseGame(){
    print('Addtion_task_level ${Hive.box('appData').get('addition_task_level')}');
     switch (Hive.box('appData').get('addition_task_level')) {
      case 1:
      case 2:
        return AdditionTaskLv1and2();
        break;
      case 3:
       return AdditionTaskLv3and4();
        break;
      case 4:
        return AdditionTaskLv3and4();
        break;
      case 5:
      case 6:
      case 7:
      case 8:
        return AdditionTaskLv5and6();
        break;
      default:
        return AdditionTaskLv1and2();
    }
  }
  @override
  Widget build(BuildContext context) {
    return  _chooseGame();
    
  }
}
