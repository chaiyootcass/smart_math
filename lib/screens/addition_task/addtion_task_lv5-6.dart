import 'dart:convert';
import 'dart:math';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:bordered_text/bordered_text.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:http/http.dart' as http;
import 'package:after_layout/after_layout.dart';
import 'package:just_audio/just_audio.dart';
import 'package:liquid_progress_indicator/liquid_progress_indicator.dart';
import 'package:smart_math/game_modals/addition_task_modal.dart';
import 'package:smart_math/game_modals/log_game.dart';
import 'package:smart_math/utility/custom_star.dart';
import 'package:smart_math/utility/reward_dialog.dart';
import 'package:smart_math/utility/setting.dart';

import '../size_config.dart';

class AdditionTaskLv5and6 extends StatefulWidget {
  @override
  _AdditionState createState() => _AdditionState();
}

class _AdditionState extends State<AdditionTaskLv5and6>
    with AfterLayoutMixin<AdditionTaskLv5and6> {
  final player = AudioPlayer();
  AdditionTaskExam exams;
  DateTime countTime = new DateTime.now(); //ไว้นับเวลา
  int indexExam = 0; //index exams
  List<int> choice = [];
  int chooseAns = -1;
  int lastIndex = -1;
  Random rd = new Random();
int countFail = 0;
  List<Widget> _getExam() {
    List<Widget> exam = new List<Widget>();
    Map<String, dynamic> ex = exams.exams.elementAt(indexExam);
    print('index : $indexExam');
    choice = ex['choice'];

    for (int i = 0; i < choice.length; i++) {
      exam.add(GestureDetector(
          onTap: () {
            setState(() {
              chooseAns = choice[i];
               playSound('assets/audio/buttonClick.mp3');
            });
          },
          child: Container(
            decoration: BoxDecoration(
                color: chooseAns == choice[i]
                    ? Colors.orangeAccent
                    : Colors.grey[200],
                boxShadow: const [BoxShadow(blurRadius: 5)],
                borderRadius: BorderRadius.all(Radius.circular(5*SizeConfig.widthMultiplier))),
            child: Center(
                child: textWithStroke(
                    fontSize: 12*SizeConfig.textMultiplier,
                    text: choice[i].toString(),
                    textColor: Colors.blue,
                    strokeWidth: 10,
                    strokeColor: Colors.blueGrey)),
          )));
    }
     if(indexExam!=lastIndex) playSound('assets/audio/addition_task/calculateNumberAndAnswer.mp3');
   lastIndex = indexExam;
   return exam;
  }

  Stack textWithStroke(
      {String text,
      double fontSize: 12,
      double strokeWidth: 1,
      Color textColor: Colors.white,
      Color strokeColor: Colors.black}) {
    return Stack(
      children: <Widget>[
        Text(
          text,
          style: TextStyle(
            fontSize: fontSize,
            foreground: Paint()
              ..style = PaintingStyle.stroke
              ..strokeWidth = strokeWidth
              ..color = strokeColor,
          ),
        ),
        Text(text, style: TextStyle(fontSize: fontSize, color: textColor)),
      ],
    );
  }

  Text textOutile(String txt) {
    return Text(
      txt,
      style: TextStyle(
        fontSize: 15*SizeConfig.textMultiplier,
        color: Color.fromRGBO(102, 68, 0, 1),
        foreground: Paint()
          ..style = PaintingStyle.stroke
          ..strokeWidth = 6
          ..color = Colors.blue[700],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
         title: Text(
          'Addition Task',
          style: TextStyle(color: Colors.grey,  fontSize: 1.5 * SizeConfig.textMultiplier),
        ),
        centerTitle: false,
        actions: [
          Align(
            alignment: Alignment.centerRight,
            child:Row(
              children: [
                 Container(
                  width: 25*SizeConfig.widthMultiplier,
                  height: 3*SizeConfig.heightMultiplier,
                  padding: EdgeInsets.symmetric(horizontal: 2.0),
                  child: LiquidLinearProgressIndicator(
                    value: indexExam / exams.numExam,
                    backgroundColor: Colors.white.withOpacity(0.2),
                    valueColor: AlwaysStoppedAnimation(Colors.yellowAccent),
                    borderColor: Colors.blue,
                    borderWidth: 2.5,
                  ),
                ),
                Text('level : ${exams.level} ',
                    style: TextStyle(
                        color: Colors.grey,
                        fontSize: 1.5 * SizeConfig.textMultiplier)),
              ],
            ),
            ),
          
        ],
      backgroundColor: Colors.transparent,
      elevation: 0.0,
      ),
          body: Container(
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/bg/game_bg.jpg'), fit: BoxFit.cover),
          ),
          child: SafeArea(
            child: exams != null
                ? Stack(
                    children: <Widget>[
                      Align(
                        alignment: Alignment(0.9,0.9),
                        child: GestureDetector(
                          onTap: () async {
                            if (chooseAns == -1) {
                              await playSound('assets/audio/falsesound.mp3');
                              return;
                            }
                            if (exams.exams[indexExam]['example'] == true) {
                            } else {
                              if (chooseAns ==
                                  exams.exams.elementAt(indexExam)['ans']) {
                                exams.correct++;
                                  countFail = 0;
                                 await playSound('assets/audio/buttonClick.mp3');
                              } else {
                               countFail++;
                                if (countFail == 3) {
                                  exams.fail++;
                                  countFail = 0;
                                  await playSound('assets/audio/tryagain.mp3');
                                } else {
                                  await playSound('assets/audio/falsesound.mp3');
                                  return;
                                }
                              }
                              exams.ans.add(chooseAns);
                              exams.sol
                                  .add(exams.exams.elementAt(indexExam)['ans']);
                            }
                            setState(() {
                              chooseAns= -1;
                              checkPromote('addition_task');
                            });
                          },
                          child: Image.asset(
                            'assets/buttons/ok.png',
                            width: 20*SizeConfig.widthMultiplier,
                            fit: BoxFit.contain,
                          ),
                        ),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            flex: 1,
                            child: Container(
                              margin: EdgeInsets.symmetric(
                                  vertical: 2*SizeConfig.heightMultiplier, horizontal: 20*SizeConfig.widthMultiplier),
                              width: double.infinity,
                              height: 7*SizeConfig.heightMultiplier,
                              decoration: BoxDecoration(
                                        color: Color(0xffffd6c4),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10.0)),
                                        ),
                              child:Center(
                                      child: Text(
                                        'บวกกันแล้วได้คำตอบเป็นเท่าไรจ๊ะ',
                                        style: TextStyle(
                                          fontFamily: 'Itim',
                                          fontSize:
                                              4 * SizeConfig.textMultiplier,
                                          color: Color(0xff17478c),
                                        ),
                                      ),
                                    ),
                            )
                          ),
                          Expanded(
                            flex: 4,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                FittedBox(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      textWithStroke(
                                          fontSize: 14*SizeConfig.textMultiplier,
                                          text: exams.exams
                                              .elementAt(indexExam)['exam'][0]
                                              .toString(),
                                          textColor: Color.fromRGBO(77, 51, 0, 1),
                                          strokeWidth: 10,
                                          strokeColor:
                                              Color.fromRGBO(255, 204, 51, 1)),
                                      textWithStroke(
                                          fontSize: 14*SizeConfig.textMultiplier,
                                          text: exams.exams
                                              .elementAt(indexExam)['exam'][1]
                                              .toString(),
                                          textColor: Color.fromRGBO(77, 51, 0, 1),
                                          strokeWidth: 10,
                                          strokeColor:
                                              Color.fromRGBO(255, 204, 51, 1)),
                                      Text(
                                        '_____________________',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w900),
                                      ),
                                      textWithStroke(
                                          fontSize: 14*SizeConfig.textMultiplier,
                                          text: '?',
                                          textColor: Color.fromRGBO(77, 51, 0, 1),
                                          strokeWidth: 10,
                                          strokeColor:
                                              Color.fromRGBO(255, 204, 51, 1)),
                                      Text(
                                        '_____________________',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w900),
                                      ),
                                      Text(
                                        '_____________________',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w900),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(bottom: 18*SizeConfig.heightMultiplier),
                                  child: textWithStroke(
                                      fontSize: 13*SizeConfig.textMultiplier,
                                      text: '+',
                                      textColor: Color.fromRGBO(77, 51, 0, 1),
                                      strokeWidth: 10,
                                      strokeColor:
                                          Color.fromRGBO(255, 204, 51, 1)),
                                ),
                                SizedBox(
                                  width: 2*SizeConfig.heightMultiplier,
                                ),
                                SizedBox(
                                  width: 40*SizeConfig.heightMultiplier,
                                  height: 40*SizeConfig.heightMultiplier,
                                  child: GridView.count(
                                    padding: EdgeInsets.all(10),
                                    physics: NeverScrollableScrollPhysics(),
                                    crossAxisCount: 2,
                                    mainAxisSpacing: 2*SizeConfig.heightMultiplier,
                                    crossAxisSpacing: 2*SizeConfig.heightMultiplier,
                                    children: _getExam(),
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      )
                    ],
                  )
                : Center(
                    child: CircularProgressIndicator(),
                  ),
          )),
    );
  }

  void checkPromote(String game) {
    //ถ้าเล่นเกมส์ผิด 3 ข้อลดเลเวลลง
    if (exams.fail == 3) {
      String note = 'Fail 3 time';
      Hive.box(game).delete('unfinished_game');
      int level = Hive.box('appData').get(game + '_level');
      if (level > 1) {
        Hive.box('appData').put(game + '_level', level - 1);
        print('Level Down: $game ${Hive.box('appData').get(game)}');
      }
      LogGame test = LogGame(
          userid: Hive.box('appData').get('_id'),
          level: Hive.box('appData').get(game + '_level'),
          correct: exams.correct,
          fail: exams.fail,
          time: exams.countTime +
              DateTime.now().difference(this.countTime).inSeconds,
          note: note,
          solve: exams.sol,
          answer: exams.ans);
      http.post(
        '$serverUrl/$game/add',
        body: json.encode(test.toJson()),
        headers: {
          "content-type": "application/json",
          "accept": "application/json",
        },
      );
      Hive.box(game + '_data').add({
        'date': '${countTime.day}-${countTime.month}-${countTime.year}',
        'time': test.time,
        'correct': exams.correct,
        'fail': exams.fail
      });
      Hive.box('appData')
          .put('allTime', Hive.box('appData').get('allTime') + test.time);
      Hive.box('appData').put(
          'allCorrect', Hive.box('appData').get('allCorrect') + exams.correct);
      Hive.box('appData')
          .put('allFail', Hive.box('appData').get('allFail') + exams.fail);
      Hive.box('appData').get(game + '_level') < 5
          ? Navigator.of(context).popAndPushNamed('/addition_task')
          : null;
      exams = AdditionTaskExam();
      choice.clear();
      indexExam = 0;
      countTime = new DateTime.now();
      
      return;
    }
    //ถ้าเล่นเกมส์ครบชุด
    if (indexExam == exams.exams.length - 1) {
      print('upload log');
      //เล่นเกมส์ครบชุด ส่ง log
      String note = 'Max level';
      //เล่นเกมส์ครบชุด ส่ง log
      Hive.box(game).delete('unfinished_game');

      // เล่นเกมส์ครบชุด เช็คเงื่อนไขการอัพเลเวล
      if (Hive.box('appData').get(game + '_level') <
          Hive.box(game).get('numLevel')) {
        note = 'Level up';
        Hive.box('appData')
            .put(game + '_level', Hive.box('appData').get(game + '_level') + 1);
        print('Level Up: $game ${Hive.box('appData').get(game + '_level')}');
      }
      LogGame test = LogGame(
          userid: Hive.box('appData').get('_id'),
          level: Hive.box('appData').get(game + '_level'),
          correct: exams.correct,
          fail: exams.fail,
          time: DateTime.now().difference(this.countTime).inSeconds,
          note: note,
          solve: exams.sol,
          answer: exams.ans);
      http.post(
        '$serverUrl/$game/add',
        body: json.encode(test.toJson()),
        headers: {
          "content-type": "application/json",
          "accept": "application/json",
        },
      );
      Hive.box(game + '_data').add({
        'date': '${countTime.day}-${countTime.month}-${countTime.year}',
        'time': test.time,
        'correct': exams.correct,
        'fail': exams.fail
      });
      Hive.box('appData')
          .put('allTime', Hive.box('appData').get('allTime') + test.time);
      Hive.box('appData').put(
          'allCorrect', Hive.box('appData').get('allCorrect') + exams.correct);
      Hive.box('appData')
          .put('allFail', Hive.box('appData').get('allFail') + exams.fail);
      exams = AdditionTaskExam();
      choice.clear();
      indexExam = 0;
      countTime = new DateTime.now();
      addTimeLimit(test.time);
      showDialog(
          context: context, builder: (context) => RewardDialog(rd.nextInt(8))).then((_) {
            checkTimeLimit();
          });
      return;
    }
    //เล่นยังไม่ครบ
    lastIndex=indexExam;
    indexExam++;
  }

  void playSound(String sound) async {
     await player.setAsset(sound);
     await player.play();
  }
  @override
  void dispose() async {
    super.dispose();
    await player.dispose();
  }

 void checkTimeLimit(){
    if(Hive.box('appData').get('timeUse')==null) return;
    if(Hive.box('appData').get('timeUse')>=(Hive.box('appData').get('time')*60)){
      AwesomeDialog(context: context,
            dialogType: DialogType.INFO,
            animType: AnimType.BOTTOMSLIDE,
            btnCancel: null,
            tittle: 'เวลาหมดแล้ว',
            desc: 'ไว้มาเล่นพรุ่งนี้นะจ้ะ',
            btnOkOnPress: () {
            }).show().then((_) {
                Navigator.pop(context);
            });
    }
    print(Hive.box('appData').get('timeUse'));
     print(Hive.box('appData').get('time'));
  }

  void addTimeLimit(int time){
    String today = DateTime.now().day.toString()+DateTime.now().month.toString();
    if(Hive.box('appData').get('dayLimit')!=today){
       Hive.box('appData').put('dayLimit', today);
       Hive.box('appData').put('timeUse', 0);
    }{
      Hive.box('appData').put('timeUse', Hive.box('appData').get('timeUse')+time);
    }
  }
  @override
  void afterFirstLayout(BuildContext context) {
    setState(() {
      exams = AdditionTaskExam();
    });
  }
}
