import 'dart:math';


import 'package:bordered_text/bordered_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hive/hive.dart';
import 'package:just_audio/just_audio.dart';
import 'package:provider/provider.dart';
import 'package:smart_math/screens/size_config.dart';
import 'package:smart_math/utility/feedback_dialog.dart';
import 'package:smart_math/utility/list_game.dart';
import 'package:smart_math/utility/reward_dialog.dart';
import '../providers/UserRepository.dart';

class GameMenu extends StatefulWidget {
  GameMenu({Key key}) : super(key: key);
  @override
  _GameMenuState createState() => _GameMenuState();
}

class _GameMenuState extends State<GameMenu> {
  List<Map<String, dynamic>> listGame = [];
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      settingGames(_onSwitchChanged);
    });
    Hive.box('appData').get('setting').forEach((data) {
      listGame.add({
        'Name': data['Name'],
        'Icon': data['Icon'],
        'route': data['route'],
        'open': data['open']
      });
    });
  }

  @override
  void dispose() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    super.dispose();
   
  }

  void _onSwitchChanged() {
    setState(() {
      Hive.box('appData').put('setting', listGame);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFf2f2f2),
      appBar: AppBar(
        title: GestureDetector(child: Text('แบบฝึกหัด'),onTap: (){},),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
             Navigator.of(context).pop();
            //Provider.of<UserRepository>(context, listen: false).signOut();
          },
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.settings),
            onPressed: () {
              showDialog<List<Map<String, dynamic>>>(
                  context: context,
                  builder: (context) => settingGames(_onSwitchChanged));
            },
          ),
          IconButton(
            icon: Icon(Icons.show_chart),
            onPressed: () {
              Navigator.of(context).pushNamed('/chart');
            },
          ),
           IconButton(
            icon: Icon(Icons.exit_to_app),
            onPressed: () {
               print('SignOut()');
               Provider.of<UserRepository>(context, listen: false).signOut().then((value) {
                  Navigator.of(context).pop();
               });
            },
          ),
        ],
      ),
      body: Container(
        child: GridView.count(
          scrollDirection: Axis.horizontal,
          crossAxisCount: 2,
          padding: EdgeInsets.all(15),
          crossAxisSpacing: 10,
          mainAxisSpacing: 5,
          children:
              listGame.where((element) => element['open'] == true).map((data) {
            return Card(
                color: Colors.transparent,
                elevation: 0,
                child: GestureDetector(
                child: Container(
                  decoration: BoxDecoration(color: Color.fromRGBO(251, 219, 212, 1),borderRadius: BorderRadius.circular(24)),
                  child: Stack(
                    children: [
                      Align(
                         alignment: Alignment.center,
                        child: Container(
                          height: 35*SizeConfig.imageSizeMultiplier,
                          width: 35*SizeConfig.imageSizeMultiplier,
                           decoration: BoxDecoration(
                         image: DecorationImage(
                          image: AssetImage(data['Icon']),
                          fit: BoxFit.contain))
                        ),
                      ),
                      Align(
                    alignment: Alignment.bottomCenter,
                    child: Padding(
                      padding: EdgeInsets.only(bottom:1.2*SizeConfig.heightMultiplier),
                      child: Text(
                        data['Name'],
                        style: TextStyle(
                          fontFamily: 'Itim',
                          fontSize: 2.3
                                   * SizeConfig.textMultiplier,
                          color: Colors.grey,
                        ),
                      ),
                    ))
                    ],
                  ),
                ),
                onTap: () => Navigator.of(context).pushNamed(data['route']),
                  ));
          }).toList(),
        ),
      ),
    );
  }

  void genData() {
    Random rd = new Random();
    listgames.forEach((element) {
      int time = 0, correct = 0, fail = 0;
      for (int i = 1; i < 25; i++) {
        time = rd.nextInt(30) + 15;
        correct = rd.nextInt(10) + 5;
        fail = rd.nextInt(5);
        Hive.box(element['game'] + '_data').add({
          'date': '$i-6-2563',
          'time': time,
          'correct': correct,
          'fail': fail
        });
        Hive.box('appData')
            .put('allTime', Hive.box('appData').get('allTime') + time);
        Hive.box('appData')
            .put('allCorrect', Hive.box('appData').get('allCorrect') + correct);
        Hive.box('appData')
            .put('allFail', Hive.box('appData').get('allFail') + fail);
      }
    });
  }

  Dialog settingGames(Function setMenu) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12.0),
      ),
      child: Wrap(
        children: <Widget>[
          Container(
            width: double.infinity,
            height: 6 * SizeConfig.heightMultiplier,
            alignment: Alignment.bottomCenter,
            decoration: BoxDecoration(
              color: Color(0xFFF2BEA1),
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(12),
                topRight: Radius.circular(12),
              ),
            ),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Text(
                "ตั้งค่าแบบฝึกหัด",
                style: TextStyle(
                  color: Colors.black87,
                  fontFamily: 'Mali',
                  //fontWeight: FontWeight.w500,
                  fontSize: 3.0 * SizeConfig.textMultiplier,
                ),
                //fontWeight: FontWeight.w200),
              ),
            ),
          ),
          ShowSettingGame(listGame, setMenu),
        ],
      ),
    );
  }
}

class ShowSettingGame extends StatefulWidget {
  final List<dynamic> listGame;
  final Function setMenu;
  ShowSettingGame(this.listGame, this.setMenu);
  @override
  _ShowSettingGameState createState() => _ShowSettingGameState();
}

class _ShowSettingGameState extends State<ShowSettingGame> {
  @override
  Widget build(BuildContext context) {
    return Container(
        // height: MediaQuery.of(context).size.height,
        width: double.infinity,
        child: SingleChildScrollView(
          //controller: scrollController,
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 5,
                ),
                Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.grey[100],
                            spreadRadius: 3.5,
                            blurRadius: 2.0)
                      ]),
                  padding: EdgeInsets.symmetric(horizontal: 30),
                  margin: EdgeInsets.symmetric(horizontal: 50),
                  child: Column(
                      children: widget.listGame.map((data) {
                    int index = widget.listGame.indexOf(data);
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          '${data['Name']}',
                          style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 2 * SizeConfig.textMultiplier,
                              color: Colors.grey[700]),
                        ),
                        Switch(
                            value: widget.listGame[index]['open'],
                            activeColor: Color.fromRGBO(50, 172, 121, 1),
                            onChanged: (bool open) {
                              setState(() {
                                widget.listGame[index]['open'] =
                                    !widget.listGame[index]['open'];
                                widget.setMenu();
                              });
                            })
                      ],
                    );
                  }).toList()),
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: SizedBox(
                        width: 0,
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: Text('จำกัดเวลาต่อวัน(นาที)',
                          style: TextStyle(
                            color: Colors.black87,
                            fontFamily: 'Mali',
                            fontSize: 2.0 * SizeConfig.textMultiplier,
                          )),
                    ),
                    Expanded(
                      flex: 2,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 50),
                        child: TextFormField(
                          style: TextStyle(
                              fontSize: 2.0 * SizeConfig.textMultiplier,
                              fontFamily: 'Mali'),
                          keyboardType: TextInputType.number,
                          initialValue:
                              Hive.box('appData').get('time').toString(),
                          onChanged: (String value) {
                            Hive.box('appData').put('time', int.parse(value));
                          },
                        ),
                      ),
                    )
                  ],
                ),
              ]),
        ));
  }
}
