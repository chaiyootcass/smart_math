import 'dart:convert';
import 'dart:io';
import 'dart:math' show Random;
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:http/http.dart' as http;
import 'package:after_layout/after_layout.dart';
import 'package:bordered_text/bordered_text.dart';
import 'package:just_audio/just_audio.dart';
import 'package:liquid_progress_indicator/liquid_progress_indicator.dart';
import 'package:smart_math/game_modals/log_game.dart';
import 'package:smart_math/game_modals/numberline_task_modal.dart';
import 'package:smart_math/screens/size_config.dart';
import 'package:smart_math/utility/custom_star.dart';
import 'package:smart_math/utility/feedback_dialog.dart';
import 'package:smart_math/utility/reward_dialog.dart';
import 'package:smart_math/utility/setting.dart';

class NumberlineTask extends StatefulWidget {
  @override
  _NumberlineTaskState createState() => _NumberlineTaskState();
}

class _NumberlineTaskState extends State<NumberlineTask>
    with AfterLayoutMixin<NumberlineTask> {
  final String namegame = "numberline_task";
  final player = AudioPlayer();
  Random rd = new Random();
  DateTime countTime = new DateTime.now(); //ไว้นับเวลา
  NumberlineTaskExam exams;
  int indexExam = 0; //index exams
  List<int> choice = [];
int countFail = 0;
  List<Widget> _getExam() {
    List<Widget> exam = new List<Widget>();
    Map<String, dynamic> ex = exams.exams.elementAt(indexExam);
    choice = ex['choice'].cast<int>();
    for (int i = 0; i < choice.length; i++) {
      exam.add(
        Expanded(
          flex: 1,
          child: DragTarget<int>(
            onAccept: (data) async {
              if (data == ex['ans']) {
                exams.correct++;
                  countFail = 0;
                await playSound('assets/audio/clapping.mp3');
                if (indexExam < exams.exams.length - 1)
                  showDialog(
                      context: context, builder: (context) => FeedbackDialog());
              } else {
                 countFail++;
                if (countFail == 3) {
                  exams.fail++;
                  countFail = 0;
                  await playSound('assets/audio/tryagain.mp3');
                } else {
                  await playSound('assets/audio/falsesound.mp3');
                  return;
                }
              }
              exams.ans.add(choice[i]);
              exams.sol.add(ex['ans']);
              setState(() {
                checkPromote(namegame);
              });
            },
            onWillAccept: (data) {
              return true;
            },
            builder: (context, List<int> candidateData, rejectedData) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      BorderedText(
                        strokeWidth: 10.0,
                        strokeColor: Colors.green[900],
                        child: Text(
                          '|',
                          style: TextStyle(
                            fontSize: 3 * SizeConfig.heightMultiplier,
                            color: Colors.green[300],
                          ),
                        ),
                      ),
                      BorderedText(
                        strokeWidth: 10.0,
                        strokeColor: Colors.green[900],
                        child: Text(
                          showByLineLv(exams.lineLv, i)
                              ? choice[i].toString()
                              : "",
                          style: TextStyle(
                            fontSize: 6 * SizeConfig.heightMultiplier,
                            color: Colors.green[300],
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              );
            },
          ),
        ),
      );
    }
    playSound('assets/audio/numberline_task/putTheRocketToTheRightLine.mp3');
    return exam;
  }

  bool showByLineLv(int lv, int positon) {
    if (positon == 0 || positon == 10 || lv == 1) {
      return true;
    }
    if (lv == 2 && positon == 5) {
      return true;
    }
    return false;
  }

  void playSound(String sound) async {
    await player.setAsset(sound);
    await player.play();
  }

  @override
  void dispose() async {
    super.dispose();
    await player.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
          title: Text(
          'Numberline Task',
          style: TextStyle(color: Colors.grey,  fontSize: 1.5 * SizeConfig.textMultiplier),
        ),
        centerTitle: false,
          actions: [
          Align(
            alignment: Alignment.centerRight,
            child: Row(
              children: [
                 Container(
                  width: 25*SizeConfig.widthMultiplier,
                  height: 3*SizeConfig.heightMultiplier,
                  padding: EdgeInsets.symmetric(horizontal: 2.0),
                  child: LiquidLinearProgressIndicator(
                    value: indexExam / exams.numExam,
                    backgroundColor: Colors.white.withOpacity(0.2),
                    valueColor: AlwaysStoppedAnimation(Colors.yellowAccent),
                    borderColor: Colors.blue,
                    borderWidth: 2.5,
                  ),
                ),
                Text('level : ${exams.level} ',
                    style: TextStyle(
                        color: Colors.grey,
                        fontSize: 1.5 * SizeConfig.textMultiplier)),
              ],
            ),
          )
        ],
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        // title: Text('Numberline Task',
        //   style: TextStyle(color: Colors.black),),
        // leading: IconButton(
        //   icon: Icon(Icons.arrow_back),
        //   onPressed: () {
        //     try {
        //       exams.indexExam = indexExam;
        //       exams.countTime +=
        //           DateTime.now().difference(this.countTime).inSeconds;
        //       print(exams.toJson());
        //       Hive.box('numberline_task')
        //           .put('unfinished_game', exams.toJson());
        //       Navigator.pop(context);
        //     } catch (e) {
        //       Navigator.pop(context);
        //     }
        //   },
        // ),
      ),
      body: Container(
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: exams != null
                    ? AssetImage(
                        'assets/games/numberline/backgrounds/background${exams.level < 7 ? exams.level : 6}.jpg')
                    : AssetImage(
                        'assets/games/numberline/backgrounds/background0.jpg'),
                fit: BoxFit.cover),
          ),
          child: SafeArea(
            child: exams != null
                ? Stack(
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Expanded(
                            flex: 1,
                            child: Container(
                              height: 8 * SizeConfig.heightMultiplier,
                              width: 125 * SizeConfig.widthMultiplier,
                              // padding: EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                  color: Color(0xffffd6c4),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10.0)),
                                 ),
                              child: Center(
                                child: Text(
                                  'ให้หนูเอาจรวดลงจอดบนเส้นจำนวนให้ถูกต้อง',
                                  style: TextStyle(
                                    fontFamily: 'Itim',
                                    fontSize: 4 * SizeConfig.textMultiplier,
                                    color: Color(0xff17478c),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 5,
                            child: Stack(
                              children: <Widget>[
                                Align(
                                  alignment: Alignment(
                                      rd.nextDouble() *
                                          (rd.nextBool() ? 1 : -1),
                                      rd.nextDouble() *
                                          (rd.nextBool() ? 1 : -1)),
                                  child: Draggable<int>(
                                    data:
                                        exams.exams.elementAt(indexExam)['ans'],
                                    childWhenDragging: SizedBox(
                                      height: 10 * SizeConfig.heightMultiplier,
                                      width: 40 * SizeConfig.widthMultiplier,
                                    ),
                                     onDragCompleted: () {
                                      print('Completed');
                                      sleep(Duration(seconds:2));
                                    },
                                    feedback: Container(
                                      height: 22 * SizeConfig.heightMultiplier,
                                      width: 40 * SizeConfig.widthMultiplier,
                                      child: Image.asset(
                                          'assets/games/numberline/rocket/dot-rocket-${exams.exams.elementAt(indexExam)['ans']}.png',
                                          fit: BoxFit.scaleDown),
                                    ),
                                    child: Container(
                                      height: 22 * SizeConfig.heightMultiplier,
                                      width: 40 * SizeConfig.widthMultiplier,
                                      child: Image.asset(
                                          'assets/games/numberline/rocket/dot-rocket-${exams.exams.elementAt(indexExam)['ans']}.png',
                                          fit: BoxFit.scaleDown),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Expanded(
                            flex: 2,
                            child: Stack(
                              children: <Widget>[
                                Row(
                                  children: '--------------------------'
                                      .split('')
                                      .map((f) {
                                    return Expanded(
                                        flex: 1,
                                        child: Text(
                                          f,
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize:
                                                8 * SizeConfig.textMultiplier,
                                          ),
                                        ));
                                  }).toList(),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: 1.0 * SizeConfig.widthMultiplier,
                                      right: 2.0 * SizeConfig.widthMultiplier,
                                      top: 3 * SizeConfig.heightMultiplier),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: _getExam(),
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      )
                    ],
                  )
                : Center(
                    child: CircularProgressIndicator(),
                  ),
          )),
    );
  }

  @override
  void afterFirstLayout(BuildContext context) {
     checkTimeLimit();
    setState(() {
      exams = NumberlineTaskExam();
      indexExam = exams.indexExam;
    });
  }
   void checkTimeLimit(){
    if(Hive.box('appData').get('timeUse')==null) return;
    if(Hive.box('appData').get('timeUse')>=(Hive.box('appData').get('time')*60)){
      AwesomeDialog(context: context,
            dialogType: DialogType.INFO,
            animType: AnimType.BOTTOMSLIDE,
            btnCancel: null,
            tittle: 'เวลาหมดแล้ว',
            desc: 'ไว้มาเล่นพรุ่งนี้นะจ้ะ',
            btnOkOnPress: () {
            }).show().then((_) {
                Navigator.pop(context);
            });
    }
    print(Hive.box('appData').get('timeUse'));
     print(Hive.box('appData').get('time'));
  }

  void addTimeLimit(int time){
    String today = DateTime.now().day.toString()+DateTime.now().month.toString();
    if(Hive.box('appData').get('dayLimit')!=today){
       Hive.box('appData').put('dayLimit', today);
       Hive.box('appData').put('timeUse', 0);
    }{
      Hive.box('appData').put('timeUse', Hive.box('appData').get('timeUse')+time);
    }
  }

  void checkPromote(String game) {
    //ถ้าเล่นเกมส์ผิด 3 ข้อเลเวลลง
    if (exams.fail == 3) {
      String note = 'Fail 3 time';
      Hive.box(game).delete('unfinished_game');
      int level = Hive.box('appData').get(game + '_level');
      if (level > 1) {
        note = 'Level Down';
        Hive.box('appData').put(game + '_level', level - 1);
        print('Level Down: $game ${Hive.box('appData').get(game)}');
      }
      //ส่งข้อมูลไป Server
      LogGame test = LogGame(
          userid: Hive.box('appData').get('_id'),
          level: Hive.box('appData').get(game + '_level'),
          correct: exams.correct,
          fail: exams.fail,
          time: exams.countTime +
              DateTime.now().difference(this.countTime).inSeconds,
          note: note,
          solve: exams.sol,
          answer: exams.ans);
      http.post(
        '$serverUrl/$game/add',
        body: json.encode(test.toJson()),
        headers: {
          "content-type": "application/json",
          "accept": "application/json",
        },
      );
      //เก็บ Log ไว้ที่เครื่อง
      Hive.box(game + '_data').add({
        'date': '${countTime.day}-${countTime.month}-${countTime.year}',
        'time': test.time,
        'correct': exams.correct,
        'fail': exams.fail
      });
      Hive.box('appData')
          .put('allTime', Hive.box('appData').get('allTime') + test.time);
      Hive.box('appData').put(
          'allCorrect', Hive.box('appData').get('allCorrect') + exams.correct);
      Hive.box('appData')
          .put('allFail', Hive.box('appData').get('allFail') + exams.fail);
      exams = NumberlineTaskExam();
      choice.clear();
      countTime = new DateTime.now();
      indexExam = exams.indexExam;
      return;
    }
    //ถ้าเล่นเกมส์ครบชุด
    if (indexExam == exams.exams.length - 1) {
      print('upload log');
      String note = 'Max level';
      //เล่นเกมส์ครบชุด ส่ง log
      Hive.box(game).delete('unfinished_game');

      // เล่นเกมส์ครบชุด เช็คเงื่อนไขการอัพเลเวล
      if (Hive.box('appData').get(game + '_level') <
          Hive.box(game).get('numLevel')) {
        note = 'Level up';
        Hive.box('appData')
            .put(game + '_level', Hive.box('appData').get(game + '_level') + 1);
        print('Level Up: $game ${Hive.box('appData').get(game + '_level')}');
      }
      LogGame test = LogGame(
          userid: Hive.box('appData').get('_id'),
          level: Hive.box('appData').get(game + '_level'),
          correct: exams.correct,
          fail: exams.fail,
          time: exams.countTime +
              DateTime.now().difference(this.countTime).inSeconds,
          note: note,
          solve: exams.sol,
          answer: exams.ans);
      http.post(
        '$serverUrl/$game/add',
        body: json.encode(test.toJson()),
        headers: {
          "content-type": "application/json",
          "accept": "application/json",
        },
      );
      Hive.box(game + '_data').add({
        'date': '${countTime.day}-${countTime.month}-${countTime.year}',
        'time': test.time,
        'correct': exams.correct,
        'fail': exams.fail
      });
      Hive.box('appData')
          .put('allTime', Hive.box('appData').get('allTime') + test.time);
      Hive.box('appData').put(
          'allCorrect', Hive.box('appData').get('allCorrect') + exams.correct);
      Hive.box('appData')
          .put('allFail', Hive.box('appData').get('allFail') + exams.fail);
      exams = NumberlineTaskExam();
      choice.clear();
      countTime = new DateTime.now();
      indexExam = exams.indexExam;
     addTimeLimit(test.time);
      showDialog(
          context: context, builder: (context) => RewardDialog(rd.nextInt(8))).then((_) {
            checkTimeLimit();
          });
      return;
    }
    //เล่นยังไม่ครบ
    indexExam++;
  }
}
