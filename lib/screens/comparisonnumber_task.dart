import 'dart:convert';
import 'dart:math' show Random;

import 'package:after_layout/after_layout.dart';
import 'package:awesome_dialog/awesome_dialog.dart';

import 'package:bordered_text/bordered_text.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:just_audio/just_audio.dart';
import 'package:liquid_progress_indicator/liquid_progress_indicator.dart';
import 'package:random_color/random_color.dart';
import 'package:smart_math/game_modals/comparisonnumber_task.dart';
import 'package:http/http.dart' as http;
import 'package:smart_math/screens/size_config.dart';
import 'package:smart_math/utility/custom_star.dart';
import 'package:smart_math/utility/feedback_dialog.dart';
import 'package:smart_math/utility/reward_dialog.dart';
import 'package:smart_math/utility/setting.dart';
import '../game_modals/log_game.dart';

class ComparisonNumber extends StatefulWidget {
  @override
  _ComparisonNumberState createState() => _ComparisonNumberState();
}

class _ComparisonNumberState extends State<ComparisonNumber>
    with AfterLayoutMixin<ComparisonNumber> {
  final String namegame = "comparisonnumber_task";
  final player = AudioPlayer();
  int correcttest = 0;
 int countFail = 0;
  RandomColor _randomColor = RandomColor();
  List<Color> boxColors = [];
  DateTime countTime = new DateTime.now(); //ไว้นับเวลา
  ComparsionNumberTask exams;
  int indexExam = 0; //index exams

  Random rd = new Random(); //for random colors
  List<dynamic> choice = [];
  List<Widget> getExamContainer() {
    List<Widget> exam = new List<Widget>();
    Map<String, dynamic> ex = exams.exams.elementAt(indexExam);
    choice = ex['exam'];
    for (int i = 0; i < 2; i++) {
      exam.add(
        GestureDetector(
        onTap: () async {
          await playSound('assets/audio/comparisonnumber_task/selectBox.mp3');
          if (choice[i] == ex['ans']) {
            exams.correct++;
              countFail = 0;
            if (indexExam < exams.exams.length - 1)
              showDialog(
                  context: context, builder: (context) => FeedbackDialog());
          } else {
             countFail++;
                if (countFail == 3) {
                  exams.fail++;
                  countFail = 0;
                  await playSound('assets/audio/tryagain.mp3');
                } else {
                  await playSound('assets/audio/falsesound.mp3');
                  return;
                }
          }
          exams.ans.add(choice[i]);
          exams.sol.add(ex['ans']);

          setState(() {
            checkPromote('comparisonnumber_task');
          });
        },
        child: Container(
          width: 30 * SizeConfig.heightMultiplier,
          height: 30 * SizeConfig.heightMultiplier,
          padding: EdgeInsets.only(top: 5.0 * SizeConfig.heightMultiplier),
          decoration: BoxDecoration(
              image: DecorationImage(
                  image:
                      AssetImage('assets/games/comparisontark/box-empty.png'),
                  fit: BoxFit.contain)),
          child: Center(
            child: BorderedText(
              strokeWidth: 8.0,
              strokeColor: Colors.black,
              child: Text(
                choice[i].toString(),
                style: TextStyle(
                  fontSize: 20 * SizeConfig.textMultiplier,
                  color: _randomColor.randomColor(),
                ),
              ),
            ),
          ),
        ),
      )
      );
    }
    playSound('assets/audio/comparisonnumber_task/chooseGreaterBoxFemale.mp3');
    return exam;
  }

  void playSound(String sound) async {
    await player.setAsset(sound);
    await player.play();
  }

  @override
  void dispose() {
    super.dispose();
    player.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
           title: Text(
          'Comparison Number',
          style: TextStyle(color: Colors.grey,  fontSize: 1.5 * SizeConfig.textMultiplier),
        ),
        centerTitle: false,
         actions: [
          Align(
            alignment: Alignment.centerRight,
            child: Row(
              children: [
                 Container(
                  width: 25*SizeConfig.widthMultiplier,
                  height: 3*SizeConfig.heightMultiplier,
                  padding: EdgeInsets.symmetric(horizontal: 2.0),
                  child: LiquidLinearProgressIndicator(
                    value: indexExam / exams.numExam,
                    backgroundColor: Colors.white.withOpacity(0.2),
                    valueColor: AlwaysStoppedAnimation(Colors.yellowAccent),
                    borderColor: Colors.blue,
                    borderWidth: 2.5,
                  ),
                ),
                Text('level : ${exams.level} ',
                    style: TextStyle(
                        color: Colors.grey,
                        fontSize: 1.5 * SizeConfig.textMultiplier)),
              ],
            ),
          )
        ],
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          // title: Text(
          //   'Comparison Number',
          //   style: TextStyle(color: Colors.black),
          // ),
          // leading: IconButton(
          //   icon: Icon(Icons.arrow_back),
          //   onPressed: () {
          //     try {
          //       exams.indexExam = indexExam;
          //       exams.countTime +=
          //           DateTime.now().difference(this.countTime).inSeconds;
          //       print(exams.toJson());
          //       Hive.box('comparisonnumber_task')
          //           .put('unfinished_game', exams.toJson());
          //       Navigator.pop(context);
          //     } catch (e) {
          //       Navigator.pop(context);
          //     }
          //   },
          // ),
        ),
        body: Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(
                      'assets/games/counting/backgrounds/background-moutain.png'),
                  fit: BoxFit.cover),
            ),
            child: exams != null
                ? SafeArea(
                    child: Stack(
                      children: <Widget>[
                        Center(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(
                                  left: 20.0 * SizeConfig.heightMultiplier,
                                  right: 20.0 * SizeConfig.heightMultiplier,
                                  bottom: 50 * SizeConfig.heightMultiplier,
                                  //top: 1.0 * SizeConfig.heightMultiplier,
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Expanded(
                                      flex: 1,
                                      child: Container(
                                        height: 8 * SizeConfig.heightMultiplier,
                                        decoration: BoxDecoration(
                                              color: Color(0xffffd6c4),
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                      ),
                                        child: Center(
                                          child: Text(
                                            'ให้หนูกดหมายเลขที่มีค่ามากกว่า',
                                            style: TextStyle(
                                              fontFamily: 'Itim',
                                              fontSize: 4 *
                                                  SizeConfig.textMultiplier,
                                             color: Color(0xff17478c),
                                            ),
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                        Column(
                          children: <Widget>[
                            Expanded(
                              flex: 9,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: getExamContainer(),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  )
                : Center(child: CircularProgressIndicator())));
  }

  void checkPromote(String game) {
    //ถ้าเล่นเกมส์ผิด 3 ข้อเลเวลลง
    if (exams.fail == 3) {
      String note = 'Fail 3 time';
      Hive.box(game).delete('unfinished_game');
      int level = Hive.box('appData').get(game + '_level');
      if (level > 1) {
        note = 'Level Down';
        Hive.box('appData').put(game + '_level', level - 1);
        print('Level Down: $game ${Hive.box('appData').get(game)}');
      }
      LogGame test = LogGame(
          userid: Hive.box('appData').get('_id'),
          level: Hive.box('appData').get(game + '_level'),
          correct: exams.correct,
          fail: exams.fail,
          time: exams.countTime +
              DateTime.now().difference(this.countTime).inSeconds,
          note: note,
          solve: exams.sol,
          answer: exams.ans);
      http.post(
        '$serverUrl/$game/add',
        body: json.encode(test.toJson()),
        headers: {
          "content-type": "application/json",
          "accept": "application/json",
        },
      );
      Hive.box(game + '_data').add({
        'date': '${countTime.day}-${countTime.month}-${countTime.year}',
        'time': test.time,
        'correct': exams.correct,
        'fail': exams.fail
      });
      Hive.box('appData')
          .put('allTime', Hive.box('appData').get('allTime') + test.time);
      Hive.box('appData').put(
          'allCorrect', Hive.box('appData').get('allCorrect') + exams.correct);
      Hive.box('appData')
          .put('allFail', Hive.box('appData').get('allFail') + exams.fail);
      exams = ComparsionNumberTask();
      countTime = new DateTime.now();
      indexExam = 0;
      return;
    }
    //ถ้าเล่นเกมส์ครบชุด
    if (indexExam == exams.exams.length - 1) {
      print('upload log');
      String note = 'Max level';
      //เล่นเกมส์ครบชุด ส่ง log
      Hive.box(game).delete('unfinished_game');

      // เล่นเกมส์ครบชุด เช็คเงื่อนไขการอัพเลเวล
      if (Hive.box('appData').get(game + '_level') <
          Hive.box(game).get('numLevel')) {
        note = 'Level up';
        Hive.box('appData')
            .put(game + '_level', Hive.box('appData').get(game + '_level') + 1);
        print('Level Up: $game ${Hive.box('appData').get(game + '_level')}');
      }
      LogGame test = LogGame(
          userid: Hive.box('appData').get('_id'),
          level: Hive.box('appData').get(game + '_level'),
          correct: exams.correct,
          fail: exams.fail,
          time: exams.countTime +
              DateTime.now().difference(this.countTime).inSeconds,
          note: note,
          solve: exams.sol,
          answer: exams.ans);
      http.post(
        '$serverUrl/$game/add',
        body: json.encode(test.toJson()),
        headers: {
          "content-type": "application/json",
          "accept": "application/json",
        },
      );
      Hive.box(game + '_data').add({
        'date': '${countTime.day}-${countTime.month}-${countTime.year}',
        'time': test.time,
        'correct': exams.correct,
        'fail': exams.fail
      });
      Hive.box('appData')
          .put('allTime', Hive.box('appData').get('allTime') + test.time);
      Hive.box('appData').put(
          'allCorrect', Hive.box('appData').get('allCorrect') + exams.correct);
      Hive.box('appData')
          .put('allFail', Hive.box('appData').get('allFail') + exams.fail);
      exams = ComparsionNumberTask();
      countTime = new DateTime.now();
      indexExam = exams.indexExam;
      addTimeLimit(test.time);
      showDialog(
          context: context,
          builder: (context) => RewardDialog(rd.nextInt(8))).then((_) {
        checkTimeLimit();
      });
      return;
    }
    indexExam++;
  }

  void checkTimeLimit() {
    if (Hive.box('appData').get('timeUse') == null) return;
    if (Hive.box('appData').get('timeUse') >=
        (Hive.box('appData').get('time') * 60)) {
      AwesomeDialog(
              context: context,
              dialogType: DialogType.INFO,
              animType: AnimType.BOTTOMSLIDE,
              btnCancel: null,
              tittle: 'เวลาหมดแล้ว',
              desc: 'ไว้มาเล่นพรุ่งนี้นะจ้ะ',
              btnOkOnPress: () {})
          .show()
          .then((_) {
        Navigator.pop(context);
      });
    }
    print(Hive.box('appData').get('timeUse'));
    print(Hive.box('appData').get('time'));
  }

  void addTimeLimit(int time) {
    String today =
        DateTime.now().day.toString() + DateTime.now().month.toString();
    if (Hive.box('appData').get('dayLimit') != today) {
      Hive.box('appData').put('dayLimit', today);
      Hive.box('appData').put('timeUse', 0);
    }
    {
      Hive.box('appData')
          .put('timeUse', Hive.box('appData').get('timeUse') + time);
    }
  }

  @override
  void afterFirstLayout(BuildContext context) {
    checkTimeLimit();
    setState(() {
      exams = ComparsionNumberTask();
      indexExam = exams.indexExam;
      boxColors.add(_randomColor.randomColor(colorHue: ColorHue.red));
      boxColors.add(_randomColor.randomColor(colorHue: ColorHue.green));
    });
  }
}
