import 'package:flutter/material.dart';

Dialog resultCustomDialog({@required BuildContext context,@required int score, @required int lenght, @required double passScore}) {
  return Dialog(
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(12.0),
    ),
    child: GestureDetector(
      onTap: () => Navigator.of(context).pop(),
          child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20.0),
        ),
        height: 400.0,
        width: 500.0,
        child: Column(
          children: <Widget>[
            Container(
              width: double.infinity,
              height: 80,
              alignment: Alignment.bottomCenter,
              decoration: BoxDecoration(
                color: Colors.greenAccent,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(12),
                  topRight: Radius.circular(12),
                ),
              ),
              child: Align(
                alignment: Alignment.center,
                child: Text(
                  "ผลการทดสอบ",
                  style: TextStyle(
                      color: Colors.black87,
                      fontSize: 35,
                      fontWeight: FontWeight.w600),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('ทำได้ $score คะแนน ',
                    style: TextStyle(
                      color: Colors.black87,
                      fontSize: 28,
                    ))
              ],
            ),
            SizedBox(
              height: 20,
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
                          child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: List<Widget>.generate(
                    score,
                    (index) => Icon(
                          Icons.star,
                          color: Colors.yellow,
                          size: 35,
                        )),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('จากคะแนนเต็ม $lenght คะแนน ',
                    style: TextStyle(
                      color: Colors.black87,
                      fontSize: 28,
                    ))
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                    'แปลผล: ${score >= passScore ? 'ผ่านเกณฑ์' : 'ควรฝึกทักษะนี้เพิ่ม'} ',
                    style: TextStyle(
                      color: Colors.black87,
                      fontSize: 28,
                    ))
              ],
            ),
          ],
        ),
      ),
    ),
  );
}
