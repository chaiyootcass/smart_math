import 'dart:async';

import 'package:flutter/material.dart';
import 'package:smart_math/screens/size_config.dart';

class FeedbackDialog extends StatefulWidget {
 
  @override
  State<StatefulWidget> createState() => FeedbackDialogState();
}

class FeedbackDialogState extends State<FeedbackDialog>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;
  BuildContext dialog;
  Timer time;
  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 450));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Curves.elasticInOut);
    controller.addListener(() {
      setState(() {});
    });
    controller.forward();
  }
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    time = Timer(Duration(milliseconds: 1200),(){
      Navigator.pop(dialog);
    });
  }

  @override
  void dispose() {
    super.dispose();
    time.cancel();
  }

  @override
  Widget build(BuildContext context) {
    dialog=context;
    return Center(
      child: Material(
        color: Colors.transparent,
        child: ScaleTransition(
          scale: scaleAnimation,
          child: Container(
            width: 60*SizeConfig.widthMultiplier,
            height: 60*SizeConfig.heightMultiplier,
            child: FittedBox(
              fit: BoxFit.contain,
                          child: GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Image.asset('assets/reward/correct0.png'),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
