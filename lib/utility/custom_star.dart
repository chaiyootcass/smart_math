import 'package:flutter/material.dart';
import 'package:liquid_progress_indicator/liquid_progress_indicator.dart';

class CustomStar extends StatelessWidget {
  final double value;
  CustomStar(this.value);
  @override
  Widget build(BuildContext context) {
    return    LiquidCustomProgressIndicator(
             backgroundColor: Colors.black.withOpacity(0.3),
            valueColor: AlwaysStoppedAnimation(Colors.yellow), 
            direction: Axis.vertical,
            value: value,
            shapePath: _buildStarPath(),
          );
  }

  Path _buildStarPath() {
    double mid = 160;
    double half = 80;
    mid = mid - half;
    return Path()
      ..moveTo(40,68)
      ..lineTo(mid + half * 1.5, half * 0.84)
      ..lineTo(mid + half * 0.68, half * 1.45)
      ..lineTo(mid + half * 1.0, half * 0.5)
      ..lineTo(mid + half * 1.32, half * 1.45)
      ..lineTo(mid + half * 0.5, half * 0.84)
      ..close();
  }
}