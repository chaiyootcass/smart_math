import 'package:flutter/material.dart';

class RewardDialog extends StatefulWidget {
  final int rewardNum;
  RewardDialog(this.rewardNum);
  @override
  State<StatefulWidget> createState() => RewardDialogState();
}

class RewardDialogState extends State<RewardDialog>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 450));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Curves.elasticInOut);
    controller.addListener(() {
      setState(() {});
    });
    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Material(
        color: Colors.transparent,
        child: ScaleTransition(
          scale: scaleAnimation,
          child: Container(
            width: MediaQuery.of(context).size.width-150,
            height:  MediaQuery.of(context).size.height-150,
            decoration: ShapeDecoration(
                color: Colors.white,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0))),
            child: GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: FittedBox(
                fit: BoxFit.contain,
                child:
                    Image.asset('assets/reward/reward${widget.rewardNum}.gif',fit: BoxFit.contain,)
                    ,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
