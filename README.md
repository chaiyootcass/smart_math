
```
├── README.md
├── assets
│   ├── animals
│   │   ├── ?\231?\201.png
│   │   ├── ม?\211าลาย.png
│   │   ├── สิ?\207?\202?\225.png
│   │   ├── สุ?\231ั?\202.png
│   │   ├── หมี.png
│   │   ├── ?\201?\201ะ.png
│   │   └── ?\201มว.png
│   ├── audio
│   │   ├── addition_task
│   │   │   ├── calculateNumberAndAnswer.mp3
│   │   │   ├── question-number.mp3
│   │   │   ├── question-object-1.mp3
│   │   │   ├── question-object-2.mp3
│   │   │   ├── question-object-example-0.mp3
│   │   │   ├── question-object-example-1.mp3
│   │   │   └── questionCloudApple.mp3
│   │   ├── buttonClick.mp3
│   │   ├── children-yay0.mp3
│   │   ├── children-yay1.mp3
│   │   ├── clapping.mp3
│   │   ├── comparison_task
│   │   │   ├── chooseGreaterBoxFemale.mp3
│   │   │   └── exampleNumberRightOverLeftFemale.mp3
│   │   ├── comparisonnumber_task
│   │   │   ├── chooseGreaterBoxFemale.mp3
│   │   │   └── selectBox.mp3
│   │   ├── correspondence_task
│   │   │   ├── chooseAnswerBelow.mp3
│   │   │   └── countAndAnswer.mp3
│   │   ├── counting_task
│   │   │   ├── countBack.mp3
│   │   │   ├── countFront.mp3
│   │   │   ├── examplecandy.mp3
│   │   │   ├── exampleques1.mp3
│   │   │   ├── howManyAnimal.mp3
│   │   │   ├── num_1.mp3
│   │   │   ├── num_10.mp3
│   │   │   ├── num_11.mp3
│   │   │   ├── num_12.mp3
│   │   │   ├── num_13.mp3
│   │   │   ├── num_14.mp3
│   │   │   ├── num_15.mp3
│   │   │   ├── num_16.mp3
│   │   │   ├── num_17.mp3
│   │   │   ├── num_18.mp3
│   │   │   ├── num_19.mp3
│   │   │   ├── num_2.mp3
│   │   │   ├── num_20.mp3
│   │   │   ├── num_3.mp3
│   │   │   ├── num_4.mp3
│   │   │   ├── num_5.mp3
│   │   │   ├── num_6.mp3
│   │   │   ├── num_7.mp3
│   │   │   ├── num_8.mp3
│   │   │   ├── num_9.mp3
│   │   │   ├── sound_ques_1.mp3
│   │   │   ├── sound_ques_10.mp3
│   │   │   ├── sound_ques_11.mp3
│   │   │   ├── sound_ques_12.mp3
│   │   │   ├── sound_ques_13.mp3
│   │   │   ├── sound_ques_14.mp3
│   │   │   ├── sound_ques_15.mp3
│   │   │   ├── sound_ques_16.mp3
│   │   │   ├── sound_ques_17.mp3
│   │   │   ├── sound_ques_18.mp3
│   │   │   ├── sound_ques_19.mp3
│   │   │   ├── sound_ques_2.mp3
│   │   │   ├── sound_ques_20.mp3
│   │   │   ├── sound_ques_3.mp3
│   │   │   ├── sound_ques_4.mp3
│   │   │   ├── sound_ques_5.mp3
│   │   │   ├── sound_ques_6.mp3
│   │   │   ├── sound_ques_7.mp3
│   │   │   ├── sound_ques_8.mp3
│   │   │   └── sound_ques_9.mp3
│   │   ├── example.mp3
│   │   ├── falsesound.mp3
│   │   ├── gotoRealQuestion.mp3
│   │   ├── letSeeExample.mp3
│   │   ├── numberident_task
│   │   │   ├── number-0.mp3
│   │   │   ├── number-1.mp3
│   │   │   ├── number-10.mp3
│   │   │   ├── number-11.mp3
│   │   │   ├── number-12.mp3
│   │   │   ├── number-13.mp3
│   │   │   ├── number-14.mp3
│   │   │   ├── number-15.mp3
│   │   │   ├── number-16.mp3
│   │   │   ├── number-17.mp3
│   │   │   ├── number-18.mp3
│   │   │   ├── number-19.mp3
│   │   │   ├── number-2.mp3
│   │   │   ├── number-20.mp3
│   │   │   ├── number-25.mp3
│   │   │   ├── number-29.mp3
│   │   │   ├── number-3.mp3
│   │   │   ├── number-33.mp3
│   │   │   ├── number-4.mp3
│   │   │   ├── number-40.mp3
│   │   │   ├── number-5.mp3
│   │   │   ├── number-6.mp3
│   │   │   ├── number-7.mp3
│   │   │   ├── number-8.mp3
│   │   │   ├── number-9.mp3
│   │   │   └── pressThisNumberFemale.mp3
│   │   ├── numberline_task
│   │   │   ├── example.mp3
│   │   │   ├── exampleSound.mp3
│   │   │   ├── examplerocket.mp3
│   │   │   ├── examplestar.mp3
│   │   │   └── putTheRocketToTheRightLine.mp3
│   │   ├── ordinal_task
│   │   │   ├── errorBoxIsEmpty.mp3
│   │   │   ├── gotAnswerChooseAnimalBelow.mp3
│   │   │   ├── numberOnTheBox.mp3
│   │   │   ├── pickNumberPlaceInBox.mp3
│   │   │   ├── question0.mp3
│   │   │   ├── question1.mp3
│   │   │   ├── question2.mp3
│   │   │   └── questions.mp3
│   │   ├── questionToStart.mp3
│   │   ├── selectBox.mp3
│   │   ├── selectSoundExample.mp3
│   │   ├── soundclickans.mp3
│   │   ├── starCollected.mp3
│   │   ├── subtraction_task
│   │   │   ├── question-number-example.mp3
│   │   │   ├── question-number.mp3
│   │   │   ├── question-object-1.mp3
│   │   │   ├── question-object-2.mp3
│   │   │   ├── question-object-example-0.mp3
│   │   │   └── question-object-example-1.mp3
│   │   └── tryagain.mp3
│   ├── bg
│   │   ├── game_bg.jpg
│   │   ├── home.png
│   │   └── sticker_bg.jpg
│   ├── buttons
│   │   └── ok.png
│   ├── fonts
│   │   ├── Amycute.ttf
│   │   ├── FC\ Muffin\ Regular.ttf
│   │   ├── Itim-Regular.ttf
│   │   ├── Mali-Bold.ttf
│   │   ├── PeachPlay.ttf
│   │   ├── SanamDeklen_chaya.ttf
│   │   └── pahnto.ttf
│   ├── fruits
│   │   ├── Asset1.png
│   │   ├── Asset2.png
│   │   ├── Asset3.png
│   │   ├── Asset4.png
│   │   ├── Asset5.png
│   │   ├── Asset6.png
│   │   ├── Asset7.png
│   │   ├── Asset8.png
│   │   └── Asset9.png
│   ├── games
│   │   ├── addition
│   │   │   ├── apple
│   │   │   │   ├── apple-choice-1.png
│   │   │   │   ├── apple-choice-10.png
│   │   │   │   ├── apple-choice-2.png
│   │   │   │   ├── apple-choice-3.png
│   │   │   │   ├── apple-choice-4.png
│   │   │   │   ├── apple-choice-5.png
│   │   │   │   ├── apple-choice-6.png
│   │   │   │   ├── apple-choice-7.png
│   │   │   │   ├── apple-choice-8.png
│   │   │   │   ├── apple-choice-9.png
│   │   │   │   └── cloud.png
│   │   │   ├── backgrounds
│   │   │   │   ├── background-number.png
│   │   │   │   └── background-object.png
│   │   │   └── questions-choices
│   │   │       ├── choice-question-object-0-1.png
│   │   │       ├── choice-question-object-0-2.png
│   │   │       ├── choice-question-object-0-3.png
│   │   │       ├── choice-question-object-1-2.png
│   │   │       ├── choice-question-object-1-3.png
│   │   │       ├── choice-question-object-1-4.png
│   │   │       ├── choice-question-object-2-4.png
│   │   │       ├── choice-question-object-2-5.png
│   │   │       ├── choice-question-object-2-6.png
│   │   │       ├── question-number-1.png
│   │   │       ├── question-object-0-1.png
│   │   │       ├── question-object-1-1.png
│   │   │       ├── question-object-1-2.png
│   │   │       ├── question-object-1.png
│   │   │       ├── question-object-2-2.png
│   │   │       ├── question-object-2-3.png
│   │   │       ├── question-object-2.png
│   │   │       ├── sample-object-1-2.png
│   │   │       └── sample-object-1.png
│   │   ├── comparisontark
│   │   │   ├── box-closed.png
│   │   │   ├── box-empty.png
│   │   │   ├── char-1.png
│   │   │   ├── char-10.png
│   │   │   ├── char-11.png
│   │   │   ├── char-12.png
│   │   │   ├── char-13.png
│   │   │   ├── char-14.png
│   │   │   ├── char-15.png
│   │   │   ├── char-16.png
│   │   │   ├── char-17.png
│   │   │   ├── char-18.png
│   │   │   ├── char-19.png
│   │   │   ├── char-2.png
│   │   │   ├── char-20.png
│   │   │   ├── char-21.png
│   │   │   ├── char-22.png
│   │   │   ├── char-23.png
│   │   │   ├── char-24.png
│   │   │   ├── char-3.png
│   │   │   ├── char-4.png
│   │   │   ├── char-5.png
│   │   │   ├── char-6.png
│   │   │   ├── char-7.png
│   │   │   ├── char-8.png
│   │   │   └── char-9.png
│   │   ├── counting
│   │   │   ├── animals
│   │   │   │   ├── chicken-normal.png
│   │   │   │   ├── chicken-shadow.png
│   │   │   │   ├── ladybug-normal.png
│   │   │   │   ├── ladybug-shadow.png
│   │   │   │   ├── mouse-normal.png
│   │   │   │   └── mouse-shadow.png
│   │   │   ├── backgrounds
│   │   │   │   ├── background-moutain.png
│   │   │   │   └── background-space.png
│   │   │   ├── buttons
│   │   │   │   ├── button-cloud-ok.png
│   │   │   │   ├── button-cloud-up.png
│   │   │   │   ├── button-correct-default.png
│   │   │   │   ├── button-correct-down.png
│   │   │   │   ├── button-correct-hover.png
│   │   │   │   ├── button-wrong-default.png
│   │   │   │   ├── button-wrong-down.png
│   │   │   │   └── button-wrong-hover.png
│   │   │   └── resource
│   │   │       ├── countbear.png
│   │   │       ├── countbglv1.png
│   │   │       ├── countbglv2.png
│   │   │       ├── countbglv3.png
│   │   │       ├── countbglv4.png
│   │   │       ├── countbglv5.png
│   │   │       ├── countbglv6.png
│   │   │       ├── countcandy.png
│   │   │       ├── countcandyempty.png
│   │   │       ├── countcandylv1.png
│   │   │       ├── countcandylv2.png
│   │   │       ├── countcandylv3.png
│   │   │       ├── countcandylv4.png
│   │   │       ├── countcandylv5.png
│   │   │       ├── countcandylv6.png
│   │   │       ├── countcandypush.png
│   │   │       ├── countok.png
│   │   │       ├── countopush.png
│   │   │       └── counttray.png
│   │   ├── numberident
│   │   │   ├── bird-cartoon.png
│   │   │   ├── number-circle-0.png
│   │   │   ├── number-circle-1.png
│   │   │   ├── number-circle-10.png
│   │   │   ├── number-circle-11.png
│   │   │   ├── number-circle-12.png
│   │   │   ├── number-circle-13.png
│   │   │   ├── number-circle-14.png
│   │   │   ├── number-circle-15.png
│   │   │   ├── number-circle-2.png
│   │   │   ├── number-circle-23.png
│   │   │   ├── number-circle-24.png
│   │   │   ├── number-circle-25.png
│   │   │   ├── number-circle-26.png
│   │   │   ├── number-circle-27.png
│   │   │   ├── number-circle-29.png
│   │   │   ├── number-circle-3.png
│   │   │   ├── number-circle-4.png
│   │   │   ├── number-circle-5.png
│   │   │   ├── number-circle-6.png
│   │   │   ├── number-circle-7.png
│   │   │   ├── number-circle-8.png
│   │   │   ├── number-circle-9.png
│   │   │   ├── wood.png
│   │   │   └── woodbg.png
│   │   ├── numberline
│   │   │   ├── backgrounds
│   │   │   │   ├── background0.jpg
│   │   │   │   ├── background1.jpg
│   │   │   │   ├── background2.jpg
│   │   │   │   ├── background3.jpg
│   │   │   │   ├── background4.jpg
│   │   │   │   ├── background5.jpg
│   │   │   │   └── background6.jpg
│   │   │   └── rocket
│   │   │       ├── dot-rocket-0.png
│   │   │       ├── dot-rocket-1.png
│   │   │       ├── dot-rocket-10.png
│   │   │       ├── dot-rocket-2.png
│   │   │       ├── dot-rocket-3.png
│   │   │       ├── dot-rocket-4.png
│   │   │       ├── dot-rocket-5.png
│   │   │       ├── dot-rocket-6.png
│   │   │       ├── dot-rocket-7.png
│   │   │       ├── dot-rocket-8.png
│   │   │       ├── dot-rocket-9.png
│   │   │       ├── meteor-1.png
│   │   │       ├── meteor-10.png
│   │   │       ├── meteor-2.png
│   │   │       ├── meteor-3.png
│   │   │       ├── meteor-4.png
│   │   │       ├── meteor-5.png
│   │   │       ├── meteor-6.png
│   │   │       ├── meteor-7.png
│   │   │       ├── meteor-8.png
│   │   │       ├── meteor-9.png
│   │   │       ├── number-rocket-1.png
│   │   │       ├── number-rocket-10.png
│   │   │       ├── number-rocket-2.png
│   │   │       ├── number-rocket-3.png
│   │   │       ├── number-rocket-4.png
│   │   │       ├── number-rocket-5.png
│   │   │       ├── number-rocket-6.png
│   │   │       ├── number-rocket-7.png
│   │   │       ├── number-rocket-8.png
│   │   │       ├── number-rocket-9.png
│   │   │       ├── star-rocket-1.png
│   │   │       ├── star-rocket-10.png
│   │   │       ├── star-rocket-2.png
│   │   │       ├── star-rocket-3.png
│   │   │       ├── star-rocket-4.png
│   │   │       ├── star-rocket-5.png
│   │   │       ├── star-rocket-6.png
│   │   │       ├── star-rocket-7.png
│   │   │       ├── star-rocket-8.png
│   │   │       └── star-rocket-9.png
│   │   ├── ordinal
│   │   │   ├── train1.png
│   │   │   └── train2.png
│   │   └── subtraction
│   │       ├── choice-0-1.png
│   │       ├── choice-0-2.png
│   │       ├── choice-0-3.png
│   │       ├── choice-1-3.png
│   │       ├── choice-1-4.png
│   │       ├── choice-1-5.png
│   │       ├── choice-2-5.png
│   │       ├── choice-2-6.png
│   │       ├── choice-2-7.png
│   │       ├── question0.png
│   │       ├── question1.png
│   │       └── question2.png
│   ├── icons
│   │   ├── number_identification.jpg
│   │   └── stickers_icon.jpg
│   ├── menu
│   │   ├── addition.jpg
│   │   ├── comparenumber.jpg
│   │   ├── compareobj.jpg
│   │   ├── count.jpg
│   │   ├── counting.jpg
│   │   ├── five.jpg
│   │   ├── flower.jpg
│   │   ├── numberline.jpg
│   │   ├── numcorres.jpg
│   │   ├── numiden.jpg
│   │   ├── ordinality.jpg
│   │   ├── sub.jpg
│   │   ├── test.jpg
│   │   ├── ?\201?\232?\232?\227?\224สอ?\232.jpg
│   │   └── ?\201?\232?\232?\235ึ?\201หั?\224.jpg
│   ├── reward
│   │   ├── correct0.png
│   │   ├── reward0.gif
│   │   ├── reward1.gif
│   │   ├── reward2.gif
│   │   ├── reward3.gif
│   │   ├── reward4.gif
│   │   ├── reward5.gif
│   │   ├── reward6.gif
│   │   └── reward7.gif
│   └── stickers
│       ├── 1.png
│       ├── 2.png
│       ├── 3.png
│       ├── สิ?\207?\202?\225.png
│       ├── สุ?\231ั?\202.png
│       ├── หมี.png
│       └── ?\201มว.png
├── lib
│   ├── game_modals
│   │   ├── addition_task_modal.dart
│   │   ├── comparison_task_modal.dart
│   │   ├── comparisonnumber_task.dart
│   │   ├── correspondence_task_modal.dart
│   │   ├── counting_task_modal.dart
│   │   ├── log_game.dart
│   │   ├── number_ident_modal.dart
│   │   ├── numberline_task_modal.dart
│   │   ├── ordinal_task_modal.dart
│   │   ├── test_task.dart
│   │   ├── test_task_exams.dart
│   │   └── test_task_modal.dart
│   ├── main.dart
│   ├── providers
│   │   └── UserRepository.dart
│   ├── screens
│   │   ├── addition_task
│   │   │   ├── additon_task.dart
│   │   │   ├── addtion_task_lv1-2.dart
│   │   │   ├── addtion_task_lv3-4.dart
│   │   │   └── addtion_task_lv5-6.dart
│   │   ├── chart_page.dart
│   │   ├── comparison_task.dart
│   │   ├── comparisonnumber_task.dart
│   │   ├── correspondace_task.dart
│   │   ├── counting_task.dart
│   │   ├── game_menu.dart
│   │   ├── login_page.dart
│   │   ├── main_task.dart
│   │   ├── number_ident_task.dart
│   │   ├── numberline_task.dart
│   │   ├── ordinal_task.dart
│   │   ├── size_config.dart
│   │   ├── splash_page.dart
│   │   ├── stickers_room.dart
│   │   └── test_task
│   │       ├── addition_a_task.dart
│   │       ├── addition_b_task.dart
│   │       ├── comparison_task.dart
│   │       ├── comparisonnumber_task.dart
│   │       ├── correspondence_task.dart
│   │       ├── couting_bc_task.dart
│   │       ├── couting_d_task.dart
│   │       ├── game_menu.dart
│   │       ├── number_ident_task.dart
│   │       ├── numberline_task.dart
│   │       ├── ordinal_a_task.dart
│   │       ├── ordinal_b_task.dart
│   │       ├── subtraction_a_task.dart
│   │       └── subtraction_b_task.dart
│   └── utility
│       ├── custom_dialog.dart
│       ├── custom_star.dart
│       ├── feedback_dialog.dart
│       ├── list_game.dart
│       ├── numberToThai.dart
│       ├── reward_dialog.dart
│       └── setting.dart
├── pubspec.lock
├── pubspec.yaml
├── smart_math.iml
└── test
    └── widget_test.dart
```


วิธีการติดตั้ง
1. ติดตั้งตัว Flutter โดยเลือกระบบปฏิบัติการของเครื่อง https://flutter.dev/docs/get-started/install
2. ดาวโหลดไฟล์ของโปรเจคนี้ผ่านทาง git clone https://gitlab.com/chaiyootcass/smart_math.git หรือกดตรงดาวโหลดได้ที่ https://gitlab.com/chaiyootcass/smart_math
3. แก้ไขไฟล์ lib/utility/setting.dart โดยเปลี่ยนค่าของตัวแปร serverUrl ให้เป็นไอพีของเครื่องที่เก็บดาต้าเบสของผู้เล่น
4. สามารถรันผ่าน simulator โดย เข้า cmd(window OS) แล้ว cd เข้าโฟลเดอร์โปรเจคนี้ จากนั้นพิมพ์คำสั่ง flutter run
5. หากต้องการไฟล์ apk สามารถพิมพ์คำสั่ง flutter build apk โดยไฟล์จะอยู่ที่โฟลเดอร์ build/app/outputs/flutter-apk/app-release.apk

